# INFORMATIONS

EpiStone is a fantasy card game.

Application folder is for front-end
Server folder is for back-end

 
How to launch the project :
- Run the script data.sql in folder script in sqlServer to create and init the database
- In the file server/EpiStone/appsettings.json
, change the value "Epistone" in "ConnectionStrings"
  to match your configuration
- Compile and run in IIS the EpiStone project in backend folder (port : 59165)
- Compile and run the EpiStone project in frontend folder

EpiStoneTestProject folder is for test in back-end
- To Edit the ConnectionStrings, go to the file server/EpiStoneTestProject/appsettings.json
  and change the value "Epistone" in "ConnectionStrings"
- Compile and run the the tests
- To run the tests in ControllersTests folder, you need to execute them one by one
