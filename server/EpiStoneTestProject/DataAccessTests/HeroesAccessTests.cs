﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EpiStoneTestProject.DataAccessTests
{
    public class HeroesAccessTests
    {
        private EpiStone.DataAccess.HeroesAccess _heroes;
        private Mock.FakeHeroeRepo _repo;

        public HeroesAccessTests()
        {
            _repo = new Mock.FakeHeroeRepo();
            _repo._heroes = new List<EpiStone.DBO.Heroe>()
            {
                new EpiStone.DBO.Heroe()
                {

                },
                new EpiStone.DBO.Heroe()
                {
                    Id = 1,
                    Name = "test"
                },
                new EpiStone.DBO.Heroe()
                {

                }
            };
            _heroes = new EpiStone.DataAccess.HeroesAccess(_repo);
        }

        [Fact]
        public void getAllHeroes()
        {
            var list = _heroes.GetList() as List<EpiStone.DBO.Heroe>;
            Assert.NotNull(list);
            Assert.NotEmpty(list);
            Assert.Equal(3, list.Count);
        }

        [Fact]
        public void getHeroe()
        {
            var heroe = _heroes.GetById(1);
            Assert.IsType<EpiStone.DBO.Heroe>(heroe);
            Assert.Equal(1, heroe.Id);
            Assert.Equal("test", heroe.Name);
        }
    }
}
