﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EpiStoneTestProject.DataAccessTests
{
    public class UsersAccessTests
    {
        private EpiStone.DataAccess.UsersAccess _users;
        private Mock.FakeUserRepo _repo;

        public UsersAccessTests()
        {
            _repo = new Mock.FakeUserRepo();
            _repo._users = new List<EpiStone.DBO.User>()
            {
                new EpiStone.DBO.User()
                {
                    Pseudo = "sqdsq"
                },
                new EpiStone.DBO.User()
                {
                    Id = 1,
                    Pseudo = "notest"
                },
                new EpiStone.DBO.User()
                {
                    Pseudo = "qsdsqd"
                }
            };
            _users = new EpiStone.DataAccess.UsersAccess(_repo);
        }

        [Fact]
        public void getUser()
        {
            var user = _users.GetUser(1);
            Assert.NotNull(user);
            Assert.IsType<EpiStone.DBO.User>(user);
            Assert.Equal(1, user.Id);
            Assert.NotEqual("test", user.Pseudo);
        }

        [Fact]
        public void createUser()
        {
            EpiStone.DBO.User newUser = new EpiStone.DBO.User()
            {
                Id = 2,
                Pseudo = "test",
                Defeats = 0,
                Draws = 0,
                Email = "",
                Password = "",
                Score = 0,
                Victories = 0
            };
            _users.CreateUser(newUser);
            var user = _repo._users.Find(elem => elem.Id == 2);
            Assert.NotNull(user);
            Assert.IsType<EpiStone.DBO.User>(user);
            Assert.Equal(2, user.Id);
            Assert.Equal("test", user.Pseudo);
        }

        [Fact]
        public void updateUser()
        {
            EpiStone.DBO.User newUser = new EpiStone.DBO.User()
            {
                Id = 1,
                Pseudo = "test1"
            };
            var user = _repo._users.Find(elem => elem.Id == 1);
            Assert.NotNull(user);
            Assert.IsType<EpiStone.DBO.User>(user);
            Assert.Equal(1, user.Id);
            Assert.NotEqual("test1", user.Pseudo);
        }

        [Fact]
        public void deleteUser()
        {
            _users.DeleteUser(1);
            var user = _repo._users.Find(elem => elem.Id == 1);
            Assert.Null(user);
        }

        [Fact]
        public void noCreateUser()
        {
            EpiStone.DBO.User newUser = new EpiStone.DBO.User()
            {
                Id = 6,
                Pseudo = "test"
            };
            var user = _repo._users.Find(elem => elem.Id == 6);
            Assert.Null(user);
        }
    }
}
