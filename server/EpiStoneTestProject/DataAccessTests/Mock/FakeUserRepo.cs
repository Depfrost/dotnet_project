﻿using System;
using System.Collections.Generic;
using System.Text;
using EpiStone.DBO;

namespace EpiStoneTestProject.DataAccessTests.Mock
{
    public class FakeUserRepo : EpiStone.DataAccess.Repository.IRepository<EpiStone.DBO.User>
    {

        public List<User> _users { get; set; }
        public bool Create(User entity)
        {
            _users.Add(entity);
            return true;
        }

        public bool Delete(long id)
        {
            int index = _users.FindIndex(elem => elem.Id == id);
            _users.RemoveAt(index);
            return true;
        }

        public IEnumerable<User> GetAll()
        {
            return _users;
        }

        public User GetById(long id)
        {
            return _users.Find(elem => elem.Id == id);
        }

        public bool Update(User entity, long id)
        {
            int index = _users.FindIndex(elem => elem.Id == id);
            _users.RemoveAt(index);
            _users.Add(entity);
            return true;
        }
    }
}
