﻿using System;
using System.Collections.Generic;
using System.Text;
using EpiStone.DBO;

namespace EpiStoneTestProject.DataAccessTests.Mock
{
    public class FakeHeroeRepo : EpiStone.DataAccess.Repository.IRepository<EpiStone.DBO.Heroe>
    {
        public List<Heroe> _heroes { get; set; }
        public bool Create(Heroe entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Heroe> GetAll()
        {
            return _heroes;
        }

        public Heroe GetById(long id)
        {
            return _heroes.Find(elem => elem.Id == id);
        }

        public bool Update(Heroe entity, long id)
        {
            throw new NotImplementedException();
        }
    }
}
