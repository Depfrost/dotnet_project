﻿using System;
using System.Collections.Generic;
using System.Text;
using EpiStone.DBO;

namespace EpiStoneTestProject.DataAccessTests.Mock
{
    public class FakeEffectRepo : EpiStone.DataAccess.Repository.IRepository<Effect>
    {
        public List<Effect> _effects { get; set; }
        public bool Create(Effect entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Effect> GetAll()
        {
            return _effects;
        }

        public Effect GetById(long id)
        {
            return _effects.Find(elem => elem.Id == id);
        }

        public bool Update(Effect entity, long id)
        {
            throw new NotImplementedException();
        }
    }
}
