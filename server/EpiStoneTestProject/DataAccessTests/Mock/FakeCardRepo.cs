﻿using System;
using System.Collections.Generic;
using System.Text;
using EpiStone.DBO;

namespace EpiStoneTestProject.DataAccessTests.Mock
{
    public class FakeCardRepo : EpiStone.DataAccess.Repository.IRepository<EpiStone.DBO.Card>
    {
        public List<Card> _cards { get; set; }
        public bool Create(Card entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Card> GetAll()
        {
            return _cards;
        }

        public Card GetById(long id)
        {
            return _cards.Find(elem => elem.Id == id);
        }

        public bool Update(Card entity, long id)
        {
            throw new NotImplementedException();
        }
    }
}
