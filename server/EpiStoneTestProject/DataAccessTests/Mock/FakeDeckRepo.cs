﻿using System;
using System.Collections.Generic;
using System.Text;
using EpiStone.DBO;

namespace EpiStoneTestProject.DataAccessTests.Mock
{
    public class FakeDeckRepo : EpiStone.DataAccess.Repository.IRepository<Deck>
    {
        public List<Deck> _decks { get; set; }
        public bool Create(Deck entity)
        {
            _decks.Add(entity);
            return true;
        }

        public bool Delete(long id)
        {
            int index = _decks.FindIndex(elem => elem.Id == id);
            _decks.RemoveAt(index);
            return true;
        }

        public IEnumerable<Deck> GetAll()
        {
            throw new NotImplementedException();
        }

        public Deck GetById(long id)
        {
            return _decks.Find(elem => elem.Id == id);
        }

        public bool Update(Deck entity, long id)
        {
            int index = _decks.FindIndex(elem => elem.Id == id);
            _decks.RemoveAt(index);
            _decks.Add(entity);
            return true;
        }
    }
}
