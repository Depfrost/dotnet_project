﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EpiStoneTestProject.DataAccessTests
{
    public class EffectsAccessTests
    {
        private EpiStone.DataAccess.EffectsAccess _effects;
        private Mock.FakeEffectRepo _repo;

        public EffectsAccessTests()
        {
            _repo = new Mock.FakeEffectRepo();
            _repo._effects = new List<EpiStone.DBO.Effect>()
            {
                new EpiStone.DBO.Effect()
                {
                    Id = 1,
                    Description = "test"
                },
                new EpiStone.DBO.Effect()
                {

                },
                new EpiStone.DBO.Effect()
                {

                }
            };
            _effects = new EpiStone.DataAccess.EffectsAccess(_repo);
        }

        [Fact]
        public void getAllEffect()
        {
            var list = _effects.GetList() as List<EpiStone.DBO.Effect>;
            Assert.NotNull(list);
            Assert.NotEmpty(list);
            Assert.Equal(3, list.Count);
        }

        [Fact]
        public void getEffect()
        {
            var effect = _effects.GetById(1);
            Assert.IsType<EpiStone.DBO.Effect>(effect);
            Assert.Equal(1, effect.Id);
            Assert.Equal("test", effect.Description);
        }
    }
}
