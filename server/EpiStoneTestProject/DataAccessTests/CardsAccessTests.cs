﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using EpiStone.DBO;

namespace EpiStoneTestProject.DataAccessTests
{

    public class CardsAccessTests
    {
        private EpiStone.DataAccess.CardsAccess _cards;
        private Mock.FakeCardRepo _repo;

        public CardsAccessTests()
        {
            _repo = new Mock.FakeCardRepo();
            _repo._cards = new List<Card>()
            {
                new Card()
                {
                    Attack = 1,
                    Id = 1
                },
                new Card()
                {

                },
                new Card()
                {

                }
            };
            _cards = new EpiStone.DataAccess.CardsAccess(_repo);
        }

        [Fact]
        public void getAllCards()
        {
            var list = _cards.GetList() as List<Card>;
            Assert.NotNull(list);
            Assert.NotEmpty(list);
            Assert.Equal(3, list.Count);
        }

        [Fact]
        public void getCard()
        {
            var card = _cards.GetCard(1);
            Assert.IsType<Card>(card);
            Assert.Equal(1, card.Id);
            Assert.Equal(1, card.Attack);
        }
    }
}
