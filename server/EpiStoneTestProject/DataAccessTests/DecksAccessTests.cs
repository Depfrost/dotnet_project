﻿using System;
using System.Collections.Generic;
using System.Text;
using EpiStone.DBO;
using Xunit;

namespace EpiStoneTestProject.DataAccessTests
{
    public class DecksAccessTests
    {
        private EpiStone.DataAccess.DecksAccess _decks;
        private Mock.FakeDeckRepo _repo;

        public DecksAccessTests()
        {
            _repo = new Mock.FakeDeckRepo();
            _repo._decks = new List<EpiStone.DBO.Deck>()
            {
                new Deck()
                {
                    Id = 1,
                    Name = "test"
                },
                new Deck()
                {

                },
                new Deck()
                {

                }
            };
            _decks = new EpiStone.DataAccess.DecksAccess(_repo);
        }

        [Fact]
        public void createDeck()
        {
            Deck newDeck = new Deck()
            {
                Id = 3,
                Name = "test1"
            };
            _decks.CreateDeck(newDeck);
            var deck = _repo._decks.Find(elem => elem.Id == 3);
            Assert.NotNull(deck);
            Assert.IsType<Deck>(deck);
            Assert.Equal(3, deck.Id);
            Assert.Equal("test1", deck.Name);
        }

        [Fact]
        public void getDeck()
        {
            var deck = _decks.GetDeck(1);
            Assert.NotNull(deck);
            Assert.IsType<Deck>(deck);
            Assert.Equal(1, deck.Id);
            Assert.Equal("test", deck.Name);
        }

        [Fact]
        public void updateDeck()
        {
            Deck newUser = new Deck()
            {
                Id = 1,
                Name = "test1"
            };
            var deck = _repo._decks.Find(elem => elem.Id == 1);
            Assert.NotNull(deck);
            Assert.IsType<Deck>(deck);
            Assert.Equal(1, deck.Id);
            Assert.NotEqual("test1", deck.Name);
        }

        [Fact]
        public void deleteDeck()
        {
            _decks.DeleteDeck(1);
            var deck = _repo._decks.Find(elem => elem.Id == 1);
            Assert.Null(deck);
        }
    }
}
