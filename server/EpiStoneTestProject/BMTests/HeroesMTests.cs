﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EpiStoneTestProject.BMTests
{
    public class HeroesMTests
    {
        private EpiStone.BusinessManagement.HeroesManagement _bm;
        private Mock.FakeHeroeData _data;

        public HeroesMTests()
        {
            _data = new Mock.FakeHeroeData();
            _data.heroes = new List<EpiStone.DBO.Heroe>()
            {
                new EpiStone.DBO.Heroe()
                {

                },
                new EpiStone.DBO.Heroe()
                {
                    Id = 1,
                    Name = "test"
                },
                new EpiStone.DBO.Heroe()
                {

                }
            };
            _bm = new EpiStone.BusinessManagement.HeroesManagement(_data);
        }

        [Fact]
        public void getAllHeroes()
        {
            var list = _data.GetList() as List<EpiStone.DBO.Heroe>;
            Assert.NotNull(list);
            Assert.NotEmpty(list);
            Assert.Equal(3, list.Count);
        }

        [Fact]
        public void getHeroe()
        {
            var heroe = _data.GetById(1);
            Assert.IsType<EpiStone.DBO.Heroe>(heroe);
            Assert.Equal(1, heroe.Id);
            Assert.Equal("test", heroe.Name);
        }
    }
}
