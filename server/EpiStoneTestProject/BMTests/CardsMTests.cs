﻿using EpiStone.DBO;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EpiStoneTestProject.BMTests
{
    public class CardsMTests
    {
        private EpiStone.BusinessManagement.CardsManagement _bm;
        private Mock.FakeCardData _data;

        public CardsMTests()
        {
            _data = new Mock.FakeCardData();
            _data.cards = new List<EpiStone.DBO.Card>()
            {
                new Card()
                {
                    Attack = 1,
                    Id = 1
                },
                new Card()
                {

                },
                new Card()
                {

                }
            };
            _bm = new EpiStone.BusinessManagement.CardsManagement(_data);
        }

        [Fact]
        public void getAllCards()
        {
            var list = _data.GetList() as List<Card>;
            Assert.NotNull(list);
            Assert.NotEmpty(list);
            Assert.Equal(3, list.Count);
        }

        [Fact]
        public void getCard()
        {
            var card = _data.GetCard(1);
            Assert.IsType<Card>(card);
            Assert.Equal(1, card.Id);
            Assert.Equal(1, card.Attack);
        }
    }
}
