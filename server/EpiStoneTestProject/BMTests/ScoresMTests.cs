﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EpiStoneTestProject.BMTests
{
    public class ScoresMTests
    {
        private EpiStone.BusinessManagement.ScoresManagement _bm;
        private Mock.FakeScoreData _data;

        public ScoresMTests()
        {
            _data = new Mock.FakeScoreData();
            _data.users = new List<EpiStone.DBO.User>()
            {
                new EpiStone.DBO.User()
                {
                    Id = 1,
                    Defeats = 1,
                    Draws = 2,
                    Victories = 3,
                    Score = 14
                },
                new EpiStone.DBO.User()
                {
                    Id = 2,
                    Defeats = 0,
                    Draws = 2,
                    Victories = 3,
                    Score = 13
                },
                new EpiStone.DBO.User()
                {
                    Id = 3,
                    Defeats = 3,
                    Draws = 2,
                    Victories = 1,
                    Score = 10
                }
            };
            _bm = new EpiStone.BusinessManagement.ScoresManagement(_data);
        }

        [Fact]
        public void getAllScore()
        {
            var list = _bm.GetList();
            Assert.NotNull(list);
            Assert.NotEmpty(list);
            foreach (var item in list)
            {
                Assert.Equal(item.Score, item.Defeats + item.Draws * 2 + item.Victories * 3);
            }
        }

        [Fact]
        public void getScoreByUser()
        {
            var scores = _bm.GetScores(1);
            Assert.NotNull(scores);
            Assert.IsType<EpiStone.DBO.User>(scores);
            Assert.Equal(scores.Score, scores.Defeats + scores.Draws * 2 + scores.Victories * 3);

        }
    }
}
