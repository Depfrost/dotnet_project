﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EpiStoneTestProject.BMTests
{
    public class UsersMTests
    {
        private EpiStone.BusinessManagement.UsersManagement _bm;
        private Mock.FakeUserData _data;

        public UsersMTests()
        {
            _data = new Mock.FakeUserData();
            _data.users = new List<EpiStone.DBO.User>()
            {
                new EpiStone.DBO.User()
                {
                    Pseudo = "sqdsq"
                },
                new EpiStone.DBO.User()
                {
                    Id = 1,
                    Pseudo = "notest"
                },
                new EpiStone.DBO.User()
                {
                    Pseudo = "qsdsqd"
                }
            };
            _bm = new EpiStone.BusinessManagement.UsersManagement(_data);
        }

        [Fact]
        public void getUser()
        {
            var user = _data.GetUser(1);
            Assert.NotNull(user);
            Assert.IsType<EpiStone.DBO.User>(user);
            Assert.Equal(1, user.Id);
            Assert.NotEqual("test", user.Pseudo);
        }

        [Fact]
        public void createUser()
        {
            EpiStone.DBO.User newUser = new EpiStone.DBO.User()
            {
                Id = 2,
                Pseudo = "test",
                Defeats = 0,
                Draws = 0,
                Email = "",
                Password = "",
                Score = 0,
                Victories = 0
            };
            _data.CreateUser(newUser);
            var user = _data.users.Find(elem => elem.Id == 2);
            Assert.NotNull(user);
            Assert.IsType<EpiStone.DBO.User>(user);
            Assert.Equal(2, user.Id);
            Assert.Equal("test", user.Pseudo);
        }

        [Fact]
        public void updateUser()
        {
            EpiStone.DBO.User newUser = new EpiStone.DBO.User()
            {
                Id = 1,
                Pseudo = "test1"
            };
            var user = _data.users.Find(elem => elem.Id == 1);
            Assert.NotNull(user);
            Assert.IsType<EpiStone.DBO.User>(user);
            Assert.Equal(1, user.Id);
            Assert.NotEqual("test1", user.Pseudo);
        }

        [Fact]
        public void deleteUser()
        {
            _data.DeleteUser(1);
            var user = _data.users.Find(elem => elem.Id == 1);
            Assert.Null(user);
        }

        [Fact]
        public void connexionPseudo()
        {
            var user = _data.ConnexionUsers("notest");
            Assert.NotNull(user);
            Assert.IsType<EpiStone.DBO.User>(user);
            Assert.Equal(1, user.Id);
        }
    }
}
