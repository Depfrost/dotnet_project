﻿using EpiStone.DBO;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EpiStoneTestProject.BMTests
{
    public class DecksMTests
    {
        private EpiStone.BusinessManagement.DecksManagement _bm;
        private Mock.FakeDeckData _data;

        public DecksMTests()
        {
            _data = new Mock.FakeDeckData();
            _data.decks = new List<EpiStone.DBO.Deck>()
            {
                new Deck()
                {
                    Id = 1,
                    Name = "test",
                    UserId = 1
                },
                new Deck()
                {

                },
                new Deck()
                {

                }
            };
            _bm = new EpiStone.BusinessManagement.DecksManagement(_data);
        }

        [Fact]
        public void createDeck()
        {
            Deck newDeck = new Deck()
            {
                Id = 3,
                Name = "test1"
            };
            _data.CreateDeck(newDeck);
            var deck = _data.decks.Find(elem => elem.Id == 3);
            Assert.NotNull(deck);
            Assert.IsType<Deck>(deck);
            Assert.Equal(3, deck.Id);
            Assert.Equal("test1", deck.Name);
        }

        [Fact]
        public void getDeck()
        {
            var deck = _data.GetDeck(1);
            Assert.NotNull(deck);
            Assert.IsType<Deck>(deck);
            Assert.Equal(1, deck.Id);
            Assert.Equal("test", deck.Name);
        }

        [Fact]
        public void updateDeck()
        {
            Deck newUser = new Deck()
            {
                Id = 1,
                Name = "test1"
            };
            var deck = _data.decks.Find(elem => elem.Id == 1);
            Assert.NotNull(deck);
            Assert.IsType<Deck>(deck);
            Assert.Equal(1, deck.Id);
            Assert.NotEqual("test1", deck.Name);
        }

        [Fact]
        public void deleteDeck()
        {
            _data.DeleteDeck(1);
            var deck = _data.decks.Find(elem => elem.Id == 1);
            Assert.Null(deck);
        }

        [Fact]
        public void getDeckByUserId()
        {
            var list = _data.GetDeckByUser(1) as List<Deck>;
            Assert.NotNull(list);
            Assert.NotEmpty(list);
            Assert.IsType<List<Deck>>(list);
            Assert.Single(list);
        }
    }
}
