﻿using System;
using System.Collections.Generic;
using System.Text;
using EpiStone.DBO;

namespace EpiStoneTestProject.BMTests.Mock
{
    public class FakeHeroeData : EpiStone.DataAccess.Interfaces.IHeroes
    {
        public List<Heroe> heroes { get; set; }
        public Heroe GetById(long id)
        {
            return heroes.Find(elem => elem.Id == id);
        }

        public IEnumerable<Heroe> GetList()
        {
            return heroes;
        }
    }
}
