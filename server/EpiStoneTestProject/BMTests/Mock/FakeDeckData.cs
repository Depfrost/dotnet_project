﻿using System;
using System.Collections.Generic;
using System.Text;
using EpiStone.DBO;

namespace EpiStoneTestProject.BMTests.Mock
{
    public class FakeDeckData : EpiStone.DataAccess.Interfaces.IDecks
    {
        public List<Deck> decks { get; set; }
        public bool CreateDeck(Deck deck)
        {
            decks.Add(deck);
            return true;
        }

        public bool DeleteDeck(long id)
        {
            int index = decks.FindIndex(elem => elem.Id == id);
            decks.RemoveAt(index);
            return true;
        }

        public Deck GetDeck(long id)
        {
            return decks.Find(elem => elem.Id == id);
        }

        public IEnumerable<Deck> GetDeckByUser(long id)
        {
            return decks.FindAll(elem => elem.UserId == id);
        }

        public IEnumerable<Deck> GetList()
        {
            return decks;
        }

        public bool UpdateDeck(Deck deck, long id)
        {
            int index = decks.FindIndex(elem => elem.Id == id);
            decks.RemoveAt(index);
            decks.Add(deck);
            return true;
        }
    }
}
