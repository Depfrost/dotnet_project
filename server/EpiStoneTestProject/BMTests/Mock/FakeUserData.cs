﻿using System;
using System.Collections.Generic;
using System.Text;
using EpiStone.DBO;

namespace EpiStoneTestProject.BMTests.Mock
{
    public class FakeUserData : EpiStone.DataAccess.Interfaces.IUsers
    {
        public List<User> users { get; set; }
        public User ConnexionUsers(string pseudo)
        {
            return users.Find(elem => elem.Pseudo == pseudo);
        }

        public bool CreateUser(User user)
        {
            users.Add(user);
            return true;
        }

        public bool DeleteUser(long id)
        {
            int index = users.FindIndex(elem => elem.Id == id);
            users.RemoveAt(index);
            return true;
        }

        public IEnumerable<User> GetList()
        {
            return users;
        }

        public User GetUser(long id)
        {
            return users.Find(elem => elem.Id == id);
        }

        public bool UpdateUser(User user, long id)
        {
            int index = users.FindIndex(elem => elem.Id == id);
            users.RemoveAt(index);
            users.Add(user);
            return true;
        }
    }
}
