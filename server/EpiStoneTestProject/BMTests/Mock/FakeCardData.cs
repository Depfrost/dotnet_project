﻿using System;
using System.Collections.Generic;
using System.Text;
using EpiStone.DBO;

namespace EpiStoneTestProject.BMTests.Mock
{
    public class FakeCardData : EpiStone.DataAccess.Interfaces.ICards
    {
        public List<Card> cards { get; set; }
        public Card GetCard(long id)
        {
            return cards.Find(elem => elem.Id == id);
        }

        public IEnumerable<Card> GetList()
        {
            return cards;
        }
    }
}
