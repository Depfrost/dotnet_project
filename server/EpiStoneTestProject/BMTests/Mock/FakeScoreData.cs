﻿using System;
using System.Collections.Generic;
using System.Text;
using EpiStone.DBO;

namespace EpiStoneTestProject.BMTests.Mock
{
    public class FakeScoreData : EpiStone.DataAccess.Interfaces.IScores
    {
        public List<User> users { get; set; }
        public IEnumerable<User> GetList()
        {
            return users;
        }

        public User GetScores(long id)
        {
            return users.Find(elem => elem.Id == id);
        }

        public bool UpdateScore(User user, long id)
        {
            throw new NotImplementedException();
        }
    }
}
