﻿using System;
using System.Collections.Generic;
using System.Text;
using EpiStone.DBO;

namespace EpiStoneTestProject.BMTests.Mock
{
    public class FakeEffectData : EpiStone.DataAccess.Interfaces.IEffects
    {
        public List<Effect> effects { get; set; }
        public Effect GetById(long id)
        {
            return effects.Find(elem => elem.Id == id);
        }

        public IEnumerable<Effect> GetList()
        {
            return effects;
        }
    }
}
