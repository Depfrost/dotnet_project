﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EpiStoneTestProject.BMTests
{
    public class EffectsMTests
    {
        private EpiStone.BusinessManagement.EffectsManagement _bm;
        private Mock.FakeEffectData _data;

        public EffectsMTests()
        {
            _data = new Mock.FakeEffectData();
            _data.effects = new List<EpiStone.DBO.Effect>()
            {
                new EpiStone.DBO.Effect()
                {
                    Id = 1,
                    Description = "test"
                },
                new EpiStone.DBO.Effect()
                {

                },
                new EpiStone.DBO.Effect()
                {

                }
            };
            _bm = new EpiStone.BusinessManagement.EffectsManagement(_data);
        }

        [Fact]
        public void getAllEffect()
        {
            var list = _data.GetList() as List<EpiStone.DBO.Effect>;
            Assert.NotNull(list);
            Assert.NotEmpty(list);
            Assert.Equal(3, list.Count);
        }

        [Fact]
        public void getEffect()
        {
            var effect = _data.GetById(1);
            Assert.IsType<EpiStone.DBO.Effect>(effect);
            Assert.Equal(1, effect.Id);
            Assert.Equal("test", effect.Description);
        }
    }
}
