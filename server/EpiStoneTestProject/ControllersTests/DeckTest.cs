﻿using EpiStone;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EpiStoneTestProject.ControllersTests
{
    public class DeckTest
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;


        public DeckTest()
        {
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = _server.CreateClient();

        }

        [Fact]
        public async Task GetDeck()
        {
            var response = await _client.GetAsync("/api/decks/22");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var decks = JsonConvert.DeserializeObject<EpiStone.Models.Get.DeckModel>(responseString);

            Assert.Equal(22, decks.Id);
        }

        [Fact]
        public async Task GetDeckByUser()
        {
            var response = await _client.GetAsync("/api/decks/user/1");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var decks = JsonConvert.DeserializeObject<List<EpiStone.Models.Get.DeckModel>>(responseString);

            Assert.NotNull(decks);
            Assert.NotEmpty(decks);
        }
    }
}
