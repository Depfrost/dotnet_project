﻿using EpiStone;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EpiStoneTestProject.ControllersTests
{
    public class CardTest
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;


        public CardTest()
        {
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = _server.CreateClient();

        }

        [Fact]
        public async Task GetCard()
        {
            var response = await _client.GetAsync("/api/cards/1");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var cards = JsonConvert.DeserializeObject<EpiStone.Models.Get.CardModel>(responseString);

            Assert.Equal(1, cards.Id);
        }

        [Fact]
        public async Task GetAllCards()
        {
            var response = await _client.GetAsync("/api/cards");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var cards = JsonConvert.DeserializeObject<List<EpiStone.Models.Get.CardModel>>(responseString);

            Assert.NotNull(cards);
            Assert.NotEmpty(cards);
        }
    }
}
