﻿using EpiStone;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EpiStoneTestProject.ControllersTests
{
    public class ScoreTest
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;
        public ScoreTest()
        {
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = _server.CreateClient();
        }

        [Fact]
        public async Task GetAllScores()
        {
            var response = await _client.GetAsync("/api/scores");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var scores = JsonConvert.DeserializeObject<List<EpiStone.Models.Get.ScoreModel>>(responseString);

            Assert.NotNull(scores);
            Assert.NotEmpty(scores);
        }

        [Fact]
        public async Task GetScoreByUser()
        {
            var response = await _client.GetAsync("/api/scores/user/1");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var score = JsonConvert.DeserializeObject<EpiStone.Models.Get.ScoreModel>(responseString);

            Assert.NotNull(score);
        }

        [Fact]
        public async Task CheckScoreCalculation()
        {
            var response = await _client.GetAsync("/api/scores");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var scores = JsonConvert.DeserializeObject<List<EpiStone.Models.Get.ScoreModel>>(responseString);

            Assert.NotNull(scores);
            Assert.NotEmpty(scores);

            foreach (var item in scores)
            {
                var real_score = item.Draws * 2 + item.Defeats + item.Victories * 3;
                Assert.Equal(item.Score, real_score);
            }
        }
    }
}
