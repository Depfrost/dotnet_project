﻿using EpiStone;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EpiStoneTestProject.ControllersTests
{
    public class EffectTest
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public EffectTest()
        {
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = _server.CreateClient();

        }

        [Fact]
        public async Task GetEffect()
        {
            var response = await _client.GetAsync("/api/effects/1");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var persons = JsonConvert.DeserializeObject<EpiStone.Models.Get.EffectModel>(responseString);

            Assert.Equal(1, persons.Id);
        }

        [Fact]
        public async Task GetAllEffects()
        {
            var response = await _client.GetAsync("/api/effects");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var effects = JsonConvert.DeserializeObject<List<EpiStone.Models.Get.EffectModel>>(responseString);

            Assert.NotNull(effects);
            Assert.NotEmpty(effects);
        }
    }
}
