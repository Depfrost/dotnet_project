﻿using EpiStone;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EpiStoneTestProject.ControllersTests
{
    public class UserTest
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;


        public UserTest()
        {
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = _server.CreateClient();

        }

        [Fact]
        public async Task GetUser()
        {
            var response = await _client.GetAsync("/api/users/1");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var persons = JsonConvert.DeserializeObject<EpiStone.Models.Get.UserModel>(responseString);

            Assert.Equal(1, persons.Id);
        }
    }
}
