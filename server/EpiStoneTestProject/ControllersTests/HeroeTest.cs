﻿using EpiStone;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EpiStoneTestProject.ControllersTests
{
    public class HeroeTest
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public HeroeTest()
        {
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = _server.CreateClient();

        }

        [Fact]
        public async Task GetHeroe()
        {
            var response = await _client.GetAsync("/api/heroes/1");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var persons = JsonConvert.DeserializeObject<EpiStone.Models.Get.HeroeModel>(responseString);

            Assert.Equal(1, persons.Id);
        }

        [Fact]
        public async Task GetAllHeroes()
        {
            var response = await _client.GetAsync("/api/heroes");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var heroes = JsonConvert.DeserializeObject<List<EpiStone.Models.Get.HeroeModel>>(responseString);

            Assert.NotNull(heroes);
            Assert.NotEmpty(heroes);
        }
    }
}
