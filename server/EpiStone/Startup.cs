﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AutoMapper;
using System.IO;
using EpiStone.DataAccess.Models;
using Swashbuckle.AspNetCore.Swagger;
using EpiStone.Hub;
using EpiStone.Hub.Models;
using EpiStone.BusinessManagement;
using EpiStone.DataAccess;
using EpiStone.Models.Hub;

namespace EpiStone
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get;}

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region BuildSetting
            var builder = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json");
       
            var config = builder.Build();
            var connectionString = config.GetConnectionString("EpiStone");
            #endregion

            #region AddServices

            services.AddSignalR();
            services.AddDbContext<EpiStoneContext>(options=> options.UseSqlServer(connectionString));
            
            services.AddScoped<BusinessManagement.Interfaces.IUsersManagement, UsersManagement>();
            services.AddTransient<DataAccess.Interfaces.IUsers, UsersAccess>();
            services.AddTransient<DataAccess.Repository.IRepository<DBO.User>, DataAccess.Repository.Repository<Users, DBO.User>>();

            services.AddScoped<BusinessManagement.Interfaces.ICardsManagement, CardsManagement>();
            services.AddTransient<DataAccess.Interfaces.ICards, CardsAccess>();
            services.AddTransient<DataAccess.Repository.IRepository<DBO.Card>, DataAccess.Repository.Repository<Cards, DBO.Card>>();

            services.AddScoped<BusinessManagement.Interfaces.IDecksManagement, DecksManagement>();
            services.AddTransient<DataAccess.Interfaces.IDecks, DecksAccess>();
            services.AddTransient<DataAccess.Repository.IRepository<DBO.Deck>, DataAccess.Repository.Repository<Decks, DBO.Deck>>();

            services.AddScoped<BusinessManagement.Interfaces.IHeroesManagement, HeroesManagement>();
            services.AddTransient<DataAccess.Interfaces.IHeroes, HeroesAccess>();
            services.AddTransient<DataAccess.Repository.IRepository<DBO.Heroe>, DataAccess.Repository.Repository<Heroes, DBO.Heroe>>();

            services.AddScoped<BusinessManagement.Interfaces.IEffectsManagement, EffectsManagement>();
            services.AddTransient<DataAccess.Interfaces.IEffects, EffectsAccess>();
            services.AddTransient<DataAccess.Repository.IRepository<DBO.Effect>, DataAccess.Repository.Repository<Effects, DBO.Effect>>();

            services.AddScoped<BusinessManagement.Interfaces.IScoresManagement, ScoresManagement>();
            services.AddTransient<DataAccess.Interfaces.IScores, ScoresAccess>();

            services.AddTransient<Hub.Interfaces.IGameState, GameState>();

            services.AddTransient<Hub.Business.Interfaces.IGamesManagement, Hub.Business.GamesManagement>();
            services.AddTransient<Hub.Business.Interfaces.IWaitingListManagement, Hub.Business.WaitingListManagement>();
            services.AddTransient<Hub.Business.Interfaces.IPlayersManagement, Hub.Business.PlayersManagement>();
            services.AddTransient<Hub.Data.Interfaces.IGames, Hub.Data.GamesAccess>();
            services.AddTransient<Hub.Data.Interfaces.IPlayers, Hub.Data.PlayersAccess>();
            services.AddTransient<Hub.Data.Interfaces.IWaitingList, Hub.Data.WaitingListAccess>();

            services.AddMvc();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "EpiStone", Version = "v1" });
                var basePath = AppContext.BaseDirectory;
                var xmlPath = Path.Combine(basePath, "EpiStone.xml");
                c.IncludeXmlComments(xmlPath);
            });

            #endregion

            #region AutoMapper
            Mapper.Reset();
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<DBO.User, Models.Post.UserModelPost>();
                cfg.CreateMap<Models.Post.UserModelPost, DBO.User>();
                cfg.CreateMap<Users, DBO.User>();
                cfg.CreateMap<DBO.User, Users>();

               
                cfg.CreateMap<DBO.User, Models.Get.ScoreModel>();
                cfg.CreateMap<Models.Get.ScoreModel, DBO.User>();

                cfg.CreateMap<Decks, DBO.Deck>();
                cfg.CreateMap<DBO.Deck, Decks>();
                cfg.CreateMap<DBO.Deck, Models.Post.DeckModelPost>();
                cfg.CreateMap<Models.Post.DeckModelPost, DBO.Deck>();
                cfg.CreateMap<DBO.Deck, Models.Get.DeckModel>();
                cfg.CreateMap<Models.Get.DeckModel, DBO.Deck>();

                cfg.CreateMap<Cards, DBO.Card>();
                cfg.CreateMap<DBO.Card, Models.Get.CardModel>().AfterMap((x, y) => {
                    y.Type = x.Type == 1 ? "Sort" : "Monstre";
                    y.Attack = x.Life == null ? -1 : x.Attack.Value;
                    y.Life = x.Life == null ? -1 : x.Life.Value;
                    y.EffectId = x.EffectId == null ? -1 : x.EffectId.Value;
                });


                cfg.CreateMap<Effects, DBO.Effect>();
                cfg.CreateMap<DBO.Effect, Models.Get.EffectModel>().AfterMap((x, y) =>
                {
                    y.TargetAllMonster = x.TargetAllMonster == 1;
                    y.TargetHero = x.TargetHero == 1;
                    y.TargetSingleMonster = x.TargetSingleMonster == 1;
                });

                cfg.CreateMap<Heroes, DBO.Heroe>();
                cfg.CreateMap<DBO.Heroe, Models.Get.HeroeModel>();

                cfg.CreateMap<Monster, Models.Hub.MonsterFieldModel>().AfterMap((x, y) =>
                {
                    y.Id = x.MonsterId;
                });

                cfg.CreateMap<CardGame, CardModelGame>().AfterMap((x, y) => {
                    y.Type = x.Type == 1 ? "Sort" : "Monstre";
                    y.Attack = x.Life == null ? -1 : x.Attack.Value;
                    y.Life = x.Life == null ? -1 : x.Life.Value;
                    y.EffectId = x.EffectId == null ? -1 : x.EffectId.Value;
                });


                cfg.CreateMap<Player, Models.Hub.PlayerModel>().AfterMap((x, y) =>
                {
                    y.NbCardDeck = x.Deck.Count;
                    y.PlayerHand = new List<CardModelGame>();
                    y.FieldMonster = new List<Models.Hub.MonsterFieldModel>();
                    foreach (var item in x.PlayerHand)
                    {
                        y.PlayerHand.Add(Mapper.Map<CardModelGame>(item));
                    }
                    foreach (var item in x.FieldMonster)
                    {
                        y.FieldMonster.Add(Mapper.Map<Models.Hub.MonsterFieldModel>(item));
                    }
                });

                cfg.CreateMap<Player, Models.Hub.EnemyModel>().AfterMap((x, y) =>
                {
                    y.NbCardDeck = x.Deck.Count;
                    y.NbCardHand = x.PlayerHand.Count;
                    y.FieldMonster = new List<Models.Hub.MonsterFieldModel>();
                    foreach (var item in x.FieldMonster)
                    {
                        y.FieldMonster.Add(Mapper.Map<Models.Hub.MonsterFieldModel>(item));
                    }
                });

                cfg.CreateMap<Game, BoardModel>();

                cfg.CreateMap<CardGame, DBO.Card>();
                cfg.CreateMap<DBO.Card, CardGame>();
            });
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSignalR(builder => builder.MapHub<GameHub>("/game"));

            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "EpiStone V1");
            });
        }
    }
}
