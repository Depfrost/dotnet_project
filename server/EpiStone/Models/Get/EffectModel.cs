﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Models.Get
{
    public class EffectModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public bool TargetHero { get; set; }
        public bool TargetSingleMonster { get; set; }
        public bool TargetAllMonster { get; set; }
    }
}
