﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Models.Hub
{
    public class CardModelGame
    {
        public long SpecialId { get; set; }
        public long Id { get; set; }
        public long EffectId { get; set; }
        public string Type { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public long Life { get; set; }
        public long Attack { get; set; }
        public long Cost { get; set; }
    }
}
