﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Models.Hub
{
    [Serializable]
    public class EnemyModel
    {
        public long PlayerId { get; set; }
        public string HeroImage { get; set; }

        public long Cristals { get; set; }
        public long Life { get; set; }
        public long NbCardDeck { get; set; }
        public long NbCardHand { get; set; }
        public List<MonsterFieldModel> FieldMonster { get; set; }
    }
}
