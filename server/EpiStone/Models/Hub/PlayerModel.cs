﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Models.Hub
{
    [Serializable]
    public class PlayerModel
    {
        public long PlayerId { get; set; }
        public long Cristals { get; set; }
        public string HeroImage { get; set; }
        public long Life { get; set; }
        public long NbCardDeck { get; set; }
        public List<CardModelGame> PlayerHand { get; set; }
        public List<MonsterFieldModel> FieldMonster { get; set; }
    }
}
