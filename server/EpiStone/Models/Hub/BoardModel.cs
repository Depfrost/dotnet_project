﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Models.Hub
{
    [Serializable]
    public class BoardModel
    {
        public long Turns { get; set; }

        public long IncrementId { get; set; }

        public bool PlayerTurn { get; set; }
        public PlayerModel Player { get; set; }
        public EnemyModel Enemy { get; set; }
    }
}
