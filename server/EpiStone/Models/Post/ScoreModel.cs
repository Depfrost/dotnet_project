﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Models.Post
{
    public class ScoreModelPost
    {
        public bool Draw { get; set; }

        public long IdWinner { get; set; }

        public long IdLooser { get; set; }
    }
}
