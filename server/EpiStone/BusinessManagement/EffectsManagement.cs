﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement
{
    public class EffectsManagement : Interfaces.IEffectsManagement
    {
        private DataAccess.Interfaces.IEffects _effects;
        public EffectsManagement(DataAccess.Interfaces.IEffects effects)
        {
            _effects = effects;
        }

        public IEnumerable<DBO.Effect> GetList()
        {
            return _effects.GetList();
        }

        public DBO.Effect GetById(long id)
        {
            return _effects.GetById(id);
        }
    }
}
