﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement
{
    public class DecksManagement : Interfaces.IDecksManagement
    {
        private DataAccess.Interfaces.IDecks _deck;

        public DecksManagement(DataAccess.Interfaces.IDecks deck)
        {
            this._deck = deck;
        }

        public IEnumerable<DBO.Deck> GetList()
        {
            return _deck.GetList();
        }

        public DBO.Deck GetDecks(long id)
        {
            return _deck.GetDeck(id);
        }


        public bool CreateDeck(DBO.Deck deck)
        {
            return _deck.CreateDeck(deck);
        }



        public bool DeleteDeck(long id)
        {
            return _deck.DeleteDeck(id);

        }

        public bool UpdateDeck(DBO.Deck deck, long id)
        {
            return _deck.UpdateDeck(deck, id);
        }

        public IEnumerable<DBO.Deck> GetDeckByUser(long id)
        {
            return _deck.GetDeckByUser(id);
        }
    }
}
