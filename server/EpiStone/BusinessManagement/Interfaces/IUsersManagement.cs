﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement.Interfaces
{
    public interface IUsersManagement
    {
        IEnumerable<DBO.User> GetList();
        DBO.User GetUser(long id);
        bool CreateUser(DBO.User User);
        bool DeleteUser(long id);
        DBO.User ConnexionUsers(string pseudo);
        bool UpdateUser(DBO.User user, long id);
    }
}
