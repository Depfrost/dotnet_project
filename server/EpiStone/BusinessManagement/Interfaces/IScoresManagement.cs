﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement.Interfaces
{
    public interface IScoresManagement
    {
        IEnumerable<DBO.User> GetList();
        DBO.User GetScores(long id);
        bool UpdateScore(DBO.User user, long id);
    }
}
