﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement.Interfaces
{
    public interface IHeroesManagement
    {
        IEnumerable<DBO.Heroe> GetList();
        DBO.Heroe GetHeroe(long id);
    }
}
