﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement.Interfaces
{
    public interface ICardsManagement
    {
        IEnumerable<DBO.Card> GetList();
        DBO.Card GetCard(long id);
    }
}
