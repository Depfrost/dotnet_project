﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement.Interfaces
{
    public interface IDecksManagement
    {
        IEnumerable<DBO.Deck> GetList();
        DBO.Deck GetDecks(long id);
        bool CreateDeck(DBO.Deck deck);
        bool DeleteDeck(long id);
        IEnumerable<DBO.Deck> GetDeckByUser(long id);
        bool UpdateDeck(DBO.Deck deck, long id);
    }
}
