﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement.Interfaces
{
    public interface IEffectsManagement
    {
        IEnumerable<DBO.Effect> GetList();
        DBO.Effect GetById(long id);
    }
}
