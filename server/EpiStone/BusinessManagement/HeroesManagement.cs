﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement
{
    public class HeroesManagement : Interfaces.IHeroesManagement
    {
        private DataAccess.Interfaces.IHeroes _heroes;
        public HeroesManagement(DataAccess.Interfaces.IHeroes heroes)
        {
            _heroes = heroes;
        }

        public IEnumerable<DBO.Heroe> GetList()
        {
            return _heroes.GetList();
        }

        public DBO.Heroe GetHeroe(long id)
        {
            return _heroes.GetById(id);
        }
    }
}
