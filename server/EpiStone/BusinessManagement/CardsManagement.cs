﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement
{
    public class CardsManagement : Interfaces.ICardsManagement
    {
        private DataAccess.Interfaces.ICards _cards;

        public CardsManagement(DataAccess.Interfaces.ICards cards)
        {
            _cards = cards;
        }
        public IEnumerable<DBO.Card> GetList()
        {
            return _cards.GetList().OrderBy(item => item.Cost);
        }

        public DBO.Card GetCard(long id)
        {
            return _cards.GetCard(id);
        }
    }
}
