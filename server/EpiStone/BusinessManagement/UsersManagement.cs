﻿using EpiStone.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement
{
    public class UsersManagement : Interfaces.IUsersManagement
    {
        private DataAccess.Interfaces.IUsers _user;

        public UsersManagement(DataAccess.Interfaces.IUsers user)
        {
            this._user = user;
        }

        public IEnumerable<DBO.User> GetList()
        {
            return _user.GetList();
        }

        public DBO.User GetUser(long id)
        {
            return _user.GetUser(id);
        }


        public bool CreateUser(DBO.User User)
        {
            return _user.CreateUser(User);
        }

      

        public bool DeleteUser(long id)
        {
            return _user.DeleteUser(id);

        }

        public bool UpdateUser(DBO.User user, long id)
        {
            return _user.UpdateUser(user , id);
        }
        
        public DBO.User ConnexionUsers(string pseudo)
        {
            return _user.ConnexionUsers(pseudo);
        }
    }
}
