﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement
{
    public class ScoresManagement : Interfaces.IScoresManagement
    {
        private DataAccess.Interfaces.IScores _score;

        public ScoresManagement(DataAccess.Interfaces.IScores score)
        {
            this._score = score;
        }

        public IEnumerable<DBO.User> GetList()
        {
            return _score.GetList();
        }

        public DBO.User GetScores(long id)
        {
            return _score.GetScores(id);
        }

        public bool UpdateScore(DBO.User user, long id)
        {
            return _score.UpdateScore(user, id);
        }
    }
}
