﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using EpiStone.BusinessManagement.Interfaces;

namespace EpiStone.Controllers
{
    [Produces("application/json")]
    [Route("api/Cards")]
    public class CardsController : Controller
    {
        private ICardsManagement _cards;

        public CardsController(ICardsManagement cards)
        {
            _cards = cards;
        }

        /// <summary>
        /// Get all the cards of the game
        /// </summary>
        /// <returns>A List of CardModel</returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<Models.Get.CardModel>), 200)]
        public IActionResult GetList()
        {
            try
            {
                IEnumerable<DBO.Card> list = _cards.GetList();
                List<Models.Get.CardModel> res = new List<Models.Get.CardModel>();
                foreach (var item in list)
                {
                    res.Add(Mapper.Map<Models.Get.CardModel>(item));
                }
                return Ok(res);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        /// <summary>
        /// Get a specific card with his id
        /// </summary>
        /// <param name="id">The id of the card</param>
        /// <returns>A CardModel corresponding to the parameter id</returns>
        [HttpGet("{id}", Name = "GetCard")]
        [ProducesResponseType(typeof(Models.Get.CardModel), 200)]
        public IActionResult GetCard(long id)
        {
            try
            {
                DBO.Card card = _cards.GetCard(id);
                Models.Get.CardModel res = Mapper.Map<Models.Get.CardModel>(card);
                return Ok(res);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
