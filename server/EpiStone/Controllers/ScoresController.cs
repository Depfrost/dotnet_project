﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EpiStone.BusinessManagement.Interfaces;

namespace EpiStone.Controllers
{
    [Produces("application/json")]
    [Route("api/Scores")]
    public class ScoresController : Controller
    {
        private IScoresManagement _score;


        public ScoresController(IScoresManagement scores)
        {
            this._score = scores;
        }
        /// <summary>
        /// Get the list of score
        /// </summary>
        /// <returns>A List of ScoreModel</returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<Models.Get.ScoreModel>), 200)]
        public IActionResult GetList()
        {
            try
            {
                IEnumerable<DBO.User> list = _score.GetList();
                List<Models.Get.ScoreModel> res = new List<Models.Get.ScoreModel>();
                foreach (var item in list)
                {
                    res.Add(Mapper.Map<Models.Get.ScoreModel>(item));
                }
                return Ok(res);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        /// <summary>
        /// Get the score of a user
        /// </summary>
        /// <param name="id">The id of the user</param>
        /// <returns>A ScoreModel</returns>
        [HttpGet("user/{id}", Name = "GetScore")]
        [ProducesResponseType(typeof(Models.Get.ScoreModel), 200)]
        public IActionResult GetScore(long id)
        {
            try
            {
                DBO.User user = _score.GetScores(id);
                Models.Get.ScoreModel res = Mapper.Map<Models.Get.ScoreModel>(user);
                return Ok(res);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Update the scores of two users
        /// </summary>
        /// <param name="scores">The ScoreModelPost containing all the informations to update the score of two players that finished a game</param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateScores([FromBody] Models.Post.ScoreModelPost scores)
        {
            try
            {
                DBO.User userWinner = _score.GetScores(scores.IdWinner);
                DBO.User userLooser = _score.GetScores(scores.IdLooser);

                if (scores.Draw)
                {
                    userWinner.Draws += 1;
                    userWinner.Score += 2;

                    userLooser.Draws += 1;
                    userLooser.Score += 2;

                }
                else
                {
                    userWinner.Victories += 1;
                    userWinner.Score += 3;

                    userLooser.Defeats += 1;
                    userLooser.Score += 1;
                }
                _score.UpdateScore(userWinner, scores.IdWinner);
                _score.UpdateScore(userLooser, scores.IdLooser);
                return new NoContentResult();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
         
        }

    }
}