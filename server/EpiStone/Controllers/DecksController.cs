﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EpiStone.BusinessManagement.Interfaces;

namespace EpiStone.Controllers
{
    [Produces("application/json")]
    [Route("api/Decks")]
    public class DecksController : Controller
    {
        private IDecksManagement _deck;

        public DecksController(IDecksManagement deck)
        {
            _deck = deck;
        }


        /// <summary>
        /// Get a specific deck  with his id
        /// </summary>
        /// <param name="id">The id of the deck</param>
        /// <returns>A DeckModel corresponding to the parameter id</returns>
        [HttpGet("{id}", Name = "GetDeck")]
        [ProducesResponseType(typeof(Models.Get.DeckModel), 200)]
        public IActionResult GetDeck([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var decks = _deck.GetDecks(id);
            if (decks == null)
            {
                return NotFound();
            }

            Models.Get.DeckModel res = Mapper.Map<Models.Get.DeckModel>(decks);
          

            return Ok(res);
        }

        /// <summary>
        /// Get the list of deck of a user
        /// </summary>
        /// <param name="id">The id of the user</param>
        /// <returns>A List of DeckModel of a specific user</returns>
        [HttpGet("user/{id}")]
        [ProducesResponseType(typeof(List<Models.Get.DeckModel>), 200)]
        public IActionResult GetDeckByUser([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var decks = _deck.GetDeckByUser(id);
            List<Models.Get.DeckModel> res = new List<Models.Get.DeckModel>();
            foreach (var item in decks)
            {
                res.Add(Mapper.Map<Models.Get.DeckModel>(item));
            }
            if (decks == null)
            {
                return NotFound();
            }

            return Ok(res);
        }

        /// <summary>
        /// Create a deck 
        /// </summary>
        /// <param name="decks">The DeckModelPost containing all the informations needed to create a deck </param>
        /// <returns>The list of DeckModel with the newly created Deck for a user</returns>
        [HttpPost]
        [ProducesResponseType(typeof(List<Models.Get.DeckModel>), 200)]
        public IActionResult CreateDeck([FromBody]Models.Post.DeckModelPost decks)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var deck = Mapper.Map<DBO.Deck>(decks);

            var res = _deck.CreateDeck(deck);

            if (res == false)
            {
                return BadRequest();
            }

            var list_decks = _deck.GetDeckByUser(decks.UserId);
            List<Models.Get.DeckModel> list_model = new List<Models.Get.DeckModel>();

            foreach (var item in list_decks)
            {
                list_model.Add(Mapper.Map<Models.Get.DeckModel>(item));
            }
            return Ok(list_model);
        }
        
        /// <summary>
        /// Delete a specific deck
        /// </summary>
        /// <param name="id">The id of the deck</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult DeleteDeck([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_deck.GetDecks(id) == null)
            {
                return NotFound();
            }
            _deck.DeleteDeck(id);

            return new NoContentResult();
        }

        /// <summary>
        /// Update a specific deck
        /// </summary>
        /// <param name="id">The id of the deck</param>
        /// <param name="decks">The DeckModelPost containing all the informations needed to update a deck </param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public ActionResult UpdateDeck([FromRoute] long id, [FromBody] Models.Post.DeckModelPost decks)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (decks == null)
            {
                return BadRequest();
            }

            if (_deck.GetDecks(id) == null)
            {
                return NotFound();
            }

            var deck = Mapper.Map<DBO.Deck>(decks);
            deck.Id = id;
            _deck.UpdateDeck(deck, id);

            return new NoContentResult();
        }
        
    }
}