﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EpiStone.BusinessManagement.Interfaces;

using AutoMapper;
using EpiStone.DBO;

namespace EpiStone.Controllers
{
   
    [Route("api/Users")]
    public class UsersController : Controller
    {
        private IUsersManagement _user;
        private IDecksManagement _decks;

        public UsersController(IUsersManagement user, IDecksManagement decks)
        {
            _user = user;
            _decks = decks;
        }
        
        /// <summary>
        /// Get a specific user with his pseudo and password
        /// </summary>
        /// <param name="pseudo">The pseudo of the user</param>
        /// <param name="password">The password of the user</param>
        /// <returns>A UserModel</returns>
        [HttpGet("{pseudo}/{password}")]
        [ProducesResponseType(typeof(Models.Get.UserModel), 200)]
        public IActionResult ConnexionUsers([FromRoute] string pseudo, [FromRoute] string password)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var users = _user.ConnexionUsers(pseudo);

            
            if (users == null)
            {
                return NotFound("User doesn't exist");
            }
            
            if (users.Password != password)
            {
                return BadRequest("Wrong password");
            }

            Models.Get.UserModel res = Mapper.Map<Models.Get.UserModel>(users);

            return Ok(res);
        }


        // GET: api/Users/5

        /// <summary>
        /// Get a specific user
        /// </summary>
        /// <param name="id">The id of the user</param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetUser")]
        [ProducesResponseType(typeof(Models.Get.UserModel), 200)]
        public IActionResult GetUsers([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var users = _user.GetUser(id);
            if (users == null)
            {
                return NotFound();
            }
            Models.Get.UserModel res = Mapper.Map<Models.Get.UserModel>(users);
            return Ok(res);
        }

        
        // POST: api/Users

        /// <summary>
        /// Create a user
        /// </summary>
        /// <param name="users">The UserModelPost containing all the informations to create a new user</param>
        /// <returns>The newly created User</returns>
        [HttpPost]
        public IActionResult PostUsers([FromBody]Models.Post.UserModelPost users)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = Mapper.Map<User>(users);
            var res = _user.CreateUser(user);
          
            if (res == false)
            {
                return BadRequest("Pseudo Already Exist");
            }

            var user_created = Mapper.Map<Models.Get.UserModel>(_user.ConnexionUsers(users.Pseudo));
            Deck deck = new Deck()
            {
                UserId = user_created.Id,
                Name = "Default Deck",
                HeroId = 1,
                Card1 = 10,
                Card9 = 10,
                Card8 = 1,
                Card7 = 1,
                Card6 = 2,
                Card5 = 2,
                Card4 = 3,
                Card10 = 3,
                Card11 = 5,
                Card12 = 5,
                Card13 = 6,
                Card14 = 8,
                Card15 = 8,
                Card16 = 7,
                Card17 = 7,
                Card18 = 21,
                Card19 = 21,
                Card2 = 11,
                Card20 = 11,
                Card21 = 12,
                Card22 = 12,
                Card23 = 13,
                Card24 = 13,
                Card25 = 4,
                Card3 = 4
            };
            _decks.CreateDeck(deck);
            return Ok(user_created);
        }

        // DELETE: api/Users/5

        /// <summary>
        /// Delete a specific user
        /// </summary>
        /// <param name="id">The id of the user</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
      
        public IActionResult DeleteUsers([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
             
            if (_user.GetUser(id) == null)
            {
                return NotFound();
            }
            _user.DeleteUser(id);

            return new NoContentResult();
        }

        // PUT: api/Users/5

        /// <summary>
        /// Update a specific user
        /// </summary>
        /// <param name="id">The id of the user</param>
        /// <param name="users">The UserModelPost containing all the informations to update user</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult PutUsers([FromRoute] long id, [FromBody] Models.Post.UserModelPost users)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (users == null)
            {
                return BadRequest();
            }

            if (_user.GetUser(id) == null)
            {
                return NotFound();
            }
            var user = Mapper.Map<User>(users);
            user.Id = id;
            var res = _user.UpdateUser(user, id);
            if (res == false)
            {
                return BadRequest("Pseudo Already Exist");
            }

            return new NoContentResult();
        }
    }
}