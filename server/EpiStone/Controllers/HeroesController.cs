﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using EpiStone.BusinessManagement.Interfaces;

namespace EpiStone.Controllers
{
    [Produces("application/json")]
    [Route("api/Heroes")]
    public class HeroesController : Controller
    {

        private IHeroesManagement _heroes;
        public HeroesController(IHeroesManagement heroes)
        {
            _heroes = heroes;
        }

        // GET: api/Heroes

        /// <summary>
        /// Get the list of heroes in the game
        /// </summary>
        /// <returns>A List of HeroeModel</returns>        
        [HttpGet]
        [ProducesResponseType(typeof(List<Models.Get.HeroeModel>), 200)]
        public IActionResult GetList()
        {
            try
            {
                IEnumerable<DBO.Heroe> list = _heroes.GetList();
                List<Models.Get.HeroeModel> res = new List<Models.Get.HeroeModel>();
                foreach (var item in list)
                {
                    res.Add(Mapper.Map<Models.Get.HeroeModel>(item));
                }
                return Ok(res);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        // GET: api/Heroes/5

        /// <summary>
        /// Get a specific heroe
        /// </summary>
        /// <param name="id">The id of the heroe</param>
        /// <returns>A List of HeroeModel</returns>        
        [HttpGet("{id}", Name = "GetHeroe")]
        [ProducesResponseType(typeof(List<Models.Get.HeroeModel>), 200)]
        public IActionResult GetHeroe(long id)
        {
            try
            {
                DBO.Heroe heroe = _heroes.GetHeroe(id);
                Models.Get.HeroeModel res = Mapper.Map<Models.Get.HeroeModel>(heroe);
                return Ok(res);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
