﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using EpiStone.BusinessManagement.Interfaces;

namespace EpiStone.Controllers
{
    [Produces("application/json")]
    [Route("api/Effects")]
    public class EffectsController : Controller
    {
        private IEffectsManagement _effects;

        public EffectsController(IEffectsManagement effects)
        {
            _effects = effects;
        }

        /// <summary>
        /// Get the list of effects in the game 
        /// </summary>
        /// <returns>A List of EffectModel</returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<Models.Get.EffectModel>), 200)]
        public IActionResult GetList()
        {
            try
            {
                IEnumerable<DBO.Effect> effects = _effects.GetList();
                List<Models.Get.EffectModel> res = new List<Models.Get.EffectModel>();
                foreach (var item in effects)
                {
                    res.Add(Mapper.Map<Models.Get.EffectModel>(item));
                }
                return Ok(res);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        /// <summary>
        /// Get a specific effect 
        /// </summary>
        /// <param name="id">The id of the effect</param>
        /// <returns>A EffectModel </returns>
        [HttpGet("{id}", Name = "GetEffect")]
        [ProducesResponseType(typeof(Models.Get.EffectModel), 200)]
        public IActionResult GetById(long id)
        {
            try
            {
                DBO.Effect effect = _effects.GetById(id);
                return Ok(Mapper.Map<Models.Get.EffectModel>(effect));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
