﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Hub.Interfaces
{
    public interface IGameHub
    {
        /// <summary>
        /// Handler for looking for an enemy
        /// </summary>
        /// <param name="user_id">The id of the caller</param>
        /// <param name="deck_id">The id of the deck the player has chosen</param>
        /// <returns></returns>
        Task FindGame(long user_id, long deck_id);

        /// <summary>
        /// Handler for leaving the matchmaking
        /// </summary>
        /// <param name="user_id">The id of the caller</param>
        /// <returns></returns>
        Task Disconnect(long user_id);

        /// <summary>
        /// Handler for ending turn in game
        /// </summary>
        /// <param name="id">The id of the game</param>
        /// <param name="user_id">The id of the user that end his turn</param>
        /// <returns></returns>
        Task EndTurn(string id, long user_id);

        /// <summary>
        /// Handler for playing a monster in game
        /// </summary>
        /// <param name="id">The id of the game</param>
        /// <param name="user_id">The id of the player that plays monster</param>
        /// <param name="idMonster">The id of the monster invoked</param>
        /// <param name="idSpecial">The unique id of the monster invoked</param>
        /// <param name="targetSpecialId">The unique id of the effect target if needed, -1 is no target</param>
        /// <returns></returns>
        Task PlayMonster(string id, long user_id, long idMonster, long idSpecial, long targetSpecialId = -1);

        /// <summary>
        /// Handler for attacking in game
        /// </summary>
        /// <param name="id">The id of the game</param>
        /// <param name="user_id">The id of the player whose attacking</param>
        /// <param name="specialIdAttack">The unique id of the monster that attacks</param>
        /// <param name="specialIdDefense">The unique id of the cards attacked, -1 is the enemy hero</param>
        /// <returns></returns>
        Task Attack(string id, long user_id, long specialIdAttack, long specialIdDefense = -1);

        /// <summary>
        /// Handler for playing a spell in game
        /// </summary>
        /// <param name="id">The id of the game</param>
        /// <param name="user_id">The id of the player whose playing a spell</param>
        /// <param name="specialIdCard">The unique id of the spell played</param>
        /// <param name="targetSpecialId">The unique id of the target if needed, -1 is the enemy hero</param>
        /// <returns></returns>
        Task PlaySpell(string id, long user_id, long specialIdCard, long targetSpecialId = -1);

        /// <summary>
        /// Handler for giving up in game
        /// </summary>
        /// <param name="id">The id of the game</param>
        /// <param name="user_id">The id of the guy that gives up</param>
        /// <returns></returns>
        Task GiveUp(string id, long user_id);

        /// <summary>
        /// Handler for getting updated board
        /// </summary>
        /// <param name="id">The id of the game</param>
        /// <returns></returns>
        Task GetBoardState(string id);

    }
}
