﻿using EpiStone.Hub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Hub.Interfaces
{
    public interface IGameState
    {
        /// <summary>
        /// Handler that init a game with two players, each player is configured
        /// with their life, crystal, deck.
        /// </summary>
        /// <param name="playerTurn"> Integer used to know who is the player that will begin the game</param>
        /// <param name="player1"> Information about the first player</param>
        /// <param name="player2">Information about the second player</param>
        /// <returns> A object Game that contain all the informations about the game (players, monsters, cards, decks)</returns>

        Game GameStart(int playerTurn, PlayerConnect player1, PlayerConnect player2);

        /// <summary>
        /// Handler called by a player to begin his turn, this function will set the number of crystals
        /// and the draw of card in his deck.
        /// </summary>
        /// <param name="game"> A object Game that contain all the informations about the curent game (players, monsters, cards, decks)</param>
        void StartTurn(Game game);

        /// <summary>
        /// Handler called by a player to end his turn, this function will set the status of the monsters
        /// of the player to attack for the next turn.
        /// </summary>
        /// <param name="game"> A object Game that contain all the informations about the current game (players, monsters, cards, decks)</param>

        void EndTurn(Game game);

        /// <summary>
        /// Handler Attack is called when a monster of a player wants to attack a monster of the hero of the other player
        /// The damage is applied to the current target also to the monster who attacked.
        /// </summary>
        /// <param name="game"> A object Game that contain all the informations about the current game (players, monsters, cards, decks)</param>
        /// <param name="positionAttackId"> The id of the monster launch an attack </param>
        /// <param name="positionDefenseId"> The id of the target, it is set to -1 if it is the hero </param>
        void Attack(Game game, long positionAttackId, long positionDefenseId);

        /// <summary>
        /// Handler called when a monster appear on the board, it will 
        /// create a new Object Monster if there is enough space on his board. 
        /// If the monster has an effect, the method ApplyEffect will be called to launch his effect.
        /// </summary>
        /// <param name="game">A object Game that contain all the informations about the current game (players, monsters, cards, decks)</param>
        /// <param name="idMonster"> The id of the monster in the database</param>
        /// <param name="player"> The player that launched the invocation</param>
        /// <param name="IdSpecial">The special Id of the monster</param>
        /// <param name="targetId">The id of a possible target from the effect of the monster</param>
        void InvocationMonster(Game game, long idMonster, Player player, long IdSpecial, long targetId);

        /// <summary>
        /// Function called when a spell is used, apply the cost of crystal to call the spell
        /// before calling the method ApplyEffect will be called to launch the effect of the spell.
        /// </summary>
        /// <param name="game">A object Game that contain all the informations about the current game (players, monsters, cards, decks)</param>
        /// <param name="cardId">The id of the spell </param>
        /// <param name="targetId">The id of the target of the spell, it is set to -1 if the target is the hero</param>
        void Spell(Game game,long cardId, long targetId);

        /// <summary>
        /// Handler used when a player wants to abandon the current game, the number of victories
        /// , defeats and score are updated.
        /// </summary>
        /// <param name="game">A object Game that contain all the informations about the current game (players, monsters, cards, decks)</param>
        /// <param name="player_id">The id of the player that wants to abandon</param>
        /// 
        void Abandon(Game game, long player_id);
    }
}
