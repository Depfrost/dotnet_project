﻿
using EpiStone.DataAccess;
using EpiStone.Hub.Interfaces;
using EpiStone.Hub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EpiStone.DataAccess.Interfaces;
using AutoMapper;

namespace EpiStone.Hub
{
    public class GameState : IGameState
    {
        private IDecks deckAccess;
        private ICards cardsAccess;
        private IHeroes heroesAccess;
        private IEffects effectsAccess;
        private IScores scoresAccess;

        public GameState(IDecks decks, IHeroes heroes, ICards cards, IEffects effects, IScores scores)
        {
            deckAccess = decks;
            heroesAccess = heroes;
            cardsAccess = cards;
            effectsAccess = effects;
            scoresAccess = scores;
        }

       
        public Game GameStart(int playerTurn, PlayerConnect player1, PlayerConnect player2)
        {
            Game game = new Game
            {
                PlayerTurn = playerTurn,
                IncrementId = 0,
                Turns = 0,
                Player1 = new Player()
                {
                    PlayerId = player1.Id
                },
                Player2 = new Player()
                {
                    PlayerId = player2.Id
                }
            };

            InitPlayer(game.Player1);
            InitPlayer(game.Player2);

            InitDeckPlayer(game, game.Player1, player1.DeckId);
            InitDeckPlayer(game, game.Player2, player2.DeckId);

            InitHandPlayer(game.Player1);
            InitHandPlayer(game.Player2);

            return game;
           
        }

        #region InitPlayer

        private void InitPlayer(Player player)
        {
            player.Life = 30;
            player.Cristals = 0;
            player.PenalityDraw = 1;
            player.PlayerHand = new List<CardGame>();
            player.Deck = new Stack<CardGame>();
          
            player.FieldMonster = new Monster[5];
            for (int i = 0; i < 0; i++)
            {
                player.FieldMonster[i] = null;
            }
        }

        private void InitDeckPlayer(Game game, Player player,  long idDeck)
        {
            var deck = deckAccess.GetDeck(idDeck);
            List<DBO.Card> cards = new List<DBO.Card>();
            var heroPlayer = heroesAccess.GetById(deck.HeroId);

            player.HeroImage = heroPlayer.Image;

            cards.Add(cardsAccess.GetCard(heroPlayer.Card1));
            cards.Add(cardsAccess.GetCard(heroPlayer.Card2));
            cards.Add(cardsAccess.GetCard(heroPlayer.Card3));
            cards.Add(cardsAccess.GetCard(heroPlayer.Card4));
            cards.Add(cardsAccess.GetCard(heroPlayer.Card5));

            cards.Add(cardsAccess.GetCard(deck.Card1));
            cards.Add(cardsAccess.GetCard(deck.Card2));
            cards.Add(cardsAccess.GetCard(deck.Card3));
            cards.Add(cardsAccess.GetCard(deck.Card4));
            cards.Add(cardsAccess.GetCard(deck.Card5));
            cards.Add(cardsAccess.GetCard(deck.Card6));
            cards.Add(cardsAccess.GetCard(deck.Card7));
            cards.Add(cardsAccess.GetCard(deck.Card8));
            cards.Add(cardsAccess.GetCard(deck.Card9));
            cards.Add(cardsAccess.GetCard(deck.Card10));
            cards.Add(cardsAccess.GetCard(deck.Card11));
            cards.Add(cardsAccess.GetCard(deck.Card12));
            cards.Add(cardsAccess.GetCard(deck.Card13));
            cards.Add(cardsAccess.GetCard(deck.Card14));
            cards.Add(cardsAccess.GetCard(deck.Card15));
            cards.Add(cardsAccess.GetCard(deck.Card16));
            cards.Add(cardsAccess.GetCard(deck.Card17));
            cards.Add(cardsAccess.GetCard(deck.Card18));
            cards.Add(cardsAccess.GetCard(deck.Card19));
            cards.Add(cardsAccess.GetCard(deck.Card20));
            cards.Add(cardsAccess.GetCard(deck.Card21));
            cards.Add(cardsAccess.GetCard(deck.Card22));
            cards.Add(cardsAccess.GetCard(deck.Card23));
            cards.Add(cardsAccess.GetCard(deck.Card24));
            cards.Add(cardsAccess.GetCard(deck.Card25));

            Random random = new Random();
            cards = cards.OrderBy(item => random.Next()).ToList();

            cards.ForEach(item =>
            {
                var cardGame = Mapper.Map<CardGame>(item);
                game.IncrementId++;
                cardGame.SpecialId = game.IncrementId;
                player.Deck.Push(cardGame);
            }
            );

        }
        
        private void InitHandPlayer(Player player)
        {
            for (int i = 0; i < 5; i++)
            {
                player.PlayerHand.Add(player.Deck.Pop());
            }
        }
        #endregion

       
        public void Abandon(Game game, long player_id)
        {
            if (game.Player1.PlayerId == player_id)
            {
                UpdateScore(game.Player2.PlayerId, game.Player1.PlayerId);
                game.Player1.Life = 0;
            }
            else
            {
                UpdateScore(game.Player1.PlayerId, game.Player2.PlayerId);
                game.Player2.Life = 0;
            }
        }

        public void StartTurn(Game game)
        {
            if (game.PlayerTurn == 1)
            {
                game.Player1.Cristals = game.Turns < 10 ? game.Turns : 10;
                if (game.Turns < 10)
                {
                    game.Player1.Cristals += 1;
                }

                Draw(game, game.Player1);
            }
            else
            {
                game.Player2.Cristals = game.Turns < 10 ? game.Turns : 10;
                if (game.Turns < 10)
                {
                    game.Player2.Cristals += 1;
                }

                Draw(game, game.Player2);
            }
        }

       
        public void EndTurn(Game game)
        {

            if (game.PlayerTurn == 2)
            {
                // Increment game turns
                game.Turns += 1;
                // Monsters can now attack in Player2 next turn
                foreach (Monster monster in game.Player2.FieldMonster)
                {
                    if (monster != null)
                    {
                        if (monster.Status.Equals("Gele"))
                        {
                            monster.Status = "Aucun";
                        }

                        monster.CanAttack = true;
                    }
                }

            }
            else
            {
                // Monsters can now attack in Player1 next turn
                foreach (Monster monster in game.Player1.FieldMonster)
                {
                    if (monster != null)
                    {
                        if (monster.Status.Equals("Gele"))
                        {
                            monster.Status = "Aucun";
                            monster.CanAttack = false;
                        }
                        else
                        {
                            monster.CanAttack = true;
                        }
                    }
                }
            }

            game.PlayerTurn = game.PlayerTurn == 1 ? 2 : 1;

        }

       
        public void Attack(Game game, long positionAttackId, long positionDefenseId)
        {


            if (positionDefenseId == -1)
            {

                if (game.PlayerTurn == 1)
                {

                    var monster = Array.Find(game.Player1.FieldMonster, elt => (elt != null && elt.SpecialId == positionAttackId));
                    game.Player2.Life -= monster.Attack;
                    Array.Find(game.Player1.FieldMonster, elt => (elt != null && elt.SpecialId == positionAttackId)).CanAttack = false;
                }
                else
                {
                    var monster = Array.Find(game.Player2.FieldMonster, elt => (elt != null && elt.SpecialId == positionAttackId));
                    game.Player1.Life -= monster.Attack;
                    Array.Find(game.Player2.FieldMonster, elt => (elt != null && elt.SpecialId == positionAttackId)).CanAttack = false;
                }
            }
            else
            {


                Monster monsterAttacker;
                Monster monsterDefense;

                if (game.PlayerTurn == 1)
                {
                    monsterAttacker = Array.Find(game.Player1.FieldMonster, elt => (elt != null && elt.SpecialId == positionAttackId));
                    monsterDefense = Array.Find(game.Player2.FieldMonster, elt => (elt != null && elt.SpecialId == positionDefenseId));

                    
                    if (monsterDefense.Status.Equals("Bouclier"))
                    {
                        Array.Find(game.Player2.FieldMonster, elt => (elt != null && elt.SpecialId == positionDefenseId)).Status = "Aucun";
                    }
                    else
                    {
                        Array.Find(game.Player2.FieldMonster, elt => (elt != null && elt.SpecialId == positionDefenseId)).Life -= monsterAttacker.Attack;
                    }

                    if (monsterAttacker.Status.Equals("Bouclier"))
                    {
                        Array.Find(game.Player1.FieldMonster, elt => (elt != null && elt.SpecialId == positionAttackId)).Status = "Aucun";
                    }
                    else
                    {
                        Array.Find(game.Player1.FieldMonster, elt => (elt != null && elt.SpecialId == positionAttackId)).Life -= monsterDefense.Attack;
                    }
                    Array.Find(game.Player1.FieldMonster, elt => (elt != null && elt.SpecialId == positionAttackId)).CanAttack = false;
                }
                else
                {
                    monsterAttacker = Array.Find(game.Player2.FieldMonster, elt => (elt != null && elt.SpecialId == positionAttackId));
                    monsterDefense = Array.Find(game.Player1.FieldMonster, elt => (elt != null && elt.SpecialId == positionDefenseId));
                   
                    if (monsterDefense.Status.Equals("Bouclier"))
                    {
                        Array.Find(game.Player1.FieldMonster, elt => (elt != null && elt.SpecialId == positionDefenseId)).Status = "Aucun";
                    }
                    else
                    {
                        Array.Find(game.Player1.FieldMonster, elt => (elt != null && elt.SpecialId == positionDefenseId)).Life -= monsterAttacker.Attack;

                    }

                    if (monsterAttacker.Status.Equals("Bouclier"))
                    {
                        Array.Find(game.Player2.FieldMonster, elt => (elt != null && elt.SpecialId == positionAttackId)).Status = "Aucun";
                    }
                    else
                    {
                        Array.Find(game.Player2.FieldMonster, elt => (elt != null && elt.SpecialId == positionAttackId)).Life -= monsterDefense.Attack;
                    }
                    Array.Find(game.Player2.FieldMonster, elt => (elt != null && elt.SpecialId == positionAttackId)).CanAttack = false;
                }


            }

            Update(game);
        }


       
        public void InvocationMonster(Game game, long idMonsterCard, Player player, long IdSpecial, long positionTargetId)
        {
            bool checkHand = player.PlayerHand.Exists(card => card.Id == idMonsterCard);

            if (checkHand)
            {
                var getMonster = cardsAccess.GetCard(idMonsterCard);
                getMonster.EffectId = getMonster.EffectId == null ? -1 : getMonster.EffectId.Value;

                if (player.Cristals >= getMonster.Cost)
                {

                    var res = player.PlayerHand.Find(x => x.SpecialId == IdSpecial);
                    if (res != null)
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            if (player.FieldMonster[i] == null)
                            {
                                player.FieldMonster[i] = new Monster(idMonsterCard, getMonster.Life.Value, getMonster.Attack.Value, getMonster.Cost, res.SpecialId, getMonster.EffectId.Value, getMonster.Name, getMonster.Image);
                                break;
                            }
                            else if (i == 4)
                            {
                                // No space on board, invocation aborted
                                return;
                            }
                        }

                        // Discard from hand
                        player.PlayerHand.Remove(res);

                        player.Cristals -= getMonster.Cost;


                        if (getMonster.EffectId != -1)
                        {
                            ApplyEffect(game, getMonster.EffectId.Value, IdSpecial, positionTargetId);
                        }

                        Update(game);
                    }
                }
            }
        }

       
        private void ApplyEffect(Game game, long effectId, long idEffectMonster, long targetId)
        {
            DBO.Effect spell = effectsAccess.GetById(effectId);

            if (FindTarget(game, spell, targetId, idEffectMonster) == 1)
                SpellActivation(game, game.Player1, spell, targetId, idEffectMonster);
            else
                SpellActivation(game, game.Player2, spell, targetId, idEffectMonster);

            Update(game);
        }

       
        public void Spell(Game game, long cardId, long targetId)
        {
            CardGame card;
            if (game.PlayerTurn == 1)
            {
                card = game.Player1.PlayerHand.Find(x => x.SpecialId == cardId);
            }
            else
            {
                card = game.Player2.PlayerHand.Find(x => x.SpecialId == cardId);
            }


            if (card.Type == 1)
            {
                if (game.PlayerTurn == 1)
                {
                    game.Player1.PlayerHand.Remove(card);
                    PayCostCrystal(game.Player1, card.Cost);
                }
                else
                {
                    game.Player2.PlayerHand.Remove(card);
                    PayCostCrystal(game.Player2, card.Cost);
                }
            }

            // Apply the card effect and update the game
            ApplyEffect(game, card.EffectId.Value, -1, targetId);
        }

        #region utility

        private void UpdateScore(long winnerId, long looserId)
        {
            DBO.User userWinner = scoresAccess.GetScores(winnerId);
            DBO.User userLooser = scoresAccess.GetScores(looserId);

            userWinner.Victories += 1;
            userWinner.Score += 3;

            userLooser.Defeats += 1;
            userLooser.Score += 1;

           scoresAccess.UpdateScore(userWinner, winnerId);
           scoresAccess.UpdateScore(userLooser, looserId);
        }

        private void Draw(Game game, Player player)
        {
            if (player.Deck.Count != 0)
            {
                var card = player.Deck.Pop();
                if (player.PlayerHand.Count != 10)
                {
                    player.PlayerHand.Add(card);
                }
            }
            else
            {
                player.Life -= ++player.PenalityDraw;
            }
            Update(game);
        }

        private void Update(Game game)
        {
            for  (int i = 0; i < 5; i++)
            {
                if (game.Player1.FieldMonster[i] != null && game.Player1.FieldMonster[i].Life <= 0)
                {
                    game.Player1.FieldMonster[i] = null;
                }

                if (game.Player2.FieldMonster[i] != null && game.Player2.FieldMonster[i].Life <= 0)
                {
                    game.Player2.FieldMonster[i] = null;
                }

            }

            if (game.Player1.Life <= 0)
            {
                UpdateScore(game.Player2.PlayerId, game.Player1.PlayerId);
            }
            if (game.Player2.Life <= 0)
            {
                UpdateScore(game.Player1.PlayerId, game.Player2.PlayerId);
            }
        }

        private long FindTarget(Game game, DBO.Effect spell, long targetId, long idEffectMonster)
        {
            if (spell.Type.Equals("Pioche"))
            {
                return game.PlayerTurn == 1 ? 1 : 2;
            }

            if (spell.Type.Equals("Bouclier") || spell.Type.Equals("Charge") || spell.Type.Equals("Provoc")) 
            {
                if (targetId == -1)
                {
                    return game.PlayerTurn == 1 ? 1 : 2;
                }
                else
                {
                    bool res = Array.Exists(game.Player1.FieldMonster, elt => (elt != null && elt.SpecialId == targetId));
                    return res == true ? 1 : 2;
                }
            }

            if (spell.Type.Equals("Mort") || spell.Type.Equals("Gele")) 
            {
                if (spell.TargetAllMonster == 1)
                {
                    return game.PlayerTurn == 1 ? 2 : 1;
                }
                else
                {
                    bool res =  Array.Exists(game.Player1.FieldMonster, elt => (elt != null && elt.SpecialId == targetId));
                    return res == true ? 1 : 2;
                }
            }

             if (spell.Type.Equals("Degats")) 
            {
                if (spell.TargetAllMonster == 1 || spell.TargetHero == 1)
                {
                    return game.PlayerTurn == 1 ? 2 : 1;
                }
                else
                {
                    bool res = Array.Exists(game.Player1.FieldMonster, elt => (elt != null && elt.SpecialId == targetId));
                    return res == true ? 1 : 2;
                }
            }

            if (spell.Type.Equals("Boost"))
            {
                if (spell.TargetAllMonster == 1 || spell.TargetHero == 1)
                {
                    return game.PlayerTurn == 1 ? 1 : 2;
                }
                else
                {
                    bool res = Array.Exists(game.Player1.FieldMonster, elt => (elt != null && elt.SpecialId == targetId));
                    return res == true ? 1 : 2;
                }
            }

            return 0;
        }

        private void SpellActivation(Game game, Player player, DBO.Effect spell, long targetId, long idEffectMonster)
        {
                  
            switch (spell.Type)
            {
                case "Degats":
                    SpellDegat(game, player, spell, targetId);
                    break;

                case "Gele":
                    SpellGele(player, spell, targetId);
                    break;
                case "Mort":
                    SpellMort(player, spell, targetId);
                    break;
                case "Boost":
                    SpellBoost(player, spell, targetId, idEffectMonster);
                    break;

                case "Charge":
                    SpellCharge(player, spell, targetId, idEffectMonster);
                    break;

                case "Bouclier":
                    SpellBouclier(player, spell, targetId, idEffectMonster);
                    break;
                case "Pioche":
                    SpellPioche(game, player, spell);
                    break;
                case "Provoc":
                    SpellProvoc(player, spell, targetId, idEffectMonster);
                    break;

                default:
                    break;
            }
           
        }
    
       
        private void PayCostCrystal (Player player, long cost)
        {
            player.Cristals -= cost;
        }

       
        private void ChangeStatutMonster(Monster monster, string statut)
        {
            monster.Status = statut;
        }

        #endregion
        
        #region spells
        private void SpellProvoc(Player player, DBO.Effect spell, long targetId, long idEffectMonster)
        {
            if (targetId != -1)
                ChangeStatutMonster(Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)), "Provoc");
            else
                ChangeStatutMonster(Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == idEffectMonster)), "Provoc");
        }


      
        private void SpellBoost(Player player, DBO.Effect spell, long targetId,  long idEffectMonster)
        {
            if (spell.TargetAllMonster == 1)
            {
                for (int i = 0; i < 5; i++)
                {
                    if (player.FieldMonster[i] != null && player.FieldMonster[i].SpecialId != idEffectMonster)
                    {
                        ApplyValueMonster(player, spell.Value, player.FieldMonster[i].SpecialId);
                    }
                }
            }
            else if (spell.TargetHero == 1)
            {
                long life = Convert.ToInt64(spell.Value);
                player.Life += life;
            }
            else if (spell.TargetSingleMonster == 1)
            {
                ApplyValueMonster(player, spell.Value, targetId);
            }
        }

        
        private void ApplyValueMonster(Player player, string value, long targetId)
        {
            var values = value.Split('/');

            string valueAttack = values[0];
            string valueLife = values[1];

            bool addAttack = false;
            bool addLife = false;

            if (valueAttack.Contains('+'))
            {
                valueAttack.Substring(1);
                addAttack = true;

            }
            if (valueLife.Contains('+'))
            {
                valueLife.Substring(1);
                addLife = true;
            }

            var attack = Convert.ToInt64(valueAttack);

            if (addAttack)
            {
                Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)).Attack += attack;
            }
            else
            {
                Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)).Attack = attack;
            }

            var life = Convert.ToInt64(valueLife);
            if (addLife)
            {
                Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)).Life += life;
            }
            else
            {
                Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)).Life = life;
            }
        }

       
        private void SpellDegat(Game game, Player player, DBO.Effect spell, long targetId)
        {
            if (spell.TargetAllMonster == 1)
            {
                foreach (var monster in player.FieldMonster)
                {
                    if (monster != null)
                    {
                        if (monster.Status.Equals("Bouclier"))
                        {
                            monster.Status = "Aucun";
                        }
                        else
                        {
                            long dmg = Convert.ToInt64(spell.Value);
                            monster.Life -= dmg;
                        }
                    }
                }
            }
            else if (spell.TargetHero == 1 && spell.TargetSingleMonster == 1)
            {
                if (targetId == -1)
                {
                    long dmg = Convert.ToInt64(spell.Value);
                    player.Life -= dmg;
                }
                else
                {
                    if (Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)).Status.Equals("Bouclier"))
                    {
                        Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)).Status = "Aucun";
                    }
                    else
                    {
                        long dmg = Convert.ToInt64(spell.Value);
                        Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)).Life -= dmg;
                    }
                     
                }
            }
            else if (spell.TargetHero == 1)
            {
                long dmg = Convert.ToInt64(spell.Value);
                player.Life -= dmg;
            }
            else if (spell.TargetSingleMonster == 1)
            {
                if (Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)).Status.Equals("Bouclier"))
                {
                    Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)).Status = "Aucun";
                }
                else
                {
                    long dmg = Convert.ToInt64(spell.Value);
                    Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)).Life -= dmg;
                }
            }

            else
            {
                // Handle Monster that cost 3 life points

                if (game.PlayerTurn == 1)
                {
                    long dmg = Convert.ToInt64(spell.Value);
                    game.Player1.Life -= dmg;
                }
                else
                {
                    long dmg = Convert.ToInt64(spell.Value);
                    game.Player2.Life -= dmg;
                }
            }
        }

       
        private void SpellPioche(Game game, Player player, DBO.Effect spell)
        {
            long numberDraw = Convert.ToInt64(spell.Value);

            for (int i = 0; i < numberDraw; i++)
            {
                Draw(game, player);
            }
        }

        private void SpellBouclier(Player player, DBO.Effect spell, long targetId, long idEffectMonster)
        {
            if (targetId != -1)
                ChangeStatutMonster(Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)), "Bouclier");
            else
                ChangeStatutMonster(Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == idEffectMonster)), "Bouclier");
        }

        private void SpellCharge(Player player, DBO.Effect spell, long targetId, long idEffectMonster)
        {
            if (targetId != -1)
            {
                Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)).CanAttack = true;
                ChangeStatutMonster(Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)), "Charge");
            }
            else
            {
                Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == idEffectMonster)).CanAttack = true;
                ChangeStatutMonster(Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == idEffectMonster)), "Charge");
            }
        }

       
        private void SpellMort(Player player, DBO.Effect spell, long targetId)
        {
            if (spell.TargetAllMonster == 1)
            {
                foreach (var monster in player.FieldMonster)
                {
                    if (monster != null)
                    {
                        monster.Life = 0;
                    }
                }
            }
            else
            {
                Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)).Life = 0;
            }
        }

       
        private void SpellGele(Player player, DBO.Effect spell, long targetId)
            {
            if (spell.TargetAllMonster == 1)
            {
                foreach (var monster in player.FieldMonster)
                {
                    if (monster != null)
                    {
                        monster.CanAttack = false;
                        ChangeStatutMonster(monster, "Gele");

                    }
                }
            }
            else
            {
                Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)).CanAttack = false;
                ChangeStatutMonster(Array.Find(player.FieldMonster, elt => (elt != null && elt.SpecialId == targetId)), "Gele");
            }
        }

        #endregion

    }
}
