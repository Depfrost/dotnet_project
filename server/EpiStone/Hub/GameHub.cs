﻿using EpiStone.DataAccess;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EpiStone.Models.Hub;
using Newtonsoft.Json;
using EpiStone.Hub.Models;
using EpiStone.Hub.Business.Interfaces;

namespace EpiStone.Hub
{
    public class GameHub : Microsoft.AspNetCore.SignalR.Hub, Interfaces.IGameHub
    {
        private Interfaces.IGameState _newgame;
        private IGamesManagement _games;
        private IPlayersManagement _players;
        private IWaitingListManagement _waitingList;

        public GameHub(Interfaces.IGameState newGame, IGamesManagement games, IWaitingListManagement waitinglist, IPlayersManagement player)
        {
            _newgame = newGame;
            _games = games;
            _waitingList = waitinglist;
            _players = player;
        }
        public async Task Disconnect(long user_id)
        {
            try
            {
                bool find = _waitingList.FindPlayerById(user_id);
                if (find)
                {
                    PlayerConnect newPlayer = new PlayerConnect()
                    {
                        isConnected = false,
                        ConnectionId = Context.ConnectionId,
                        Id = user_id
                    };
                    _waitingList.UpdateToWaitingList(newPlayer);
                }
            }
            catch (Exception e)
            {
                await Clients.Caller.SendAsync(Settings.Error, e.Message);
            }
        }
        public async Task FindGame(long user_id, long deck_id)
        {
            try
            {
                PlayerConnect player = new PlayerConnect()
                {
                    ConnectionId = Context.ConnectionId,
                    DeckId = deck_id,
                    Id = user_id,
                    isConnected = true
                };
                // Add Player to the empty queue
                if (_waitingList.IsEmpty())
                {
                    _waitingList.AddToWaitingList(player);
                    await Clients.Caller.SendAsync(Settings.WaitingList, null);
                    return;
                }
                // Update the player if already in the queue
                bool find = _waitingList.FindPlayerById(user_id);
                if (find)
                {
                    _waitingList.UpdateToWaitingList(player);
                    return;
                }
                PlayerConnect enemy = _waitingList.GetPlayerFromWaitingList();
                // Add if error in get enemy
                if (!enemy.isConnected)
                {
                    _waitingList.AddToWaitingList(player);
                    return;
                }
                string id = enemy.Id + "/" + player.Id;
                Game game = _newgame.GameStart(1, enemy, player);
                _newgame.StartTurn(game);
                _games.AddToGame(id, game);
                _players.AddPlayers(id, new PlayerConnect[] { enemy, player });

                await Clients.Caller.SendAsync(Settings.FindEnemy, id, enemy.Id, false);
                await Clients.Client(enemy.ConnectionId).SendAsync(Settings.FindEnemy, id, user_id, true);
            }
            catch (Exception e)
            {
                await Clients.Caller.SendAsync(Settings.Error, e.Message);
            }
        }
        public async Task EndTurn(string id, long user_id)
        {
            try
            {
                Game game = _games.GetFromGame(id);
                _newgame.EndTurn(game);
                _newgame.StartTurn(game);
                await GetBoardState(id);
            }
            catch (Exception e)
            {
                await Clients.Caller.SendAsync(Settings.Error, e.Message);
            }
        }
        public async Task PlayMonster(string id, long user_id, long idMonster, long idSpecial, long targetSpecialId = -1)
        {
            try
            {
                Game game = _games.GetFromGame(id);
                if (game.PlayerTurn == 1)
                {
                    _newgame.InvocationMonster(game, idMonster, game.Player1, idSpecial, targetSpecialId);
                }
                else
                {
                    _newgame.InvocationMonster(game, idMonster, game.Player2, idSpecial, targetSpecialId);
                }
                await GetBoardState(id);
            }
            catch (Exception e)
            {
                await Clients.Caller.SendAsync(Settings.Error, e.Message);
            }
        }
        public async Task PlaySpell(string id, long user_id, long idSpell, long targetSpecialId = -1)
        {
            try
            {
                Game game = _games.GetFromGame(id);
                _newgame.Spell(game, idSpell, targetSpecialId);
                await GetBoardState(id);
            }
            catch (Exception e)
            {
                await Clients.Caller.SendAsync(Settings.Error, e.Message);
            }
        }
        public async Task Attack(string id, long user_id, long specialIdAttack, long specialIdDefense = -1)
        {
            try
            {
                Game game = _games.GetFromGame(id);
                _newgame.Attack(game, specialIdAttack, specialIdDefense);
                await GetBoardState(id);

            }
            catch (Exception e)
            {
                await Clients.Caller.SendAsync(Settings.Error, e.Message);
            }
        }
        public async Task GiveUp(string id, long user_id)
        {
            try
            {
                Game game = _games.GetFromGame(id);
                _newgame.Abandon(game, user_id);
                await GetBoardState(id);
                _games.DeleteFromGame(id);
                _players.DeletePlayers(id);
            }
            catch (Exception e)
            {
                await Clients.Caller.SendAsync(Settings.Error, e.Message);
            }
        }
        public async Task GetBoardState(string id)
        {
            try
            {
                Game game = _games.GetFromGame(id);
                PlayerConnect[] players_in_game = _players.GetPlayerFromGame(id);
                BoardModel board = Mapper.Map<BoardModel>(game);
                PlayerModel player1 = Mapper.Map<PlayerModel>(game.Player1);
                EnemyModel enemy1 = Mapper.Map<EnemyModel>(game.Player1);
                foreach (var item in enemy1.FieldMonster)
                {
                    if (item != null)
                    {
                        item.CanAttack = false;
                    }
                }
                PlayerModel player2 = Mapper.Map<PlayerModel>(game.Player2);
                EnemyModel enemy2 = Mapper.Map<EnemyModel>(game.Player2);
                foreach (var item in enemy2.FieldMonster)
                {
                    if (item != null)
                    {
                        item.CanAttack = false;
                    }
                }
                foreach (var item in players_in_game)
                {
                    if (item.Id == game.Player1.PlayerId)
                    {
                        board.Player = player1.Life <= 0 ? null : player1;
                        board.Enemy = enemy2.Life <= 0 ? null : enemy2;
                        board.PlayerTurn = game.PlayerTurn == 1;
                    }
                    else
                    {
                        board.Enemy = enemy1.Life <= 0 ? null : enemy1;
                        board.Player = player2.Life <= 0 ? null : player2;
                        board.PlayerTurn = game.PlayerTurn == 2;
                    }
                    await Clients.Client(item.ConnectionId).SendAsync(Settings.Update, JsonConvert.SerializeObject(board));
                }
                if (game.Player1 == null || game.Player2 == null)
                {
                    _games.DeleteFromGame(id);
                    _players.DeletePlayers(id);
                }
            }
            catch (Exception e)
            {
                await Clients.Caller.SendAsync(Settings.Error, e.Message);
            }
        }
        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
        }
        public override async Task OnDisconnectedAsync(Exception ex)
        {
            await base.OnDisconnectedAsync(ex);
        }
    }
}
