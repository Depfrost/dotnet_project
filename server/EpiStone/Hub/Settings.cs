﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Hub
{
    public class Settings
    {
        public static readonly string Update = "Update";
        public static readonly string Error = "error";
        public static readonly string FindEnemy = "find_ennemy";
        public static readonly string WaitingList = "waitingList";
    }
}
