﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EpiStone.Hub.Models;

namespace EpiStone.Hub.Business
{
    public class WaitingListManagement : Interfaces.IWaitingListManagement
    {
        private Data.Interfaces.IWaitingList _list;

        public WaitingListManagement(Data.Interfaces.IWaitingList list)
        {
            _list = list;
        }
        public void AddToWaitingList(PlayerConnect player)
        {
            _list.AddToWaitingList(player);
        }

        public bool FindPlayerById(long user_id)
        {
            return _list.FindPlayerById(user_id);
        }

        public PlayerConnect GetPlayerFromWaitingList()
        {
            return _list.GetPlayerFromWaitingList();
        }

        public bool IsEmpty()
        {
            return _list.IsEmpty();
        }

        public void UpdateToWaitingList(PlayerConnect player)
        {
            _list.UpdateToWaitingList(player);
        }
    }
}
