﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EpiStone.Hub.Models;

namespace EpiStone.Hub.Business
{
    public class PlayersManagement : Interfaces.IPlayersManagement
    {
        private Data.Interfaces.IPlayers _players;

        public PlayersManagement(Data.Interfaces.IPlayers player)
        {
            _players = player;
        }
        public bool AddPlayers(string id, PlayerConnect[] players)
        {
            return _players.AddPlayers(id, players);
        }

        public bool DeletePlayers(string id)
        {
            return _players.DeletePlayers(id);
        }

        public PlayerConnect[] GetPlayerFromGame(string id)
        {
            return _players.GetPlayerFromGame(id);
        }
    }
}
