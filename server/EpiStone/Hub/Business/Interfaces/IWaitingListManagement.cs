﻿using EpiStone.Hub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Hub.Business.Interfaces
{
    public interface IWaitingListManagement
    {
        void AddToWaitingList(PlayerConnect player);
        void UpdateToWaitingList(PlayerConnect player);
        PlayerConnect GetPlayerFromWaitingList();
        bool FindPlayerById(long user_id);
        bool IsEmpty();
    }
}
