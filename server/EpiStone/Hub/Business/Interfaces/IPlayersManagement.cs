﻿using EpiStone.Hub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Hub.Business.Interfaces
{
    public interface IPlayersManagement
    {
        PlayerConnect[] GetPlayerFromGame(string id);
        bool DeletePlayers(string id);
        bool AddPlayers(string id, PlayerConnect[] players);
    }
}
