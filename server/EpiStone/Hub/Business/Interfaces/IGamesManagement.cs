﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Hub.Business.Interfaces
{
    public interface IGamesManagement
    {
        bool AddToGame(string id, Models.Game game);
        bool DeleteFromGame(string id);
        Models.Game GetFromGame(string id);
    }
}
