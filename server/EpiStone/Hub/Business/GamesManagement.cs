﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Hub.Business
{
    public class GamesManagement : Interfaces.IGamesManagement
    {
        private Data.Interfaces.IGames _games;

        public GamesManagement(Data.Interfaces.IGames game)
        {
            _games = game;
        }

        public bool AddToGame(string id, Models.Game game)
        {
            return _games.AddToGame(id, game);
        }

        public bool DeleteFromGame(string id)
        {
            return _games.DeleteFromGame(id);
        }

        public Models.Game GetFromGame(string id)
        {
            return _games.GetFromGame(id);
        }
    }
}
