﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Hub.Models
{
    public class Monster
    {
        public long MonsterId { get; set; }

        public long SpecialId { get; set; }
        public long EffectId { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public long Life { get; set; }
        public long Attack { get; set; }
        public long Cost { get; set; }
        public string Status { get; set; }
        public bool CanAttack { get; set; }

        public Monster(long id, long life, long attack, long cost, long specialId, long effectId, string name, string image)
        {
            MonsterId = id;
            Life = life;
            Attack = attack;
            Cost = cost;
            EffectId = effectId;
            SpecialId = specialId;
            Status = "Aucun";
            Name = name;
            Image = image;
            CanAttack = false;
        }


    }
}
