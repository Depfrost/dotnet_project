﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Hub.Models
{
    public class Game
    {
        public long Turns { get; set; }
        public long IncrementId { get; set; }
        public int PlayerTurn { get; set; }
        public Player Player1 { get; set; }
        public Player Player2 { get; set; }
    }
}
