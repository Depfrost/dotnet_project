﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Hub.Models
{
    public class Player
    {
        public string ConnectionId { get; set; }
        public string HeroImage { get; set; }

        public long PlayerId { get; set; }

        public long Cristals { get; set; }
        public long Life { get; set; }

        public long PenalityDraw { get; set; }
        public Stack<CardGame> Deck { get; set; }
        public List<CardGame> PlayerHand { get; set; }

        public Monster[] FieldMonster { get; set; }

        public Player()
        {

        }
      
    }
}
