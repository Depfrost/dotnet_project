﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Hub.Models
{
    public class PlayerConnect
    {
        public long Id { get; set; }
        public long DeckId { get; set; }
        public string ConnectionId { get; set; }
        public bool isConnected { get; set; }
    }
}
