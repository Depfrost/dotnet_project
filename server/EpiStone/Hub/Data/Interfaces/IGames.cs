﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Hub.Data.Interfaces
{
    public interface IGames
    {
        /// <summary>
        /// Add a game
        /// </summary>
        /// <param name="id">The id of the new game</param>
        /// <param name="game">The new game to add</param>
        /// <returns>The success of the operation</returns>
        bool AddToGame(string id, Models.Game game);

        /// <summary>
        /// Delete a game
        /// </summary>
        /// <param name="id">The id of the game</param>
        /// <returns>The success of the operation</returns>
        bool DeleteFromGame(string id);

        /// <summary>
        /// Get a game
        /// </summary>
        /// <param name="id">The id of the game</param>
        /// <returns>The corresponding game</returns>
        Models.Game GetFromGame(string id);
    }
}
