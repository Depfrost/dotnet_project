﻿using EpiStone.Hub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Hub.Data.Interfaces
{
    public interface IPlayers
    {
        /// <summary>
        /// Get the new players in the game
        /// </summary>
        /// <param name="id">The id of the game</param>
        /// <returns>A list of player that plays the game</returns>
        PlayerConnect[] GetPlayerFromGame(string id);

        /// <summary>
        /// Delete players from the playing players
        /// </summary>
        /// <param name="id">The id of the game</param>
        /// <returns>The result of the deletion</returns>
        bool DeletePlayers(string id);

        /// <summary>
        /// Add new players to the playing players
        /// </summary>
        /// <param name="id">The id of the game</param>
        /// <param name="players">The list of player that plays in the game</param>
        /// <returns>The result of the operation</returns>
        bool AddPlayers(string id, PlayerConnect[] players);
    }
}
