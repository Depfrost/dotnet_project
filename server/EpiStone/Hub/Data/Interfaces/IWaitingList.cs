﻿using EpiStone.Hub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Hub.Data.Interfaces
{
    public interface IWaitingList
    {
        /// <summary>
        /// Add a player in the waiting queue
        /// </summary>
        /// <param name="player">The new player</param>
        void AddToWaitingList(PlayerConnect player);

        /// <summary>
        /// Update a player in the waiting queue
        /// </summary>
        /// <param name="player">The updated player</param>
        void UpdateToWaitingList(PlayerConnect player);

        /// <summary>
        /// Get the first player connected in the waiting queue
        /// </summary>
        /// <returns>The oldest player connected in the waiting queue</returns>
        PlayerConnect GetPlayerFromWaitingList();

        /// <summary>
        /// Find a player in the waiting queue
        /// </summary>
        /// <param name="user_id">The id of the user</param>
        /// <returns>The result of operation</returns>
        bool FindPlayerById(long user_id);

        /// <summary>
        /// Check if the waiting queue is empty
        /// </summary>
        /// <returns>The result of operation</returns>
        bool IsEmpty();
    }
}
