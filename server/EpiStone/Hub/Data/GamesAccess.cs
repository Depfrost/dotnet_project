﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.Hub.Data
{
    public class GamesAccess : Interfaces.IGames
    {
        private readonly static ConcurrentDictionary<string, Models.Game> _games = new ConcurrentDictionary<string, Models.Game>();
        public bool AddToGame(string id, Models.Game game)
        {
            return _games.AddOrUpdate(id, game, (key, value) => game) == null;
        }

        public bool DeleteFromGame(string id)
        {
            Models.Game game;
            return _games.TryRemove(id, out game);
        }

        public Models.Game GetFromGame(string id)
        {
            Models.Game game;
            _games.TryGetValue(id, out game);
            return game;
        }
    }
}
