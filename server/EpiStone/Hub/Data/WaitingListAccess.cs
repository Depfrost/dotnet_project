﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EpiStone.Hub.Models;

namespace EpiStone.Hub.Data
{
    public class WaitingListAccess : Interfaces.IWaitingList
    {
        private readonly static ConcurrentQueue<PlayerConnect> _waitingQueue = new ConcurrentQueue<PlayerConnect>();
        public void AddToWaitingList(PlayerConnect player)
        {
            _waitingQueue.Enqueue(player);
        }

        public bool FindPlayerById(long user_id)
        {
            return _waitingQueue.FirstOrDefault(elem => elem.Id == user_id) != null;
        }

        public PlayerConnect GetPlayerFromWaitingList()
        {
            PlayerConnect player;
            do
            {
                _waitingQueue.TryDequeue(out player);
            } while (!_waitingQueue.IsEmpty && !player.isConnected);
            return player;
        }

        public bool IsEmpty()
        {
            return _waitingQueue.IsEmpty;
        }

        public void UpdateToWaitingList(PlayerConnect player)
        {
            _waitingQueue.First(elem => elem.Id == player.Id).ConnectionId = player.ConnectionId;
            _waitingQueue.First(elem => elem.Id == player.Id).DeckId = player.DeckId;
        }
    }
}
