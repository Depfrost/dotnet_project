﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EpiStone.Hub.Models;

namespace EpiStone.Hub.Data
{
    public class PlayersAccess : Interfaces.IPlayers
    {
        private readonly static ConcurrentDictionary<string, PlayerConnect[]> _players = new ConcurrentDictionary<string, PlayerConnect[]>();
        public bool AddPlayers(string id, PlayerConnect[] players)
        {
            return _players.AddOrUpdate(id, players, (key, value) => players) == null;
        }

        public bool DeletePlayers(string id)
        {
            PlayerConnect[] players;
            return _players.TryRemove(id, out players);
        }

        public PlayerConnect[] GetPlayerFromGame(string id)
        {
            PlayerConnect[] players;
            _players.TryGetValue(id, out players);
            return players;
        }
    }
}
