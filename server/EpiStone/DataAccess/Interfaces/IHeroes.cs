﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.DataAccess.Interfaces
{
    public interface IHeroes
    {
        IEnumerable<DBO.Heroe> GetList();

        DBO.Heroe GetById(long id);
    }
}
