﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.DataAccess.Interfaces
{
    public interface IDecks
    {
        IEnumerable<DBO.Deck> GetList();

        DBO.Deck GetDeck(long id);

        bool CreateDeck(DBO.Deck deck);

        bool DeleteDeck(long id);

        bool UpdateDeck(DBO.Deck deck, long id);

        IEnumerable<DBO.Deck> GetDeckByUser(long id);
    }
}
