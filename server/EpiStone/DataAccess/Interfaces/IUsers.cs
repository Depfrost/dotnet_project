﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.DataAccess.Interfaces
{
    public interface IUsers
    {
        IEnumerable<DBO.User> GetList();

        DBO.User GetUser(long id);

        bool CreateUser(DBO.User user);

        bool DeleteUser(long id);

        bool UpdateUser(DBO.User user, long id);

        DBO.User ConnexionUsers(string pseudo);
    }
}
