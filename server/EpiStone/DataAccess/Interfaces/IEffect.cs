﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.DataAccess.Interfaces
{
    public interface IEffects
    {
        IEnumerable<DBO.Effect> GetList();

        DBO.Effect GetById(long id);
    }
}
