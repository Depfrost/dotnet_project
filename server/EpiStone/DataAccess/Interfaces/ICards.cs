﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.DataAccess.Interfaces
{
    public interface ICards
    {
        IEnumerable<DBO.Card> GetList();

        DBO.Card GetCard(long id);


    }
}
