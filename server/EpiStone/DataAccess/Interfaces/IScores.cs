﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.DataAccess.Interfaces
{
    public interface IScores
    {
        IEnumerable<DBO.User> GetList();
        DBO.User GetScores(long id);

        bool UpdateScore(DBO.User user, long id);
    }
}
