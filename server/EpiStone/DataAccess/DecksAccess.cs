﻿using EpiStone.DataAccess.Interfaces;
using EpiStone.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.DataAccess
{
    public class DecksAccess: Interfaces.IDecks
    {
        private IRepository<DBO.Deck> _repo;
        public DecksAccess(IRepository<DBO.Deck> repo)
        {
            _repo = repo;
        }
        public IEnumerable<DBO.Deck> GetList()
        {
            var list = _repo.GetAll();
            return list;
        }

        public DBO.Deck GetDeck(long id)
        {
            return _repo.GetById(id);
        }

        public bool CreateDeck(DBO.Deck deck)
        {
            return _repo.Create(deck);
        }

        public bool DeleteDeck(long id)
        {
            return _repo.Delete(id);
        }

        public bool UpdateDeck(DBO.Deck deck, long id)
        {
          
            return _repo.Update(deck, id);
        }

        public IEnumerable<DBO.Deck> GetDeckByUser(long id)
        {
           return _repo.GetAll().ToList<DBO.Deck>().FindAll(x => x.UserId == id);
          
        }
    }
}
