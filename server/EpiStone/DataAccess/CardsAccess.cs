﻿using EpiStone.DataAccess.Interfaces;
using EpiStone.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.DataAccess
{
    public class CardsAccess: ICards
    {
        private IRepository<DBO.Card> _repo;
        public CardsAccess(IRepository<DBO.Card> repo)
        {
            _repo = repo;
        }
        public IEnumerable<DBO.Card> GetList()
        {
            var list = _repo.GetAll();
            return list;
        }

        public DBO.Card GetCard(long id)
        {
            return _repo.GetById(id);
        }
    }
}
