﻿using EpiStone.DataAccess.Interfaces;
using EpiStone.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.DataAccess
{
    public class HeroesAccess: IHeroes
    {
        private IRepository<DBO.Heroe> _repo;
        public HeroesAccess(IRepository<DBO.Heroe> repo)
        {
            _repo = repo;
        }

        public IEnumerable<DBO.Heroe> GetList()
        {
            return _repo.GetAll();
        }

        public DBO.Heroe GetById(long id)
        {
            return _repo.GetById(id);
        }
    }
}
