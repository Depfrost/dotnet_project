﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.DataAccess.Repository
{
    /// <summary>
    /// Repository
    /// </summary>
    /// <typeparam name="TEntity">DBO objects</typeparam>
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();

        TEntity GetById(long id);

        bool Create(TEntity entity);
        bool Delete(long id);

        bool Update(TEntity entity, long id);
    }
}
