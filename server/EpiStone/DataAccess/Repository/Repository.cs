﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EpiStone.DataAccess.Models;
using EpiStone.DBO;
using EpiStone.Models;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace EpiStone.DataAccess.Repository
{
    /// <summary>
    /// Repository to access database
    /// </summary>
    /// <typeparam name="S">Table in database</typeparam>
    /// <typeparam name="T">Corresponding DBO</typeparam>
    public class Repository<S, T> : IRepository<T> where T : EntityBase where S : class
    {
        private EpiStoneContext _context;
        private DbSet<S> _entitySet;
        public Repository(EpiStoneContext context)
        {
            _context = context;
            _entitySet = context.Set<S>();
        }

        public bool Create(T entity)
        {
            try
            {
                 _entitySet.Add(AutoMapper.Mapper.Map<S>(entity));
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IEnumerable<T> GetAll()
        {
            try
            {
                List<T> allData = new List<T>();
                foreach (var item in _entitySet)
                {
                    allData.Add(Mapper.Map<T>(item));
                }
                return allData;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public T GetById(long id)
        {
            try
            {
                var q = _entitySet.Find(id);
                return Mapper.Map<T>(q);
            }
            catch (Exception e)
            {
                var q = e;
                return null;
            }
        }


        public bool Delete(long id)
        {

            try
            {
              S elt = _entitySet.Find(id);
             _entitySet.Remove(elt);
              _context.SaveChanges();
                
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(T entity, long id)
        {
            try
            {
                S elt = _entitySet.Find(id);
                elt = AutoMapper.Mapper.Map<T, S>(entity, elt);
                _context.SaveChanges();
                
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
