﻿using EpiStone.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EpiStone.DataAccess.Repository;
using EpiStone.DataAccess.Interfaces;

namespace EpiStone.DataAccess
{
    public class UsersAccess: IUsers
    {
        private IRepository<DBO.User> _repo;

        public UsersAccess(IRepository<DBO.User> repo)
        {
            _repo = repo;
        }

        public IEnumerable<DBO.User> GetList()
        {
            return _repo.GetAll();
        }

        public DBO.User GetUser(long id)
        {
            return _repo.GetById(id);
        }

        public bool CreateUser(DBO.User user)
        {
            var res = _repo.GetAll().ToList<DBO.User>().Exists(x => x.Pseudo == user.Pseudo);
            if (res == true)
                return false;

            return _repo.Create(user);
        }

        public bool DeleteUser(long id)
        {
            return _repo.Delete(id);
        }
            
        public bool UpdateUser(DBO.User user, long id)
        {
            var res = _repo.GetAll().ToList<DBO.User>().Exists(x => x.Pseudo == user.Pseudo && x.Id != user.Id);
            if (res)
                return false;

            return _repo.Update(user, id);
        }

        public DBO.User ConnexionUsers(string pseudo)
        {
            return _repo.GetAll().ToList<DBO.User>().Find(x => x.Pseudo == pseudo);
        }

        
    }
}
