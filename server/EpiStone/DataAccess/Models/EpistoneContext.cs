﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace EpiStone.DataAccess.Models
{
    public partial class EpiStoneContext : DbContext
    {
        public virtual DbSet<Cards> Cards { get; set; }
        public virtual DbSet<Decks> Decks { get; set; }
        public virtual DbSet<Effects> Effects { get; set; }
        public virtual DbSet<Heroes> Heroes { get; set; }
        public virtual DbSet<Users> Users { get; set; }

       
        public EpiStoneContext(DbContextOptions options): base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cards>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Attack).HasColumnName("attack");

                entity.Property(e => e.Cost).HasColumnName("cost");

                entity.Property(e => e.EffectId).HasColumnName("effect_id");

                entity.Property(e => e.Image)
                    .IsRequired()
                    .HasColumnName("image")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Life).HasColumnName("life");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Type).HasColumnName("type");

                entity.HasOne(d => d.Effect)
                    .WithMany(p => p.Cards)
                    .HasForeignKey(d => d.EffectId)
                    .HasConstraintName("FK_Cards_Effects");
            });

            modelBuilder.Entity<Decks>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Card1).HasColumnName("card_1");

                entity.Property(e => e.Card10).HasColumnName("card_10");

                entity.Property(e => e.Card11).HasColumnName("card_11");

                entity.Property(e => e.Card12).HasColumnName("card_12");

                entity.Property(e => e.Card13).HasColumnName("card_13");

                entity.Property(e => e.Card14).HasColumnName("card_14");

                entity.Property(e => e.Card15).HasColumnName("card_15");

                entity.Property(e => e.Card16).HasColumnName("card_16");

                entity.Property(e => e.Card17).HasColumnName("card_17");

                entity.Property(e => e.Card18).HasColumnName("card_18");

                entity.Property(e => e.Card19).HasColumnName("card_19");

                entity.Property(e => e.Card2).HasColumnName("card_2");

                entity.Property(e => e.Card20).HasColumnName("card_20");

                entity.Property(e => e.Card21).HasColumnName("card_21");

                entity.Property(e => e.Card22).HasColumnName("card_22");

                entity.Property(e => e.Card23).HasColumnName("card_23");

                entity.Property(e => e.Card24).HasColumnName("card_24");

                entity.Property(e => e.Card25).HasColumnName("card_25");

                entity.Property(e => e.Card3).HasColumnName("card_3");

                entity.Property(e => e.Card4).HasColumnName("card_4");

                entity.Property(e => e.Card5).HasColumnName("card_5");

                entity.Property(e => e.Card6).HasColumnName("card_6");

                entity.Property(e => e.Card7).HasColumnName("card_7");

                entity.Property(e => e.Card8).HasColumnName("card_8");

                entity.Property(e => e.Card9).HasColumnName("card_9");

                entity.Property(e => e.HeroId).HasColumnName("hero_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Card1Navigation)
                    .WithMany(p => p.DecksCard1Navigation)
                    .HasForeignKey(d => d.Card1)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards1");

                entity.HasOne(d => d.Card10Navigation)
                    .WithMany(p => p.DecksCard10Navigation)
                    .HasForeignKey(d => d.Card10)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards10");

                entity.HasOne(d => d.Card11Navigation)
                    .WithMany(p => p.DecksCard11Navigation)
                    .HasForeignKey(d => d.Card11)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards11");

                entity.HasOne(d => d.Card12Navigation)
                    .WithMany(p => p.DecksCard12Navigation)
                    .HasForeignKey(d => d.Card12)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards12");

                entity.HasOne(d => d.Card13Navigation)
                    .WithMany(p => p.DecksCard13Navigation)
                    .HasForeignKey(d => d.Card13)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards13");

                entity.HasOne(d => d.Card14Navigation)
                    .WithMany(p => p.DecksCard14Navigation)
                    .HasForeignKey(d => d.Card14)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards14");

                entity.HasOne(d => d.Card15Navigation)
                    .WithMany(p => p.DecksCard15Navigation)
                    .HasForeignKey(d => d.Card15)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards15");

                entity.HasOne(d => d.Card16Navigation)
                    .WithMany(p => p.DecksCard16Navigation)
                    .HasForeignKey(d => d.Card16)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards16");

                entity.HasOne(d => d.Card17Navigation)
                    .WithMany(p => p.DecksCard17Navigation)
                    .HasForeignKey(d => d.Card17)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards17");

                entity.HasOne(d => d.Card18Navigation)
                    .WithMany(p => p.DecksCard18Navigation)
                    .HasForeignKey(d => d.Card18)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards18");

                entity.HasOne(d => d.Card19Navigation)
                    .WithMany(p => p.DecksCard19Navigation)
                    .HasForeignKey(d => d.Card19)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards19");

                entity.HasOne(d => d.Card2Navigation)
                    .WithMany(p => p.DecksCard2Navigation)
                    .HasForeignKey(d => d.Card2)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards2");

                entity.HasOne(d => d.Card20Navigation)
                    .WithMany(p => p.DecksCard20Navigation)
                    .HasForeignKey(d => d.Card20)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards20");

                entity.HasOne(d => d.Card21Navigation)
                    .WithMany(p => p.DecksCard21Navigation)
                    .HasForeignKey(d => d.Card21)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards21");

                entity.HasOne(d => d.Card22Navigation)
                    .WithMany(p => p.DecksCard22Navigation)
                    .HasForeignKey(d => d.Card22)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards22");

                entity.HasOne(d => d.Card23Navigation)
                    .WithMany(p => p.DecksCard23Navigation)
                    .HasForeignKey(d => d.Card23)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards23");

                entity.HasOne(d => d.Card24Navigation)
                    .WithMany(p => p.DecksCard24Navigation)
                    .HasForeignKey(d => d.Card24)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards24");

                entity.HasOne(d => d.Card25Navigation)
                    .WithMany(p => p.DecksCard25Navigation)
                    .HasForeignKey(d => d.Card25)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards25");

                entity.HasOne(d => d.Card3Navigation)
                    .WithMany(p => p.DecksCard3Navigation)
                    .HasForeignKey(d => d.Card3)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards3");

                entity.HasOne(d => d.Card4Navigation)
                    .WithMany(p => p.DecksCard4Navigation)
                    .HasForeignKey(d => d.Card4)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards4");

                entity.HasOne(d => d.Card5Navigation)
                    .WithMany(p => p.DecksCard5Navigation)
                    .HasForeignKey(d => d.Card5)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards5");

                entity.HasOne(d => d.Card6Navigation)
                    .WithMany(p => p.DecksCard6Navigation)
                    .HasForeignKey(d => d.Card6)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards6");

                entity.HasOne(d => d.Card7Navigation)
                    .WithMany(p => p.DecksCard7Navigation)
                    .HasForeignKey(d => d.Card7)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards7");

                entity.HasOne(d => d.Card8Navigation)
                    .WithMany(p => p.DecksCard8Navigation)
                    .HasForeignKey(d => d.Card8)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards8");

                entity.HasOne(d => d.Card9Navigation)
                    .WithMany(p => p.DecksCard9Navigation)
                    .HasForeignKey(d => d.Card9)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Cards9");

                entity.HasOne(d => d.Hero)
                    .WithMany(p => p.Decks)
                    .HasForeignKey(d => d.HeroId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Heroes");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Decks)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Decks_Users");
            });

            modelBuilder.Entity<Effects>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TargetAllMonster).HasColumnName("target_all_monster");

                entity.Property(e => e.TargetHero).HasColumnName("target_hero");

                entity.Property(e => e.TargetSingleMonster).HasColumnName("target_single_monster");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Heroes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Card1).HasColumnName("card_1");

                entity.Property(e => e.Card2).HasColumnName("card_2");

                entity.Property(e => e.Card3).HasColumnName("card_3");

                entity.Property(e => e.Card4).HasColumnName("card_4");

                entity.Property(e => e.Card5).HasColumnName("card_5");

                entity.Property(e => e.Image)
                    .IsRequired()
                    .HasColumnName("image")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Card1Navigation)
                    .WithMany(p => p.HeroesCard1Navigation)
                    .HasForeignKey(d => d.Card1)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Heroes_Cards1");

                entity.HasOne(d => d.Card2Navigation)
                    .WithMany(p => p.HeroesCard2Navigation)
                    .HasForeignKey(d => d.Card2)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Heroes_Cards2");

                entity.HasOne(d => d.Card3Navigation)
                    .WithMany(p => p.HeroesCard3Navigation)
                    .HasForeignKey(d => d.Card3)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Heroes_Heroes3");

                entity.HasOne(d => d.Card4Navigation)
                    .WithMany(p => p.HeroesCard4Navigation)
                    .HasForeignKey(d => d.Card4)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Heroes_Cards4");

                entity.HasOne(d => d.Card5Navigation)
                    .WithMany(p => p.HeroesCard5Navigation)
                    .HasForeignKey(d => d.Card5)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Heroes_Heroes5");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Defeats).HasColumnName("defeats");

                entity.Property(e => e.Draws).HasColumnName("draws");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pseudo)
                    .IsRequired()
                    .HasColumnName("pseudo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Score).HasColumnName("score");

                entity.Property(e => e.Victories).HasColumnName("victories");
            });
        }
    }
}
