﻿using System;
using System.Collections.Generic;

namespace EpiStone.DataAccess.Models
{
    public partial class Cards
    {
        public Cards()
        {
            DecksCard10Navigation = new HashSet<Decks>();
            DecksCard11Navigation = new HashSet<Decks>();
            DecksCard12Navigation = new HashSet<Decks>();
            DecksCard13Navigation = new HashSet<Decks>();
            DecksCard14Navigation = new HashSet<Decks>();
            DecksCard15Navigation = new HashSet<Decks>();
            DecksCard16Navigation = new HashSet<Decks>();
            DecksCard17Navigation = new HashSet<Decks>();
            DecksCard18Navigation = new HashSet<Decks>();
            DecksCard19Navigation = new HashSet<Decks>();
            DecksCard1Navigation = new HashSet<Decks>();
            DecksCard20Navigation = new HashSet<Decks>();
            DecksCard21Navigation = new HashSet<Decks>();
            DecksCard22Navigation = new HashSet<Decks>();
            DecksCard23Navigation = new HashSet<Decks>();
            DecksCard24Navigation = new HashSet<Decks>();
            DecksCard25Navigation = new HashSet<Decks>();
            DecksCard2Navigation = new HashSet<Decks>();
            DecksCard3Navigation = new HashSet<Decks>();
            DecksCard4Navigation = new HashSet<Decks>();
            DecksCard5Navigation = new HashSet<Decks>();
            DecksCard6Navigation = new HashSet<Decks>();
            DecksCard7Navigation = new HashSet<Decks>();
            DecksCard8Navigation = new HashSet<Decks>();
            DecksCard9Navigation = new HashSet<Decks>();
            HeroesCard1Navigation = new HashSet<Heroes>();
            HeroesCard2Navigation = new HashSet<Heroes>();
            HeroesCard3Navigation = new HashSet<Heroes>();
            HeroesCard4Navigation = new HashSet<Heroes>();
            HeroesCard5Navigation = new HashSet<Heroes>();
        }

        public long Id { get; set; }
        public long? EffectId { get; set; }
        public long Type { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public long? Life { get; set; }
        public long? Attack { get; set; }
        public long Cost { get; set; }

        public Effects Effect { get; set; }
        public ICollection<Decks> DecksCard10Navigation { get; set; }
        public ICollection<Decks> DecksCard11Navigation { get; set; }
        public ICollection<Decks> DecksCard12Navigation { get; set; }
        public ICollection<Decks> DecksCard13Navigation { get; set; }
        public ICollection<Decks> DecksCard14Navigation { get; set; }
        public ICollection<Decks> DecksCard15Navigation { get; set; }
        public ICollection<Decks> DecksCard16Navigation { get; set; }
        public ICollection<Decks> DecksCard17Navigation { get; set; }
        public ICollection<Decks> DecksCard18Navigation { get; set; }
        public ICollection<Decks> DecksCard19Navigation { get; set; }
        public ICollection<Decks> DecksCard1Navigation { get; set; }
        public ICollection<Decks> DecksCard20Navigation { get; set; }
        public ICollection<Decks> DecksCard21Navigation { get; set; }
        public ICollection<Decks> DecksCard22Navigation { get; set; }
        public ICollection<Decks> DecksCard23Navigation { get; set; }
        public ICollection<Decks> DecksCard24Navigation { get; set; }
        public ICollection<Decks> DecksCard25Navigation { get; set; }
        public ICollection<Decks> DecksCard2Navigation { get; set; }
        public ICollection<Decks> DecksCard3Navigation { get; set; }
        public ICollection<Decks> DecksCard4Navigation { get; set; }
        public ICollection<Decks> DecksCard5Navigation { get; set; }
        public ICollection<Decks> DecksCard6Navigation { get; set; }
        public ICollection<Decks> DecksCard7Navigation { get; set; }
        public ICollection<Decks> DecksCard8Navigation { get; set; }
        public ICollection<Decks> DecksCard9Navigation { get; set; }
        public ICollection<Heroes> HeroesCard1Navigation { get; set; }
        public ICollection<Heroes> HeroesCard2Navigation { get; set; }
        public ICollection<Heroes> HeroesCard3Navigation { get; set; }
        public ICollection<Heroes> HeroesCard4Navigation { get; set; }
        public ICollection<Heroes> HeroesCard5Navigation { get; set; }
    }
}
