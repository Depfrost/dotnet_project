﻿using System;
using System.Collections.Generic;

namespace EpiStone.DataAccess.Models

{
    public partial class Decks
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public long HeroId { get; set; }
        public string Name { get; set; }
        public long Card1 { get; set; }
        public long Card2 { get; set; }
        public long Card3 { get; set; }
        public long Card4 { get; set; }
        public long Card5 { get; set; }
        public long Card6 { get; set; }
        public long Card7 { get; set; }
        public long Card8 { get; set; }
        public long Card9 { get; set; }
        public long Card10 { get; set; }
        public long Card11 { get; set; }
        public long Card12 { get; set; }
        public long Card13 { get; set; }
        public long Card14 { get; set; }
        public long Card15 { get; set; }
        public long Card16 { get; set; }
        public long Card17 { get; set; }
        public long Card18 { get; set; }
        public long Card19 { get; set; }
        public long Card20 { get; set; }
        public long Card21 { get; set; }
        public long Card22 { get; set; }
        public long Card23 { get; set; }
        public long Card24 { get; set; }
        public long Card25 { get; set; }

        public Cards Card10Navigation { get; set; }
        public Cards Card11Navigation { get; set; }
        public Cards Card12Navigation { get; set; }
        public Cards Card13Navigation { get; set; }
        public Cards Card14Navigation { get; set; }
        public Cards Card15Navigation { get; set; }
        public Cards Card16Navigation { get; set; }
        public Cards Card17Navigation { get; set; }
        public Cards Card18Navigation { get; set; }
        public Cards Card19Navigation { get; set; }
        public Cards Card1Navigation { get; set; }
        public Cards Card20Navigation { get; set; }
        public Cards Card21Navigation { get; set; }
        public Cards Card22Navigation { get; set; }
        public Cards Card23Navigation { get; set; }
        public Cards Card24Navigation { get; set; }
        public Cards Card25Navigation { get; set; }
        public Cards Card2Navigation { get; set; }
        public Cards Card3Navigation { get; set; }
        public Cards Card4Navigation { get; set; }
        public Cards Card5Navigation { get; set; }
        public Cards Card6Navigation { get; set; }
        public Cards Card7Navigation { get; set; }
        public Cards Card8Navigation { get; set; }
        public Cards Card9Navigation { get; set; }
        public Heroes Hero { get; set; }
        public Users User { get; set; }
    }
}
