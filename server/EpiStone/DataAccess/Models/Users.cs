﻿using System;
using System.Collections.Generic;

namespace EpiStone.DataAccess.Models
{
    public partial class Users
    {
        public Users()
        {
            Decks = new HashSet<Decks>();
        }

        public long Id { get; set; }
        public string Pseudo { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public long Victories { get; set; }
        public long Defeats { get; set; }
        public long Draws { get; set; }
        public long Score { get; set; }

        public ICollection<Decks> Decks { get; set; }
    }
}
