﻿using System;
using System.Collections.Generic;

namespace EpiStone.DataAccess.Models
{
    public partial class Heroes
    {
        public Heroes()
        {
            Decks = new HashSet<Decks>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public long Card1 { get; set; }
        public long Card2 { get; set; }
        public long Card3 { get; set; }
        public long Card4 { get; set; }
        public long Card5 { get; set; }

        public Cards Card1Navigation { get; set; }
        public Cards Card2Navigation { get; set; }
        public Cards Card3Navigation { get; set; }
        public Cards Card4Navigation { get; set; }
        public Cards Card5Navigation { get; set; }
        public ICollection<Decks> Decks { get; set; }
    }
}
