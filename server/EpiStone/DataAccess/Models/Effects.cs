﻿using System;
using System.Collections.Generic;

namespace EpiStone.DataAccess.Models
{
    public partial class Effects
    {
        public Effects()
        {
            Cards = new HashSet<Cards>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public int? TargetHero { get; set; }
        public int? TargetSingleMonster { get; set; }
        public int? TargetAllMonster { get; set; }

        public ICollection<Cards> Cards { get; set; }
    }
}
