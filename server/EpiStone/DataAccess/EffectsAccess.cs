﻿using EpiStone.DataAccess.Interfaces;
using EpiStone.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.DataAccess
{
    public class EffectsAccess: IEffects
    {
        private IRepository<DBO.Effect> _repo;
        public EffectsAccess(IRepository<DBO.Effect> repo)
        {
            _repo = repo;
        }

        public IEnumerable<DBO.Effect> GetList()
        {
            return _repo.GetAll();
        }

        public DBO.Effect GetById(long id)
        {
            return _repo.GetById(id);
        }
    }
}
