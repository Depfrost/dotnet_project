﻿using EpiStone.DataAccess.Interfaces;
using EpiStone.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.DataAccess
{
    public class ScoresAccess: IScores
    {
        private IRepository<DBO.User> _repo;

        public ScoresAccess(IRepository<DBO.User> repo)
        {
            _repo = repo;
        }

        public IEnumerable<DBO.User> GetList()
        {
            return _repo.GetAll();
        }

        public DBO.User GetScores(long id)
        {
            return _repo.GetById(id);
        }

        public bool UpdateScore(DBO.User user, long id)
        {
            var res = _repo.GetAll().ToList<DBO.User>().Exists(x => x.Pseudo == user.Pseudo && x.Id != user.Id);
            if (res)
                return false;

            return _repo.Update(user, id);
        }
    }
}
