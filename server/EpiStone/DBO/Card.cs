﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.DBO
{
    public class Card : EntityBase
    {
        public long? EffectId { get; set; }
        public long Type { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public long? Life { get; set; }
        public long? Attack { get; set; }
        public long Cost { get; set; }
    }
}
