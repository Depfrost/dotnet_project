﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace EpiStone.DBO
{
    public class User : EntityBase
    {
        public string Pseudo { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public long Victories { get; set; }
        public long Defeats { get; set; }
        public long Draws { get; set; }
        public long Score { get; set; }
    }
}
