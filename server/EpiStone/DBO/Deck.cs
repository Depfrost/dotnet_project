﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.DBO
{
    public class Deck: EntityBase
    {
        public long UserId { get; set; }
        public long HeroId { get; set; }
        public string Name { get; set; }
        public long Card1 { get; set; }
        public long Card2 { get; set; }
        public long Card3 { get; set; }
        public long Card4 { get; set; }
        public long Card5 { get; set; }
        public long Card6 { get; set; }
        public long Card7 { get; set; }
        public long Card8 { get; set; }
        public long Card9 { get; set; }
        public long Card10 { get; set; }
        public long Card11 { get; set; }
        public long Card12 { get; set; }
        public long Card13 { get; set; }
        public long Card14 { get; set; }
        public long Card15 { get; set; }
        public long Card16 { get; set; }
        public long Card17 { get; set; }
        public long Card18 { get; set; }
        public long Card19 { get; set; }
        public long Card20 { get; set; }
        public long Card21 { get; set; }
        public long Card22 { get; set; }
        public long Card23 { get; set; }
        public long Card24 { get; set; }
        public long Card25 { get; set; }

    }
}
