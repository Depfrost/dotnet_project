﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.DBO
{
    public class Heroe : EntityBase
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public long Card1 { get; set; }
        public long Card2 { get; set; }
        public long Card3 { get; set; }
        public long Card4 { get; set; }
        public long Card5 { get; set; }

    }
}
