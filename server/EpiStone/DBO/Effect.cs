﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpiStone.DBO
{
    public class Effect : EntityBase
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public int? TargetHero { get; set; }
        public int? TargetSingleMonster { get; set; }
        public int? TargetAllMonster { get; set; }
    }
}
