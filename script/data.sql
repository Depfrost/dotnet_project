USE [master]
GO
/****** Object:  Database [EpiStone]    Script Date: 15/05/2018 23:38:11 ******/
CREATE DATABASE [EpiStone]
GO
ALTER DATABASE [EpiStone] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EpiStone].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EpiStone] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EpiStone] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EpiStone] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EpiStone] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EpiStone] SET ARITHABORT OFF 
GO
ALTER DATABASE [EpiStone] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [EpiStone] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EpiStone] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EpiStone] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EpiStone] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EpiStone] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EpiStone] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EpiStone] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EpiStone] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EpiStone] SET  DISABLE_BROKER 
GO
ALTER DATABASE [EpiStone] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EpiStone] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EpiStone] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EpiStone] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EpiStone] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EpiStone] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [EpiStone] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EpiStone] SET RECOVERY FULL 
GO
ALTER DATABASE [EpiStone] SET  MULTI_USER 
GO
ALTER DATABASE [EpiStone] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EpiStone] SET DB_CHAINING OFF 
GO
ALTER DATABASE [EpiStone] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [EpiStone] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [EpiStone] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [EpiStone] SET QUERY_STORE = OFF
GO
USE [EpiStone]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [EpiStone]
GO
/****** Object:  Table [dbo].[Cards]    Script Date: 15/05/2018 23:38:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cards](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[effect_id] [bigint] NULL,
	[type] [bigint] NOT NULL,
	[image] [varchar](100) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[life] [bigint] NULL,
	[attack] [bigint] NULL,
	[cost] [bigint] NOT NULL,
 CONSTRAINT [PK_Cards] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Decks]    Script Date: 15/05/2018 23:38:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Decks](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[user_id] [bigint] NOT NULL,
	[hero_id] [bigint] NOT NULL,
	[name] [varchar](50) NOT NULL,
	[card_1] [bigint] NOT NULL,
	[card_2] [bigint] NOT NULL,
	[card_3] [bigint] NOT NULL,
	[card_4] [bigint] NOT NULL,
	[card_5] [bigint] NOT NULL,
	[card_6] [bigint] NOT NULL,
	[card_7] [bigint] NOT NULL,
	[card_8] [bigint] NOT NULL,
	[card_9] [bigint] NOT NULL,
	[card_10] [bigint] NOT NULL,
	[card_11] [bigint] NOT NULL,
	[card_12] [bigint] NOT NULL,
	[card_13] [bigint] NOT NULL,
	[card_14] [bigint] NOT NULL,
	[card_15] [bigint] NOT NULL,
	[card_16] [bigint] NOT NULL,
	[card_17] [bigint] NOT NULL,
	[card_18] [bigint] NOT NULL,
	[card_19] [bigint] NOT NULL,
	[card_20] [bigint] NOT NULL,
	[card_21] [bigint] NOT NULL,
	[card_22] [bigint] NOT NULL,
	[card_23] [bigint] NOT NULL,
	[card_24] [bigint] NOT NULL,
	[card_25] [bigint] NOT NULL,
 CONSTRAINT [PK_Decks] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Effects]    Script Date: 15/05/2018 23:38:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Effects](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[value] [varchar](50) NOT NULL,
	[type] [varchar](50) NOT NULL,
	[description] [varchar](200) NOT NULL,
	[target_hero] [int] NULL,
	[target_single_monster] [int] NULL,
	[target_all_monster] [int] NULL,
 CONSTRAINT [PK_Effects] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Heroes]    Script Date: 15/05/2018 23:38:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Heroes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[image] [varchar](100) NOT NULL,
	[card_1] [bigint] NOT NULL,
	[card_2] [bigint] NOT NULL,
	[card_3] [bigint] NOT NULL,
	[card_4] [bigint] NOT NULL,
	[card_5] [bigint] NOT NULL,
 CONSTRAINT [PK_Heroes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 15/05/2018 23:38:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[pseudo] [varchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[victories] [bigint] NOT NULL,
	[defeats] [bigint] NOT NULL,
	[draws] [bigint] NOT NULL,
	[score] [bigint] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Cards] ON 

INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (1, NULL, 0, N'1P6TeUP.jpg', N'Nain ivre', 2, 1, 1)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (2, 16, 0, N'whGDFrO.jpg', N'Diablotin infusé', 2, 3, 1)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (3, NULL, 0, N'YF7aXd0.jpg', N'Vautour vorace', 1, 2, 1)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (4, NULL, 0, N'BVLbZH5.jpg', N'Guerrière orc', 2, 2, 2)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (5, NULL, 0, N'cltflea.jpg', N'Elfe corrompue', 3, 1, 2)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (6, NULL, 0, N'uGmMM7A.jpg', N'Sanglier enragé', 1, 3, 2)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (7, 27, 0, N'HbfLiln.jpg', N'Manipulatrice de cartes', 1, 2, 2)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (8, 30, 0, N'ZnsEhtM.jpg', N'Protecteur des arcanes', 2, 1, 2)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (10, NULL, 0, N'uqm1ay6.jpg', N'Paladin de lumière', 3, 3, 3)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (11, NULL, 0, N's9muTvz.jpg', N'Assassin orc', 2, 4, 3)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (12, NULL, 0, N'7v6YgeZ.jpg', N'Ténèbres envoutantes', 4, 2, 3)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (13, 3, 0, N'5SnhnXE.jpg', N'Chef de guerre Tauren', 2, 2, 3)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (14, 17, 0, N'r2NW3eM.jpg', N'Démoniste corompu', 3, 2, 3)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (15, NULL, 0, N'fXQ4kgp.jpg', N'Yeti possédé', 4, 4, 4)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (16, 30, 0, N'a7nKgBo.jpg', N'Défenseur troll', 4, 3, 4)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (17, 18, 0, N'2Mz0zKd.jpg', N'Ninja furtif', 2, 5, 4)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (18, 4, 0, N'CuepVJJ.jpg', N'Chaman protecteur', 3, 3, 4)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (19, NULL, 0, N'XKJ70r2.jpg', N'Fille du feu', 5, 5, 5)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (20, 5, 0, N'A3VPB2z.jpg', N'Totem arcanique', 4, 4, 5)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (21, NULL, 0, N'hUlst7b.jpg', N'Ombre meutrière', 2, 8, 5)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (22, 30, 0, N'U2gyVra.jpg', N'Nordique courageux', 7, 3, 5)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (23, NULL, 0, N'jBSFY6G.jpg', N'Sorcier diabolique', 6, 6, 6)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (24, 40, 0, N'4vzyhnk.jpg', N'Gardienne des esprits', 4, 4, 6)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (25, 19, 0, N'ipiUr2F.jpg', N'Guerrier de la devastation', 6, 4, 6)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (26, NULL, 0, N'9O2Ibvz.jpg', N'Drake métalique', 7, 7, 7)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (28, 41, 0, N'SLzRd0P.jpg', N'Héros légendaire', 6, 6, 7)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (29, 16, 0, N'W8voQqj.jpg', N'Golem ancestral', 8, 8, 7)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (30, NULL, 0, N'I2ZaKS4.jpg', N'Archère de l''ombre', 8, 8, 8)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (31, 26, 0, N'uv3MQnb.jpg', N'Gardien de l''enfer', 8, 4, 8)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (32, NULL, 0, N'XqgSpQO.jpg', N'Seigneur de guerre orc', 9, 9, 9)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (33, NULL, 0, N'68MoK1z.jpg', N'Thanos Elfe', 10, 10, 10)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (34, 17, 1, N'cSiMeQw.jpg', N'Tir Arcane', NULL, NULL, 1)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (35, 8, 1, N'80b9d20.jpg', N'Armure Renforcée', NULL, NULL, 1)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (36, 23, 1, N'QKDD7GD.jpg', N'Frappe gélée', NULL, NULL, 1)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (37, 14, 1, N'16sLzoa.jpg', N'Attaque sinistre', NULL, NULL, 1)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (38, 1, 1, N'RF9qztg.jpg', N'Potion de soin', NULL, NULL, 1)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (39, 35, 1, N'4c23083.jpg', N'Marque de la nature', NULL, NULL, 2)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (40, 18, 1, N'5dLj73U.jpg', N'Explosion arcane', NULL, NULL, 2)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (41, 9, 1, N'JaaMVCv.jpg', N'Lame empoisonée', NULL, NULL, 2)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (42, 27, 1, N'kidjfY8.jpg', N'Avidité', NULL, NULL, 2)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (43, 7, 1, N'MzNhgqC.jpg', N'Berserker', NULL, NULL, 3)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (44, 31, 1, N'dEg584H.jpg', N'Barrière', NULL, NULL, 3)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (45, 45, 1, N'RGIfU8q.jpg', N'Chargez', NULL, NULL, 3)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (46, 33, 1, N'EeoFc40.jpg', N'Soin angélique', NULL, NULL, 3)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (47, 34, 1, N'dc855e6.jpg', N'Boule de feu', NULL, NULL, 4)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (48, 41, 1, N'xSuduAV.jpg', N'Fanfare', NULL, NULL, 4)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (49, 24, 1, N'8ahE3CK.jpg', N'Annihilation', NULL, NULL, 4)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (50, 4, 1, N'zuz1Wqq.jpg', N'Vague de soin', NULL, NULL, 5)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (51, 19, 1, N'jDBG8e6.jpg', N'Choc divin', NULL, NULL, 5)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (54, 10, 1, N'pmsRH5D.jpg', N'Bouclier divin', NULL, NULL, 5)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (55, 35, 1, N'Ce8f5uH.jpg', N'Miracle', NULL, NULL, 6)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (56, 36, 1, N'a478c18.png', N'Remède divin', NULL, NULL, 6)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (57, 21, 1, N'AOUOXYd.jpg', N'Feu étoile', NULL, NULL, 6)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (58, 22, 1, N'Ef6g8Th.jpg', N'Incendie', NULL, NULL, 7)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (59, 29, 1, N'ajduGF7.png', N'Sprint', NULL, NULL, 7)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (60, 25, 1, N'aWxTMxp.jpg', N'Coup vicieux', NULL, NULL, 8)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (61, 23, 1, N'nD543Fv.jpg', N'Blizzard', NULL, NULL, 8)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (62, 11, 1, N'Tn3i20o.jpg', N'Evolution', NULL, NULL, 9)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (63, 37, 1, N'raCZBOc.jpg', N'Bénédiction ténèbreuse', NULL, NULL, 10)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (64, 26, 1, N'eNddgLH.jpg', N'Appel du vide', NULL, NULL, 10)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (65, 30, 0, N'IwlTsRy.jpg', N'Protecteur', 2, 2, 2)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (66, 13, 0, N'r7F5K7R.jpg', N'Cavalier', 3, 5, 4)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (67, 35, 0, N'EN6KARV.jpg', N'Chef de guerre', 6, 6, 8)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (68, 39, 1, N'Gl7CpnT.jpg', N'Ceinture de vie', NULL, NULL, 3)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (69, 35, 1, N'FnrWyjx.jpg', N'Encouragement', NULL, NULL, 6)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (70, 40, 1, N'2MUcBPu.jpg', N'Arcane d''intelligence', NULL, NULL, 2)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (71, 32, 1, N'ijDDv7D.jpg', N'Iceberg', NULL, NULL, 4)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (72, 21, 1, N'YUJquNI.jpg', N'Piège miracle', NULL, NULL, 4)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (73, 38, 1, N'CfX728w.jpg', N'Barrière magique', NULL, NULL, 6)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (74, 18, 0, N'BNWhoZC.jpg', N'Sorcière élémentaire', 2, 5, 5)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (75, 20, 1, N'211wbGZ.jpg', N'Taillade', NULL, NULL, 3)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (76, 45, 1, N'474oL2B.jpg', N'Embuscade', NULL, NULL, 4)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (77, 24, 1, N'tdw5nqZ.jpg', N'Assassinat', NULL, NULL, 5)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (78, 17, 0, N'ZumFAMu.jpg', N'Chasseur d''élite', 3, 5, 3)
INSERT [dbo].[Cards] ([id], [effect_id], [type], [image], [name], [life], [attack], [cost]) VALUES (79, 34, 0, N'qsOeKCa.jpg', N'Roi de la jungle', 7, 7, 7)
SET IDENTITY_INSERT [dbo].[Cards] OFF
SET IDENTITY_INSERT [dbo].[Decks] ON 

INSERT [dbo].[Decks] ([id], [user_id], [hero_id], [name], [card_1], [card_2], [card_3], [card_4], [card_5], [card_6], [card_7], [card_8], [card_9], [card_10], [card_11], [card_12], [card_13], [card_14], [card_15], [card_16], [card_17], [card_18], [card_19], [card_20], [card_21], [card_22], [card_23], [card_24], [card_25]) VALUES (19, 2, 1, N'MainDeck', 1, 2, 3, 6, 5, 4, 7, 8, 10, 40, 41, 42, 45, 44, 43, 46, 47, 48, 51, 50, 49, 22, 21, 20, 25)
INSERT [dbo].[Decks] ([id], [user_id], [hero_id], [name], [card_1], [card_2], [card_3], [card_4], [card_5], [card_6], [card_7], [card_8], [card_9], [card_10], [card_11], [card_12], [card_13], [card_14], [card_15], [card_16], [card_17], [card_18], [card_19], [card_20], [card_21], [card_22], [card_23], [card_24], [card_25]) VALUES (20, 1, 1, N'Invincible', 20, 20, 22, 22, 19, 18, 18, 17, 17, 25, 24, 24, 23, 23, 29, 29, 28, 28, 26, 26, 31, 31, 32, 32, 34)
INSERT [dbo].[Decks] ([id], [user_id], [hero_id], [name], [card_1], [card_2], [card_3], [card_4], [card_5], [card_6], [card_7], [card_8], [card_9], [card_10], [card_11], [card_12], [card_13], [card_14], [card_15], [card_16], [card_17], [card_18], [card_19], [card_20], [card_21], [card_22], [card_23], [card_24], [card_25]) VALUES (21, 3, 1, N'SecondDeck', 38, 38, 37, 37, 36, 36, 40, 40, 39, 39, 41, 41, 47, 47, 46, 46, 45, 45, 77, 77, 76, 76, 72, 72, 75)
INSERT [dbo].[Decks] ([id], [user_id], [hero_id], [name], [card_1], [card_2], [card_3], [card_4], [card_5], [card_6], [card_7], [card_8], [card_9], [card_10], [card_11], [card_12], [card_13], [card_14], [card_15], [card_16], [card_17], [card_18], [card_19], [card_20], [card_21], [card_22], [card_23], [card_24], [card_25]) VALUES (22, 4, 1, N'EpiStoneDeck', 38, 38, 36, 36, 34, 34, 31, 31, 32, 32, 30, 30, 29, 29, 26, 26, 24, 24, 22, 22, 20, 20, 18, 18, 16)
INSERT [dbo].[Decks] ([id], [user_id], [hero_id], [name], [card_1], [card_2], [card_3], [card_4], [card_5], [card_6], [card_7], [card_8], [card_9], [card_10], [card_11], [card_12], [card_13], [card_14], [card_15], [card_16], [card_17], [card_18], [card_19], [card_20], [card_21], [card_22], [card_23], [card_24], [card_25]) VALUES (23, 3, 1, N'Deck1', 2, 5, 8, 12, 15, 18, 21, 24, 28, 31, 65, 74, 79, 33, 35, 38, 41, 44, 47, 50, 55, 58, 61, 64, 70)
INSERT [dbo].[Decks] ([id], [user_id], [hero_id], [name], [card_1], [card_2], [card_3], [card_4], [card_5], [card_6], [card_7], [card_8], [card_9], [card_10], [card_11], [card_12], [card_13], [card_14], [card_15], [card_16], [card_17], [card_18], [card_19], [card_20], [card_21], [card_22], [card_23], [card_24], [card_25]) VALUES (24, 2, 3, N'HeroeDeck', 3, 6, 10, 13, 16, 19, 22, 25, 29, 32, 66, 78, 36, 39, 42, 45, 48, 51, 56, 59, 62, 68, 71, 75, 77)
INSERT [dbo].[Decks] ([id], [user_id], [hero_id], [name], [card_1], [card_2], [card_3], [card_4], [card_5], [card_6], [card_7], [card_8], [card_9], [card_10], [card_11], [card_12], [card_13], [card_14], [card_15], [card_16], [card_17], [card_18], [card_19], [card_20], [card_21], [card_22], [card_23], [card_24], [card_25]) VALUES (25, 1, 2, N'TokenDeck', 1, 4, 7, 11, 14, 17, 20, 23, 26, 30, 33, 67, 79, 34, 37, 40, 43, 46, 49, 54, 57, 60, 63, 69, 72)
INSERT [dbo].[Decks] ([id], [user_id], [hero_id], [name], [card_1], [card_2], [card_3], [card_4], [card_5], [card_6], [card_7], [card_8], [card_9], [card_10], [card_11], [card_12], [card_13], [card_14], [card_15], [card_16], [card_17], [card_18], [card_19], [card_20], [card_21], [card_22], [card_23], [card_24], [card_25]) VALUES (26, 4, 1, N'BasicDeck', 76, 77, 73, 72, 75, 36, 35, 34, 43, 44, 45, 55, 56, 54, 1, 2, 3, 79, 74, 11, 12, 13, 18, 17, 4)
SET IDENTITY_INSERT [dbo].[Decks] OFF
SET IDENTITY_INSERT [dbo].[Effects] ON 

INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (1, N'Boost de heros', N'2', N'Boost', N'Donne 2 points de vie à votre héros', 1, 0, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (3, N'Boost de monstres', N'+1/+1', N'Boost', N'Donne +1/+1 à un monstre', 0, 1, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (4, N'Boost de monstres', N'+0/+1', N'Boost', N'Donne 0/+1 à tous les monstres alliés', 0, 0, 1)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (5, N'Boost de monstres', N'+1/+0', N'Boost', N'Donne +1/0 à tous les monstres alliés', 0, 0, 1)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (6, N'Boost de monstres', N'+2/+2', N'Boost', N'Donne +2/+2 à un monstre', 0, 1, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (7, N'Boost de monstres', N'+4/1', N'Boost', N'Donne +4 à l''attaque et fixe les points de vie à 1 d''un monstre', 0, 1, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (8, N'Boost de monstres', N'+0/+1', N'Boost', N'Donne 0/+1 à un monstre', 0, 1, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (9, N'Boost de monstres', N'+3/+0', N'Boost', N'Donne +3/+0 à un monstre', 0, 1, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (10, N'Bouclier de monstres', N'0', N'Bouclier', N'Bloque tous les dégâts reçus de la prochaine attaque', 0, 1, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (11, N'Boost de monstres', N'10/10', N'Boost', N'Fixe l''attaque et les points de vie d''un monstre à 10', 0, 1, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (13, N'Charge de monstres', N'0', N'Charge', N'Peut attaquer directement', 0, 0, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (14, N'Degats sur heros', N'2', N'Degats', N'Inflige 2 points de dégâts au héros ennemi', 1, 0, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (15, N'Degats sur heros', N'1', N'Degats', N'Inflige 1 point de dégât au héros ennemi', 1, 0, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (16, N'Degats sur heros', N'1', N'Degats', N'Inflige 1 point de dégât au heros ennemi', 1, 0, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (17, N'Degats sur monstres', N'2', N'Degats', N'Inflige 2 points de dégâts à un monstre', 0, 1, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (18, N'Degats sur monstres', N'1', N'Degats', N'Inflige 1 point de dégât à tous les monstres ennemis', 0, 0, 1)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (19, N'Degats sur monstres', N'2', N'Degats', N'Inflige 2 points de dégâts à tous les monstres ennemis', 0, 0, 1)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (20, N'Degats sur monstres', N'3', N'Degats', N'Inflige 3 points de dégâts à un monstre', 0, 1, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (21, N'Degats sur tous', N'5', N'Degats', N'Inflige 5 points de dégâts à un monstre', 0, 1, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (22, N'Degats sur monstres', N'4', N'Degats', N'Inflige 4 points de dégâts à tous les monstres ennemis', 0, 0, 1)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (23, N'Gele de monstre', N'1', N'Gele', N'Gèle un monstre pendant 1 tour', 0, 1, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (24, N'Destruction', N'0', N'Mort', N'Détruit un monstre', 0, 1, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (25, N'Degats sur tous', N'7', N'Degats', N'Inflige 7 points de dégâts à un monstre', 0, 1, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (26, N'Destruction', N'0', N'Mort', N'Détruit tous les monstres ennemis', 0, 0, 1)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (27, N'Pioche de cartes', N'1', N'Pioche', N'Pioche 1 carte', 0, 0, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (28, N'Pioche de cartes', N'2', N'Pioche', N'Pioche 2 cartes', 0, 0, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (29, N'Pioche de cartes', N'4', N'Pioche', N'Pioche 4 cartes', 0, 0, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (30, N'Provocation', N'0', N'Provoc', N'Vous êtes obligés d''attaquer ce monstre', 0, 0, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (31, N'Provocation', N'0', N'Provoc', N'Donne provocation à un monstre', 0, 1, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (32, N'Gele de monstre', N'0', N'Gele', N'Gèle tous les monstres ennemis', 0, 0, 1)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (33, N'Boost de heros', N'4', N'Boost', N'Donne 4 points de vie au héros', 1, 0, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (34, N'Degats sur tous', N'4', N'Degats', N'Inflige 4 points de dégâts à un monstre', 0, 1, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (35, N'Boost de monstres', N'+2/+2', N'Boost', N'Donne +2/+2 à tous les monstres alliés', 0, 0, 1)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (36, N'Boost de heros', N'6', N'Boost', N'Donne 6 points de vie à votre héros', 1, 0, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (37, N'Boost de monstres', N'+3/+3', N'Boost', N'Donne +3/+3 à tous les monstres alliés', 0, 0, 1)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (38, N'Boost de heros', N'8', N'Boost', N'Donne 8 points de vie au héros', 1, 0, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (39, N'Boost de monstres', N'+0/+3', N'Boost', N'Donne 0/+3 à un monstre', 0, 1, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (40, N'Pioche de cartes', N'3', N'Pioche', N'Pioche 3 cartes', 0, 0, 0)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (41, N'Boost de monstres', N'+1/+1', N'Boost', N'Donne +1/+1 à tous les monstres alliés', 0, 0, 1)
INSERT [dbo].[Effects] ([id], [name], [value], [type], [description], [target_hero], [target_single_monster], [target_all_monster]) VALUES (45, N'Boost de monstre', N'0', N'Charge', N'Donne charge à un monstre', 0, 1, 0)
SET IDENTITY_INSERT [dbo].[Effects] OFF
SET IDENTITY_INSERT [dbo].[Heroes] ON 

INSERT [dbo].[Heroes] ([id], [name], [image], [card_1], [card_2], [card_3], [card_4], [card_5]) VALUES (1, N'Guerrier', N'Depme5f.jpg', 65, 66, 67, 68, 69)
INSERT [dbo].[Heroes] ([id], [name], [image], [card_1], [card_2], [card_3], [card_4], [card_5]) VALUES (2, N'Mage', N'aWzLnTH.jpg', 70, 71, 72, 73, 74)
INSERT [dbo].[Heroes] ([id], [name], [image], [card_1], [card_2], [card_3], [card_4], [card_5]) VALUES (3, N'Chasseur', N'ekTMWz6.jpg', 75, 76, 77, 78, 79)
SET IDENTITY_INSERT [dbo].[Heroes] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([id], [pseudo], [email], [password], [victories], [defeats], [draws], [score]) VALUES (1, N'goyat', N'goyat_n@epita.fr', N'nathan', 12, 2, 0, 38)
INSERT [dbo].[Users] ([id], [pseudo], [email], [password], [victories], [defeats], [draws], [score]) VALUES (2, N'grondin', N'grondi_a@epita.fr', N'david', 12, 3, 0, 39)
INSERT [dbo].[Users] ([id], [pseudo], [email], [password], [victories], [defeats], [draws], [score]) VALUES (3, N'tang', N'tang_n@epita.fr', N'nicolas', 8, 8, 5, 42)
INSERT [dbo].[Users] ([id], [pseudo], [email], [password], [victories], [defeats], [draws], [score]) VALUES (4, N'tran', N'tran_8@epita.fr', N'louis', 3, 4, 1, 15)
SET IDENTITY_INSERT [dbo].[Users] OFF
ALTER TABLE [dbo].[Cards]  WITH CHECK ADD  CONSTRAINT [FK_Cards_Effects] FOREIGN KEY([effect_id])
REFERENCES [dbo].[Effects] ([id])
GO
ALTER TABLE [dbo].[Cards] CHECK CONSTRAINT [FK_Cards_Effects]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards1] FOREIGN KEY([card_1])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards1]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards10] FOREIGN KEY([card_10])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards10]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards11] FOREIGN KEY([card_11])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards11]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards12] FOREIGN KEY([card_12])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards12]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards13] FOREIGN KEY([card_13])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards13]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards14] FOREIGN KEY([card_14])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards14]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards15] FOREIGN KEY([card_15])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards15]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards16] FOREIGN KEY([card_16])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards16]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards17] FOREIGN KEY([card_17])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards17]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards18] FOREIGN KEY([card_18])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards18]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards19] FOREIGN KEY([card_19])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards19]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards2] FOREIGN KEY([card_2])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards2]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards20] FOREIGN KEY([card_20])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards20]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards21] FOREIGN KEY([card_21])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards21]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards22] FOREIGN KEY([card_22])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards22]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards23] FOREIGN KEY([card_23])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards23]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards24] FOREIGN KEY([card_24])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards24]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards25] FOREIGN KEY([card_25])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards25]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards3] FOREIGN KEY([card_3])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards3]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards4] FOREIGN KEY([card_4])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards4]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards5] FOREIGN KEY([card_5])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards5]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards6] FOREIGN KEY([card_6])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards6]
GO
ALTER TABLE [dbo].[Decks]  WITH NOCHECK ADD  CONSTRAINT [FK_Decks_Cards7] FOREIGN KEY([card_7])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards7]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards8] FOREIGN KEY([card_8])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards8]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Cards9] FOREIGN KEY([card_9])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Cards9]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Heroes] FOREIGN KEY([hero_id])
REFERENCES [dbo].[Heroes] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Heroes]
GO
ALTER TABLE [dbo].[Decks]  WITH CHECK ADD  CONSTRAINT [FK_Decks_Users] FOREIGN KEY([user_id])
REFERENCES [dbo].[Users] ([id])
GO
ALTER TABLE [dbo].[Decks] CHECK CONSTRAINT [FK_Decks_Users]
GO
ALTER TABLE [dbo].[Heroes]  WITH CHECK ADD  CONSTRAINT [FK_Heroes_Cards1] FOREIGN KEY([card_1])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Heroes] CHECK CONSTRAINT [FK_Heroes_Cards1]
GO
ALTER TABLE [dbo].[Heroes]  WITH CHECK ADD  CONSTRAINT [FK_Heroes_Cards2] FOREIGN KEY([card_2])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Heroes] CHECK CONSTRAINT [FK_Heroes_Cards2]
GO
ALTER TABLE [dbo].[Heroes]  WITH CHECK ADD  CONSTRAINT [FK_Heroes_Cards4] FOREIGN KEY([card_4])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Heroes] CHECK CONSTRAINT [FK_Heroes_Cards4]
GO
ALTER TABLE [dbo].[Heroes]  WITH CHECK ADD  CONSTRAINT [FK_Heroes_Heroes3] FOREIGN KEY([card_3])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Heroes] CHECK CONSTRAINT [FK_Heroes_Heroes3]
GO
ALTER TABLE [dbo].[Heroes]  WITH CHECK ADD  CONSTRAINT [FK_Heroes_Heroes5] FOREIGN KEY([card_5])
REFERENCES [dbo].[Cards] ([id])
GO
ALTER TABLE [dbo].[Heroes] CHECK CONSTRAINT [FK_Heroes_Heroes5]
GO
USE [master]
GO
ALTER DATABASE [EpiStone] SET  READ_WRITE 
GO
