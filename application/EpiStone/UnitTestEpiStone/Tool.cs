﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestEpiStone
{
    public static class Tool
    {
        public static T CloneObj<T>(T obj)
        {
            var serialised = JsonConvert.SerializeObject(obj);
            return JsonConvert.DeserializeObject<T>(serialised);
        }

        public static readonly string baseUrl = "http://localhost:59165";

        public static void DeckAreEqual(EpiStone.DBO.Deck deck1, EpiStone.DBO.Deck deck2)
        {
            Assert.AreEqual(deck1.Id, deck2.Id);
            Assert.AreEqual(deck1.Name, deck2.Name);
            Assert.AreEqual(deck1.UserId, deck2.UserId);
            Assert.AreEqual(deck1.HeroId, deck2.HeroId);
            Assert.AreEqual(deck1.Card1, deck2.Card1);
            Assert.AreEqual(deck1.Card2, deck2.Card2);
            Assert.AreEqual(deck1.Card3, deck2.Card3);
            Assert.AreEqual(deck1.Card4, deck2.Card4);
            Assert.AreEqual(deck1.Card5, deck2.Card5);
            Assert.AreEqual(deck1.Card6, deck2.Card6);
            Assert.AreEqual(deck1.Card7, deck2.Card7);
            Assert.AreEqual(deck1.Card8, deck2.Card8);
            Assert.AreEqual(deck1.Card9, deck2.Card9);
            Assert.AreEqual(deck1.Card10, deck2.Card10);
            Assert.AreEqual(deck1.Card11, deck2.Card11);
            Assert.AreEqual(deck1.Card12, deck2.Card12);
            Assert.AreEqual(deck1.Card13, deck2.Card13);
            Assert.AreEqual(deck1.Card14, deck2.Card14);
            Assert.AreEqual(deck1.Card15, deck2.Card15);
            Assert.AreEqual(deck1.Card16, deck2.Card16);
            Assert.AreEqual(deck1.Card17, deck2.Card17);
            Assert.AreEqual(deck1.Card18, deck2.Card18);
            Assert.AreEqual(deck1.Card19, deck2.Card19);
            Assert.AreEqual(deck1.Card20, deck2.Card20);
            Assert.AreEqual(deck1.Card21, deck2.Card21);
            Assert.AreEqual(deck1.Card22, deck2.Card22);
            Assert.AreEqual(deck1.Card23, deck2.Card23);
            Assert.AreEqual(deck1.Card24, deck2.Card24);
            Assert.AreEqual(deck1.Card25, deck2.Card25);
        }
        public static void UsersAreEquals(EpiStone.DBO.User user1, EpiStone.DBO.User user2)
        {
            Assert.AreEqual(user1.Defeats, user2.Defeats);
            Assert.AreEqual(user1.Draws, user2.Draws);
            Assert.AreEqual(user1.Email, user2.Email);
            Assert.AreEqual(user1.Id, user2.Id);
            Assert.AreEqual(user1.Password, user2.Password);
            Assert.AreEqual(user1.Pseudo, user2.Pseudo);
            Assert.AreEqual(user1.Score, user2.Score);
            Assert.AreEqual(user1.Victories, user2.Victories);
        }
    }
}
