﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.QualityTools.Testing.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestEpiStone.DA
{
    [TestClass]
    public class UserTest
    {
        private EpiStone.DataAccess.Interface.IUser _da;
        private EpiStone.DBO.User user;
        public UserTest()
        {
            _da = new EpiStone.DataAccess.User();
            user = new EpiStone.DBO.User()
            {
                Defeats = 3,
                Draws = 2,
                Email = "test1@test.test",
                Id = 0,
                Password = "password",
                Pseudo = "user1",
                Score = 18,
                Victories = 6
            };
        }
        [TestMethod]
        public async Task TestCreateUserAsync()
        {
            string urlCreated = "";
            object objSend = null;
            EpiStone.DBO.User newUser = new EpiStone.DBO.User()
            {
                Pseudo = "pseudo",
                Email = "email",
                Password = "password"
            };
            using (ShimsContext.Create())
            {
                EpiStone.DataAccess.Fakes.ShimHTTPWrapper<EpiStone.DBO.User>.PostAsyncStringObject = (url, obj) =>
                {
                    urlCreated = url;
                    objSend = obj;
                    return Task.FromResult(new EpiStone.DBO.User());
                };
                var res = await _da.CreateUserAsync(newUser.Pseudo, newUser.Email, newUser.Password);
            }
            Assert.AreEqual(urlCreated, Tool.baseUrl + "/api/Users");
            Assert.IsNotNull(objSend);
            Assert.IsInstanceOfType(objSend, user.GetType());
            Tool.UsersAreEquals((EpiStone.DBO.User)objSend, newUser);
        }
        [TestMethod]
        public async Task TestDeleteUserAsync()
        {
            string urlCreated = "";
            int id = 5;
            using (ShimsContext.Create())
            {
                EpiStone.DataAccess.Fakes.ShimHTTPWrapper<bool>.DeleteAsyncString = (url) =>
                {
                    urlCreated = url;
                    return Task.FromResult(true);
                };

                var valid = await _da.DeleteUserAsync(id);
            }
            Assert.AreEqual(urlCreated, Tool.baseUrl + "/api/Users/" + id);
        }
        [TestMethod]
        public async Task TestGetScoresAsync()
        {
            string urlCreated = "";
            using (ShimsContext.Create())
            {
                EpiStone.DataAccess.Fakes.ShimHTTPWrapper<List<EpiStone.DBO.User>>.GetAsyncString = (url) =>
                {
                    urlCreated = url;
                    return Task.FromResult(new List<EpiStone.DBO.User>());
                };

                var deck = await _da.GetScoresAsync();
            }
            Assert.AreEqual(urlCreated, Tool.baseUrl + "/api/Scores");
        }
        [TestMethod]
        public async Task TestGetUserAsync()
        {
            string urlCreated = "";
            using (ShimsContext.Create())
            {
                EpiStone.DataAccess.Fakes.ShimHTTPWrapper<EpiStone.DBO.User>.GetAsyncString = (url) =>
                {
                    urlCreated = url;
                    return Task.FromResult(new EpiStone.DBO.User());
                };

                var deck = await _da.GetUserAsync(user.Pseudo, user.Password);
            }
            Assert.AreEqual(urlCreated, Tool.baseUrl + "/api/Users/" + user.Pseudo + "/" + user.Password);
        }
        [TestMethod]
        public async Task TestUpdateDeckAsync()
        {
            string urlCreated = "";
            object objSend = null;
            using (ShimsContext.Create())
            {
                EpiStone.DataAccess.Fakes.ShimHTTPWrapper<bool>.PutAsyncStringObject = (url, obj) =>
                {
                    urlCreated = url;
                    objSend = obj;
                    return Task.FromResult(true);
                };
                var list = await _da.UpdateUserAsync(user);
            }
            Assert.AreEqual(urlCreated, Tool.baseUrl + "/api/Users/" + user.Id);
            Assert.IsNotNull(objSend);
            Assert.IsInstanceOfType(objSend, user.GetType());
            Tool.UsersAreEquals((EpiStone.DBO.User)objSend, user);
        }
    }
}
