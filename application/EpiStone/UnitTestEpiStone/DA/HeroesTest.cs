﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.QualityTools.Testing.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestEpiStone.DA
{
    [TestClass]
    public class HeroesTest
    {
        private EpiStone.DataAccess.Interface.IHeroes _da;
        public HeroesTest()
        {
            _da = new EpiStone.DataAccess.Heroes();
        }
        [TestMethod]
        public async Task TestGetHeroesAsync()
        {
            string urlCreated = "";
            int id = 3;
            using (ShimsContext.Create())
            {
                EpiStone.DataAccess.Fakes.ShimHTTPWrapper<EpiStone.DBO.Heroes>.GetAsyncString = (url) =>
                {
                    urlCreated = url;
                    return Task.FromResult(new EpiStone.DBO.Heroes());
                };

                var deck = await _da.GetHeroesAsync(id);
            }
            Assert.AreEqual(urlCreated, Tool.baseUrl + "/api/Heroes/" + id);
        }
        [TestMethod]
        public async Task TestGetHeroesListAsync()
        {
            string urlCreated = "";
            using (ShimsContext.Create())
            {
                EpiStone.DataAccess.Fakes.ShimHTTPWrapper<List<EpiStone.DBO.Heroes>>.GetAsyncString = (url) =>
                {
                    urlCreated = url;
                    return Task.FromResult(new List<EpiStone.DBO.Heroes>());
                };

                var deck = await _da.GetHeroesListAsync();
            }
            Assert.AreEqual(urlCreated, Tool.baseUrl + "/api/Heroes");
        }
    }
}
