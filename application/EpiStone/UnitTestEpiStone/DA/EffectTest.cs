﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.QualityTools.Testing.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestEpiStone.DA
{
    [TestClass]
    public class EffectTest
    {
        private EpiStone.DataAccess.Interface.IEffect _da;
        public EffectTest()
        {
            _da = new EpiStone.DataAccess.Effect();
        }
        [TestMethod]
        public async Task TestGetEffectAsync()
        {
            string urlCreated = "";
            int id = 5;
            using (ShimsContext.Create())
            {
                EpiStone.DataAccess.Fakes.ShimHTTPWrapper<EpiStone.DBO.Effect>.GetAsyncString = (url) =>
                {
                    urlCreated = url;
                    return Task.FromResult(new EpiStone.DBO.Effect());
                };

                var deck = await _da.GetEffectAsync(id);
            }
            Assert.AreEqual(urlCreated, Tool.baseUrl + "/api/Effects/" + id);
        }

        [TestMethod]
        public async Task TestGetEffectsAsync()
        {
            string urlCreated = "";
            using (ShimsContext.Create())
            {
                EpiStone.DataAccess.Fakes.ShimHTTPWrapper<List<EpiStone.DBO.Effect>>.GetAsyncString = (url) =>
                {
                    urlCreated = url;
                    return Task.FromResult(new List<EpiStone.DBO.Effect>());
                };

                var deck = await _da.GetEffectsAsync();
            }
            Assert.AreEqual(urlCreated, Tool.baseUrl + "/api/Effects");
        }
    }
}
