﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.QualityTools.Testing.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestEpiStone.DA
{
    [TestClass]
    public class DeckTest
    {
        private EpiStone.DataAccess.Interface.IDeck _da;
        private EpiStone.DBO.Deck deck;

        public DeckTest()
        {
            _da = new EpiStone.DataAccess.Deck();
            deck = new EpiStone.DBO.Deck()
            {
                Card1 = 1,
                Card2 = 2,
                Card3 = 3,
                Card4 = 4,
                Card5 = 5,
                Card6 = 6,
                Card7 = 7,
                Card8 = 8,
                Card9 = 9,
                Card10 = 10,
                Card11 = 11,
                Card12 = 12,
                Card13 = 13,
                Card14 = 14,
                Card15 = 15,
                Card16 = 16,
                Card17 = 17,
                Card18 = 18,
                Card19 = 19,
                Card20 = 20,
                Card21 = 21,
                Card22 = 22,
                Card23 = 23,
                Card24 = 24,
                Card25 = 25,
                HeroId = 1,
                Id = 0,
                Name = "deck 1",
                UserId = 0
            };
        }
        [TestMethod]
        public async Task TestCreateDeckAsync()
        {
            string urlCreated = "";
            object objSend = null;
            using (ShimsContext.Create())
            {
                EpiStone.DataAccess.Fakes.ShimHTTPWrapper<List<EpiStone.DBO.Deck>>.PostAsyncStringObject = (url, obj) =>
                {
                    urlCreated = url;
                    objSend = obj;
                    return Task.FromResult(new List<EpiStone.DBO.Deck>());
                };
                var res = await _da.CreateDeckAsync(0, deck);
            }
            Assert.AreEqual(urlCreated, Tool.baseUrl + "/api/Decks");
            Assert.IsNotNull(objSend);
            Assert.IsInstanceOfType(objSend, deck.GetType());
            Tool.DeckAreEqual((EpiStone.DBO.Deck)objSend, deck);
        }
        [TestMethod]
        public async Task TestDeleteUserAsync()
        {
            string urlCreated = "";
            int id = 5;
            using (ShimsContext.Create())
            {
                EpiStone.DataAccess.Fakes.ShimHTTPWrapper<bool>.DeleteAsyncString = (url) =>
                {
                    urlCreated = url;
                    return Task.FromResult(true);
                };

                var valid = await _da.DeleteDeckAsync(id);
            }
            Assert.AreEqual(urlCreated, Tool.baseUrl + "/api/Decks/" + id);
        }
        [TestMethod]
        public async Task TestGetUserAsync()
        {
            string urlCreated = "";
            int id = 19;
            using (ShimsContext.Create())
            {
                EpiStone.DataAccess.Fakes.ShimHTTPWrapper<EpiStone.DBO.Deck>.GetAsyncString = (url) =>
                {
                    urlCreated = url;
                    return Task.FromResult(new EpiStone.DBO.Deck());
                };

                var deck = await _da.GetDeckAsync(id);
            }
            Assert.AreEqual(urlCreated, Tool.baseUrl + "/api/Decks/" + id);
        }
        [TestMethod]
        public async Task TestUpdateDeckAsync()
        {
            string urlCreated = "";
            object objSend = null;
            using (ShimsContext.Create())
            {
                EpiStone.DataAccess.Fakes.ShimHTTPWrapper<bool>.PutAsyncStringObject = (url, obj) =>
                {
                    urlCreated = url;
                    objSend = obj;
                    return Task.FromResult(true);
                };
                var list = await _da.UpdateDeckAsync(deck);
            }
            Assert.AreEqual(urlCreated, Tool.baseUrl + "/api/Decks/" + deck.Id);
            Assert.IsNotNull(objSend);
            Assert.IsInstanceOfType(objSend, deck.GetType());
            Tool.DeckAreEqual((EpiStone.DBO.Deck)objSend, deck);
        }
        [TestMethod]
        public async Task TestGetUserDecksAsync()
        {
            string urlCreated = "";
            int id = 13;
            using (ShimsContext.Create())
            {
                EpiStone.DataAccess.Fakes.ShimHTTPWrapper<List<EpiStone.DBO.Deck>>.GetAsyncString = (url) =>
                {
                    urlCreated = url;
                    return Task.FromResult(new List<EpiStone.DBO.Deck>());
                };

                var deck = await _da.GetUserDecksAsync(id);
            }
            Assert.AreEqual(urlCreated, Tool.baseUrl + "/api/Decks/user/" + id);
        }
    }
}
