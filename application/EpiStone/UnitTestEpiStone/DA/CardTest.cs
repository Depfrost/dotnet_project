﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.QualityTools.Testing.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestEpiStone.DA
{
    [TestClass]
    public class CardTest
    {
        private EpiStone.DataAccess.Interface.ICard _da;
        private readonly string base_url = "http://localhost:59165";
        public CardTest()
        {
            _da = new EpiStone.DataAccess.Card();
        }
        [TestMethod]
        public async Task TestGetCardsAsync()
        {
            using (ShimsContext.Create())
            {
                string urlCreated = "";
                EpiStone.DataAccess.Fakes.ShimHTTPWrapper<List<EpiStone.DBO.Card>>.GetAsyncString = (url) =>
                {
                    urlCreated = url;
                    return Task.FromResult(new List<EpiStone.DBO.Card>());
                };
                var list = await _da.GetCardsAsync();
                Assert.AreEqual(urlCreated, base_url + "/api/Cards");
            }
        }
        [TestMethod]
        public async Task TestGetCardAsync()
        {
            using (ShimsContext.Create())
            {
                string urlCreated = "";
                EpiStone.DataAccess.Fakes.ShimHTTPWrapper<EpiStone.DBO.Card>.GetAsyncString = (url) =>
                {
                    urlCreated = url;
                    return Task.FromResult(new EpiStone.DBO.Card());
                };
                int id = 3;
                var list = await _da.GetCardAsync(id);
                Assert.AreEqual(urlCreated, base_url + "/api/Cards/" + id);
            }
        }
    }
}
