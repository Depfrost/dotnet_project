﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestEpiStone.BM
{
    [TestClass]
    public class DeckTest
    {
        private EpiStone.BusinessManagement.Interface.IDeck _bm;
        private Mock.DataAccess.MockDeck _fakeDb;
        private EpiStone.DBO.Deck deck1;
        private EpiStone.DBO.Deck deck2;
        private EpiStone.DBO.User user;

        public DeckTest()
        {
            _fakeDb = new Mock.DataAccess.MockDeck();
            user = new EpiStone.DBO.User()
            {
                Defeats = 3,
                Draws = 5,
                Email = "test@test.test",
                Id = 0,
                Password = "password",
                Pseudo = "tester",
                Score = 10,
                Victories = 10
            };
            deck1 = new EpiStone.DBO.Deck()
            {
                Card1 = 1,
                Card2 = 2,
                Card3 = 3,
                Card4 = 4,
                Card5 = 5,
                Card6 = 6,
                Card7 = 7,
                Card8 = 8,
                Card9 = 9,
                Card10 = 10,
                Card11 = 11,
                Card12 = 12,
                Card13 = 13,
                Card14 = 14,
                Card15 = 15,
                Card16 = 16,
                Card17 = 17,
                Card18 = 18,
                Card19 = 19,
                Card20 = 20,
                Card21 = 21,
                Card22 = 22,
                Card23 = 23,
                Card24 = 24,
                Card25 = 25,
                HeroId = 1,
                Id = 0,
                Name = "deck 1",
                UserId = user.Id
            };
            deck2 = new EpiStone.DBO.Deck()
            {
                Card1 = 1,
                Card2 = 2,
                Card3 = 3,
                Card4 = 4,
                Card5 = 5,
                Card6 = 6,
                Card7 = 7,
                Card8 = 35,
                Card9 = 9,
                Card10 = 10,
                Card11 = 11,
                Card12 = 12,
                Card13 = 13,
                Card14 = 14,
                Card15 = 15,
                Card16 = 95,
                Card17 = 17,
                Card18 = 82,
                Card19 = 19,
                Card20 = 20,
                Card21 = 21,
                Card22 = 75,
                Card23 = 23,
                Card24 = 24,
                Card25 = 25,
                HeroId = 1,
                Id = 1,
                Name = "deck 2",
                UserId = user.Id + 1
            };

            _fakeDb.AddDeck(deck1);
            _fakeDb.AddDeck(deck2);
            _bm = new EpiStone.BusinessManagement.Deck(_fakeDb);
        }
        [TestMethod]
        public async Task TestCreateDeckAsync()
        {
            int oldSize = _fakeDb.GetSize();
            EpiStone.DBO.Deck newDeck = new EpiStone.DBO.Deck()
            {
                Card1 = 1,
                Card2 = 2,
                Card3 = 3,
                Card4 = 35,
                Card5 = 5,
                Card6 = 6,
                Card7 = 7,
                Card8 = 8,
                Card9 = 9,
                Card10 = 10,
                Card11 = 11,
                Card12 = 12,
                Card13 = 13,
                Card14 = 14,
                Card15 = 15,
                Card16 = 75,
                Card17 = 17,
                Card18 = 18,
                Card19 = 19,
                Card20 = 20,
                Card21 = 20,
                Card22 = 22,
                Card23 = 23,
                Card24 = 94,
                Card25 = 25,
                HeroId = 1,
                Id = 2,
                Name = "deck 3"
            };
            EpiStone.Settings.SetUser(user);
            var list = await _bm.CreateDeckAsync(newDeck);
            Assert.IsNotNull(list);
            Assert.AreEqual(_fakeDb.GetSize(), oldSize + 1);
            Assert.AreEqual(list.Count, 2);
            DeckAreEqual(list[0], deck1);
            DeckAreEqual(list[1], newDeck);
        }
        [TestMethod]
        public async Task TestDeleteDeckAsync()
        {
            var oldSize = _fakeDb.GetSize();
            Assert.IsTrue(await _bm.DeleteDeckAsync(deck1.Id));
            Assert.AreEqual(_fakeDb.GetSize(), oldSize - 1);
        }
        [TestMethod]
        public async Task TestGetDeckAsync()
        {
            var deck = await _bm.GetDeckAsync(deck1.Id);
            Assert.IsNotNull(deck);
            DeckAreEqual(deck, deck1);
        }
        [TestMethod]
        public async Task TestGetUserDecksAsync()
        {
            var list1 = await _bm.GetUserDecksAsync(0);
            Assert.IsNotNull(list1);
            Assert.AreEqual(list1.Count, 1);
            DeckAreEqual(list1[0], deck1);
            var list2 = await _bm.GetUserDecksAsync(1);
            Assert.IsNotNull(list2);
            Assert.AreEqual(list2.Count, 1);
            DeckAreEqual(list2[0], deck2);
        }
        [TestMethod]
        public async Task TestUpdateDeckAsync()
        {
            var oldSize = _fakeDb.GetSize();
            var newDeck = Tool.CloneObj(deck1);
            newDeck.Card11 = 150;
            newDeck.HeroId = 2;
            Assert.IsTrue(await _bm.UpdateDeckAsync(newDeck));
            Assert.AreEqual(_fakeDb.GetSize(), oldSize);
            DeckAreEqual(newDeck, _fakeDb.Find(deck1.Id));
        }

        private void DeckAreEqual(EpiStone.DBO.Deck deck1, EpiStone.DBO.Deck deck2)
        {
            Assert.AreEqual(deck1.Id, deck2.Id);
            Assert.AreEqual(deck1.Name, deck2.Name);
            Assert.AreEqual(deck1.UserId, deck2.UserId);
            Assert.AreEqual(deck1.HeroId, deck2.HeroId);
            Assert.AreEqual(deck1.Card1, deck2.Card1);
            Assert.AreEqual(deck1.Card2, deck2.Card2);
            Assert.AreEqual(deck1.Card3, deck2.Card3);
            Assert.AreEqual(deck1.Card4, deck2.Card4);
            Assert.AreEqual(deck1.Card5, deck2.Card5);
            Assert.AreEqual(deck1.Card6, deck2.Card6);
            Assert.AreEqual(deck1.Card7, deck2.Card7);
            Assert.AreEqual(deck1.Card8, deck2.Card8);
            Assert.AreEqual(deck1.Card9, deck2.Card9);
            Assert.AreEqual(deck1.Card10, deck2.Card10);
            Assert.AreEqual(deck1.Card11, deck2.Card11);
            Assert.AreEqual(deck1.Card12, deck2.Card12);
            Assert.AreEqual(deck1.Card13, deck2.Card13);
            Assert.AreEqual(deck1.Card14, deck2.Card14);
            Assert.AreEqual(deck1.Card15, deck2.Card15);
            Assert.AreEqual(deck1.Card16, deck2.Card16);
            Assert.AreEqual(deck1.Card17, deck2.Card17);
            Assert.AreEqual(deck1.Card18, deck2.Card18);
            Assert.AreEqual(deck1.Card19, deck2.Card19);
            Assert.AreEqual(deck1.Card20, deck2.Card20);
            Assert.AreEqual(deck1.Card21, deck2.Card21);
            Assert.AreEqual(deck1.Card22, deck2.Card22);
            Assert.AreEqual(deck1.Card23, deck2.Card23);
            Assert.AreEqual(deck1.Card24, deck2.Card24);
            Assert.AreEqual(deck1.Card25, deck2.Card25);
        }
    }
}
