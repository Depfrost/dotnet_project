﻿using System;
using System.Threading.Tasks;
using EpiStone.DBO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestEpiStone.BM
{
    [TestClass]
    public class CardTest
    {
        private EpiStone.BusinessManagement.Interface.ICard _bm;
        private Effect effect1;
        private Card card1;
        private Card card2;
        private Card card3;

        public CardTest()
        {
            var effects = new Mock.DataAccess.MockEffect();
            effect1 = new Effect()
            {
                Description = "effect desc",
                Id = 0,
                Name = "effect Name",
                TargetHero = false,
                TargetSingleMonster = true,
                Type = Effect.TypeEffect.Degats
            };
            effects.AddEffect(effect1);
            var cards = new Mock.DataAccess.MockCard();
            card1 = new Card()
            {
                Id = 1,
                Attack = 1,
                Cost = 1,
                Life = 2,
                EffectID = -1,
                Type = Card.TypeCard.Monstre,
                Image = "1P6TeUP",
                Name = "Nain ivre",
                Effect = null,
                SpecialId = -1
            };
            card2 = new Card()
            {
                Id = 7,
                Attack = -1,
                Cost = 5,
                Life = -1,
                EffectID = effect1.Id,
                Type = Card.TypeCard.Sort,
                Image = "1P6TeU",
                Name = "Boule de feu",
                Effect = null,
                SpecialId = -1
            };
            card3 = new Card()
            {
                Id = 2,
                Attack = 5,
                Cost = 3,
                Life = 4,
                EffectID = -1,
                Type = Card.TypeCard.Monstre,
                Image = "1P6TeUP2",
                Name = "Nain ivre 2",
                Effect = null,
                SpecialId = -1
            };
            cards.AddCard(card1);
            cards.AddCard(card2);
            cards.AddCard(card3);
            _bm = new EpiStone.BusinessManagement.Card(cards, effects);
        }
        [TestMethod]
        public async Task TestCardListAsync()
        {
            var list = await _bm.GetCardsAsync();
            Assert.IsNotNull(list);
            Assert.AreEqual(list.Count, 3);
            CardAreEqual(list[0], card1);
            CardAreEqual(list[1], card2);
            CardAreEqual(list[2], card3);
        }

        private void CardAreEqual(Card card1, Card card2)
        {
            Assert.AreEqual(card1.Id, card2.Id);
            Assert.AreEqual(card1.Attack, card2.Attack);
            Assert.AreEqual(card1.Cost, card2.Cost);
            Assert.AreEqual(card1.Life, card2.Life);
            Assert.AreEqual(card1.EffectID, card2.EffectID);
            Assert.AreEqual(card1.Type, card2.Type);
            Assert.AreEqual(card1.Image, card2.Image);
            Assert.AreEqual(card1.Name, card2.Name);
        }
    }
}
