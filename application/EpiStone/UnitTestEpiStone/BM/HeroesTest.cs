﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestEpiStone.BM
{
    [TestClass]
    public class HeroesTest
    {
        private EpiStone.BusinessManagement.Interface.IHeroes _bm;
        private EpiStone.DBO.Heroes heroes1;
        private EpiStone.DBO.Heroes heroes2;
        private EpiStone.DBO.Heroes heroes3;
        public HeroesTest()
        {
            var heroes = new Mock.DataAccess.MockHeroes();
            heroes1 = new EpiStone.DBO.Heroes()
            {
                Card1 = 0,
                Card2 = 1,
                Card3 = 2,
                Card4 = 3,
                Card5 = 4,
                Id = 0,
                Image = "toto.jgp",
                Name = "Heroes 1"
            };
            heroes2 = new EpiStone.DBO.Heroes()
            {
                Card1 = 10,
                Card2 = 11,
                Card3 = 12,
                Card4 = 13,
                Card5 = 14,
                Id = 1,
                Image = "toto2.jgp",
                Name = "Heroes 2"
            };
            heroes3 = new EpiStone.DBO.Heroes()
            {
                Card1 = 20,
                Card2 = 21,
                Card3 = 22,
                Card4 = 23,
                Card5 = 24,
                Id = 2,
                Image = "toto3.jgp",
                Name = "Heroes 3"
            };
            heroes.AddHeroes(heroes1);
            heroes.AddHeroes(heroes2);
            heroes.AddHeroes(heroes3);
            _bm = new EpiStone.BusinessManagement.Heroes(heroes);
        }
        [TestMethod]
        public async Task TestGetHeroesAsync()
        {
            var list = await _bm.GetHeroesListAsync();
            Assert.IsNotNull(list);
            Assert.AreEqual(list.Count, 3);
            HereosAreEquals(list[0], heroes1);
            HereosAreEquals(list[1], heroes2);
            HereosAreEquals(list[2], heroes3);
        }
        [TestMethod]
        public async Task TestMethod1Async()
        {
            var heroes = await _bm.GetHeroesAsync(1);
            Assert.IsNotNull(heroes);
            HereosAreEquals(heroes, heroes2);
        }

        private void HereosAreEquals(EpiStone.DBO.Heroes heroes1, EpiStone.DBO.Heroes heroes2)
        {
            Assert.AreEqual(heroes1.Card1, heroes2.Card1);
            Assert.AreEqual(heroes1.Card2, heroes2.Card2);
            Assert.AreEqual(heroes1.Card3, heroes2.Card3);
            Assert.AreEqual(heroes1.Card4, heroes2.Card4);
            Assert.AreEqual(heroes1.Card5, heroes2.Card5);
            Assert.AreEqual(heroes1.Id, heroes2.Id);
            Assert.AreEqual(heroes1.Image, heroes2.Image);
            Assert.AreEqual(heroes1.Name, heroes2.Name);
        }
    }
}
