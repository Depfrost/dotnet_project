﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestEpiStone.BM
{
    [TestClass]
    public class UserTest
    {
        private EpiStone.BusinessManagement.Interface.IUser _bm;
        private Mock.DataAccess.MockUser _fakeDb;
        private EpiStone.DBO.User user1;
        private EpiStone.DBO.User user2;
        private EpiStone.DBO.User user3;
        public UserTest()
        {
            _fakeDb = new Mock.DataAccess.MockUser();
            user1 = new EpiStone.DBO.User()
            {
                Defeats = 3,
                Draws = 2,
                Email = "test1@test.test",
                Id = 0,
                Password = "password",
                Pseudo = "user1",
                Score = 18,
                Victories = 6
            };
            user2 = new EpiStone.DBO.User()
            {
                Defeats = 5,
                Draws = 3,
                Email = "test2@test.test",
                Id = 1,
                Password = "password2",
                Pseudo = "user2",
                Score = 2,
                Victories = 1
            };
            user3 = new EpiStone.DBO.User()
            {
                Defeats = 5,
                Draws = 3,
                Email = "test3@test.test",
                Id = 2,
                Password = "password3",
                Pseudo = "user3",
                Score = 10,
                Victories = 3
            };
            _fakeDb.AddUser(user1);
            _fakeDb.AddUser(user2);
            _fakeDb.AddUser(user3);
            _bm = new EpiStone.BusinessManagement.User(_fakeDb);
        }
        [TestMethod]
        public async Task TestConnexionAsync()
        {
            var user = await _bm.Connexion(user1.Pseudo, user1.Password);
            Assert.IsNotNull(user);
            Tool.UsersAreEquals(user, user1);
        }
        [TestMethod]
        public async Task TestGetScoresAsync()
        {
            var list = await _bm.GetScoresAsync();
            Assert.IsNotNull(list);
            Assert.AreEqual(list.Count, 3);
            ScoresAreEquals(list[0], user1);
            ScoresAreEquals(list[1], user2);
            ScoresAreEquals(list[2], user3);
        }
        [TestMethod]
        public async Task TestInscriptionAsync()
        {
            var oldSize = _fakeDb.GetSize();
            var newUser = new EpiStone.DBO.User()
            {
                Email = "tester@test.test",
                Password = "newPassword",
                Pseudo = "tester"
            };
            var user = await _bm.Inscription(newUser.Pseudo, newUser.Email, newUser.Password);
            Assert.IsNotNull(user);
            newUser.Id = user.Id;
            Tool.UsersAreEquals(user, newUser);
            Assert.AreEqual(_fakeDb.GetSize(), oldSize + 1);
        }
        [TestMethod]
        public async Task TesUpdateAsync()
        {
            var oldSize = _fakeDb.GetSize();
            var updatedUser = Tool.CloneObj(user2);
            updatedUser.Email = "newEmail";
            updatedUser.Pseudo = "newPseudo";
            updatedUser.Password = "newPassword";
            Assert.IsTrue(await _bm.UpdateAsync(updatedUser));
            Tool.UsersAreEquals(updatedUser, _fakeDb.Get(user2.Id));
        }

        private void ScoresAreEquals(EpiStone.DBO.User user1, EpiStone.DBO.User user2)
        {
            Assert.AreEqual(user1.Defeats, user2.Defeats);
            Assert.AreEqual(user1.Draws, user2.Draws);
            Assert.AreEqual(user1.Pseudo, user2.Pseudo);
            Assert.AreEqual(user1.Score, user2.Score);
            Assert.AreEqual(user1.Victories, user2.Victories);
        }
    }
}
