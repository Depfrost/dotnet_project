﻿using System;
using System.Threading.Tasks;
using EpiStone.DBO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestEpiStone.BM
{
    [TestClass]
    public class EffectTest
    {
        private EpiStone.BusinessManagement.Interface.IEffect _bm;
        private Effect effect1;
        private Effect effect2;
        private Effect effect3;
        public EffectTest()
        {
            var effects = new Mock.DataAccess.MockEffect();
            effect1 = new Effect()
            {
                Description = "degat 1",
                Id = 0,
                Name = "Degats",
                TargetHero = false,
                TargetSingleMonster = true,
                Type = Effect.TypeEffect.Degats
            };
            effect2 = new Effect()
            {
                Description = "degat 2",
                Id = 1,
                Name = "Degats",
                TargetHero = true,
                TargetSingleMonster = false,
                Type = Effect.TypeEffect.Degats
            };
            effect3 = new Effect()
            {
                Description = "Heal 2",
                Id = 2,
                Name = "Heal",
                TargetHero = false,
                TargetSingleMonster = true,
                Type = Effect.TypeEffect.Boost
            };

            effects.AddEffect(effect1);
            effects.AddEffect(effect2);
            effects.AddEffect(effect3);
            _bm = new EpiStone.BusinessManagement.Effect(effects);
        }
        [TestMethod]
        public async Task TestGetEffectAsync()
        {
            var effect = await _bm.GetEffectAsync(1);
            Assert.IsNotNull(effect);
            EffectsAreEquals(effect, effect2);
        }
        [TestMethod]
        public async Task TestGetEffectsAsync()
        {
            var list = await _bm.GetEffectsAsync();
            Assert.IsNotNull(list);
            Assert.AreEqual(list.Count, 3);
            EffectsAreEquals(list[0], effect1);
            EffectsAreEquals(list[1], effect2);
            EffectsAreEquals(list[2], effect3);
        }

        public void EffectsAreEquals(Effect effect1, Effect effect2)
        {
            Assert.AreEqual(effect1.Description, effect2.Description);
            Assert.AreEqual(effect1.Id, effect2.Id);
            Assert.AreEqual(effect1.Name, effect2.Name);
            Assert.AreEqual(effect1.TargetHero, effect2.TargetHero);
            Assert.AreEqual(effect1.TargetSingleMonster, effect2.TargetSingleMonster);
            Assert.AreEqual(effect1.Type, effect2.Type);
        }
    }
}
