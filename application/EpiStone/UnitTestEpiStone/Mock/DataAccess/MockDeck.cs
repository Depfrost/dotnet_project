﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EpiStone.DBO;

namespace UnitTestEpiStone.Mock.DataAccess
{
    public class MockDeck : EpiStone.DataAccess.Interface.IDeck
    {
        private List<Deck> _db;
        public MockDeck()
        {
            _db = new List<Deck>();
        }

        public Task<List<Deck>> CreateDeckAsync(int id, Deck deck)
        {
            deck.UserId = id;
            _db.Add(deck);
            return Task.FromResult(_db.FindAll((item) => item.UserId == id));
        }

        public Task<bool> DeleteDeckAsync(int id)
        {
            var deck = _db.Find((item) => item.Id == id);
            if (deck == null)
                return Task.FromResult(false);
            _db.Remove(deck);
            return Task.FromResult(true);
        }

        public Task<Deck> GetDeckAsync(int id)
        {
            return Task.FromResult(_db.Find((item) => item.Id == id));
        }

        public Task<List<Deck>> GetUserDecksAsync(int idUser)
        {
            return Task.FromResult(_db.FindAll((deck) => deck.UserId == idUser));
        }

        public Task<bool> UpdateDeckAsync(Deck deck)
        {
            var index = _db.FindIndex((item) => item.Id == deck.Id);
            if (index == -1)
                return Task.FromResult(false);
            _db[index] = deck;
            return Task.FromResult(true);
        }

        public void AddDeck(Deck deck)
        {
            _db.Add(deck);
        }

        public int GetSize()
        {
            return _db.Count;
        }

        public Deck Get(int index)
        {
            return _db[index];
        }

        public Deck Find(int id)
        {
            return _db.Find((deck) => deck.Id == id);
        }
    }
}
