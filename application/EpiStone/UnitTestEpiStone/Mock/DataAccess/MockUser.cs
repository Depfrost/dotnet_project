﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EpiStone.DBO;

namespace UnitTestEpiStone.Mock.DataAccess
{
    public class MockUser : EpiStone.DataAccess.Interface.IUser
    {
        private List<User> _db;
        int index = 0;
        public MockUser()
        {
            _db = new List<User>();
        }
        public Task<User> CreateUserAsync(string pseudo, string email, string password)
        {
            User user = new User()
            {
                Id = index++,
                Email = email,
                Pseudo = pseudo,
                Password = password
            };
            _db.Add(user);
            return Task.FromResult(user);
        }

        public Task<bool> DeleteUserAsync(int id)
        {
            var user = _db.Find((item) => item.Id == id);
            if (user == null)
                return Task.FromResult(false);
            _db.Remove(user);
            return Task.FromResult(true);
        }

        public Task<List<User>> GetScoresAsync()
        {
            return Task.FromResult(_db.ToList());
        }

        public Task<User> GetUserAsync(string pseudo, string password)
        {
            var user = _db.Find((item) => item.Pseudo == pseudo && item.Password == password);
            return Task.FromResult(user);
        }

        public Task<bool> UpdateUserAsync(User user)
        {
            var index = _db.FindIndex((item) => item.Id == user.Id);
            if (index == -1)
                return Task.FromResult(false);
            _db[index] = user;
            return Task.FromResult(true);
        }

        public void AddUser(User user)
        {
            index = (index >= user.Id) ? index + 1 : user.Id + 1;
            _db.Add(user);
        }
        public int GetSize()
        {
            return _db.Count;
        }
        
        public User Get(int id)
        {
            return _db.Find((user) => user.Id == id);
        }
    }
}
