﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EpiStone.DBO;

namespace UnitTestEpiStone.Mock.DataAccess
{
    public class MockHeroes : EpiStone.DataAccess.Interface.IHeroes
    {
        private List<Heroes> _db;
        public MockHeroes()
        {
            _db = new List<Heroes>();
        }
        public Task<List<Heroes>> GetHeroesListAsync()
        {
            return Task.FromResult(_db.ToList());
        }

        public Task<Heroes> GetHeroesAsync(int id)
        {
            return Task.FromResult(_db.Find((heroes) => heroes.Id == id));
        }

        public void AddHeroes(Heroes heroes)
        {
            _db.Add(heroes);
        }
    }
}
