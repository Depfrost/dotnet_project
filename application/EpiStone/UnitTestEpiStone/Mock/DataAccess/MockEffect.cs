﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EpiStone.DBO;

namespace UnitTestEpiStone.Mock.DataAccess
{
    public class MockEffect : EpiStone.DataAccess.Interface.IEffect
    {
        private List<Effect> _db;
        public MockEffect()
        {
            _db = new List<Effect>();
        }
        public Task<Effect> GetEffectAsync(int id)
        {
            return Task.FromResult(_db.Find((effect) => effect.Id == id));
        }

        public Task<List<Effect>> GetEffectsAsync()
        {
            return Task.FromResult(_db.ToList());
        }
        public void AddEffect(Effect effect)
        {
            _db.Add(effect);
        }
    }
}
