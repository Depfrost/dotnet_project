﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EpiStone.DBO;

namespace UnitTestEpiStone.Mock.DataAccess
{
    class MockCard : EpiStone.DataAccess.Interface.ICard
    {
        private List<Card> _db;
        public MockCard()
        {
            _db = new List<Card>();
        }
        public Task<Card> GetCardAsync(int id)
        {
            return Task.FromResult(_db.Find((card) => card.Id == id));
        }

        public Task<List<Card>> GetCardsAsync()
        {
            return Task.FromResult(_db.ToList());
        }

        public void AddCard(Card card)
        {
            _db.Add(card);
        }
    }
}
