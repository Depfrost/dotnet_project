﻿using EpiStone.BusinessManagement.Interface;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace EpiStone.ViewModels
{
    public class RankingViewModel : BindableBase, INavigationAware
    {
        private IRegionManager _regionManager;
        private IUser _users;

        #region commands
        public DelegateCommand MenuCommand { get; set; }
        #endregion

        #region properties
        private ObservableCollection<Tuple<int, DBO.User>> _data;
        public ObservableCollection<Tuple<int, DBO.User>> Data
        {
            get { return _data; }
            set { SetProperty(ref _data, value); }
        }
        #endregion

        public RankingViewModel(IRegionManager regionManager, IUser users)
        {
            _regionManager = regionManager;
            _users = users;
            MenuCommand = new DelegateCommand(Menu);
        }

        private async void GetData()
        {
            List<DBO.User> users = await _users.GetScoresAsync();
            if (users == null)
            {
                return;
            }
            else
            {
                ObservableCollection<Tuple<int, DBO.User>> data = new ObservableCollection<Tuple<int, DBO.User>>();
                users = users.OrderByDescending(user => user.Score).ToList();
                int rankCount = 1;
                foreach (DBO.User user in users)
                {
                    data.Add(new Tuple<int, DBO.User>(rankCount, user));
                    rankCount++;
                }
                Data = data;
            }
        }

        private void Menu()
        {
            Navigate(new Uri(Views.Menu.ViewName, UriKind.Relative));
        }

        private void Navigate(Uri uri)
        {
            _regionManager.RequestNavigate(Views.MainWindow.MainRegionName, uri);
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            GetData();
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            
        }
    }
}
