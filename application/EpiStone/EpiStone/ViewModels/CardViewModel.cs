﻿using EpiStone.BusinessManagement.Interface;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace EpiStone.ViewModels
{
    public class CardViewModel : BindableBase, INavigationAware
    {
        public static readonly string CardParameter = "CardItem";
        Color color;
        
        private static readonly SolidColorBrush _geleStatusColor = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4C40FFF6"));
        private static readonly SolidColorBrush _provocStatusColor = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4CF35555"));
        private static readonly SolidColorBrush _bouclierStatusColor = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#4CF7ED0B"));
        private static readonly SolidColorBrush _defaultStatusColor = new SolidColorBrush(Colors.Transparent);
        private static readonly SolidColorBrush _canAttackColor = new SolidColorBrush(Colors.Green);//(Color)ColorConverter.ConvertFromString("#4C51FF00"));

        private readonly IEffect _effect;

        #region properties
        private DBO.Card _card;
        public DBO.Card Card
        {
            get { return _card; }
            set { SetProperty(ref _card, value); }
        }

        private ImageSource _image;
        public ImageSource Image
        {
            get { return _image; }
            set { SetProperty(ref _image, value); }
        }
        private string _description;
        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }
        private SolidColorBrush _borderColor;
        public SolidColorBrush BorderColor
        {
            get { return _borderColor; }
            set { SetProperty(ref _borderColor, value); }
        }
        private SolidColorBrush _cardBackground;
        public SolidColorBrush CardBackground
        {
            get { return _cardBackground; }
            set { SetProperty(ref _cardBackground, value); }
        }
        #endregion

        public CardViewModel(IEffect effect)
        {
            _effect = effect;
        }
        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            try
            {
                Card = (DBO.Card)navigationContext.Parameters[CardParameter];
            }
            catch (Exception e)
            {
                Debug.WriteLine(GetType().ToString() + " : " + nameof(OnNavigatedTo) +
                    " : cannot convert parameter to int : " + e.Message);
                return;
            }
            RefreshCard(false);
        }

        private Visibility _monsterVisibility;
        public Visibility MonsterVisibility
        {
            get { return _monsterVisibility; }
            set { SetProperty(ref _monsterVisibility, value); }
        }

        public void RefreshCard(bool isActive)
        {
            if (Card != null)
            {
                if (Image == null)
                {
                    MonsterVisibility = (Card.Type == DBO.Card.TypeCard.Monstre) ? Visibility.Visible : Visibility.Hidden;
                    Uri uriImage = new Uri(Settings.CreateRessourcePathCard(Card.Image), UriKind.Relative);
                    Image = new BitmapImage(uriImage);
                    if (Card.Effect != null)
                        Description = Card.Effect.Description;
                }
                if (Card is DBO.MonsterBoardCard)
                {
                    switch (((DBO.MonsterBoardCard)Card).Status)
                    {
                        case DBO.MonsterBoardCard.StatusCard.Bouclier:
                            CardBackground = _bouclierStatusColor;
                            break;
                        case DBO.MonsterBoardCard.StatusCard.Gele:
                            CardBackground = _geleStatusColor;
                            break;
                        case DBO.MonsterBoardCard.StatusCard.Provoc:
                            CardBackground = _provocStatusColor;
                            break;
                        case DBO.MonsterBoardCard.StatusCard.Charge:
                        case DBO.MonsterBoardCard.StatusCard.Aucun:
                            CardBackground = _defaultStatusColor;
                            break;
                    }
                    
                    switch (((DBO.MonsterBoardCard)Card).CanAttack)
                    {
                        case true:
                            BorderColor = _canAttackColor;
                            break;
                        case false:
                            BorderColor = _defaultStatusColor;
                            break;
                    }
                }
            }
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {

        }
    }
}
