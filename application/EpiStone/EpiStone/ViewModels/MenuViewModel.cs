﻿using Microsoft.AspNetCore.SignalR.Client;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EpiStone.ViewModels
{
    public class MenuViewModel : BindableBase
    {
        private IRegionManager _regionManager;

        #region commands
        public DelegateCommand ProfilCommand { get; set; }
        public DelegateCommand DisconnectCommand { get; set; }
        public DelegateCommand PlayCommand { get; set; }
        public DelegateCommand DeckCommand { get; set; }
        public DelegateCommand RankingCommand { get; set; }
        #endregion

        public MenuViewModel(IRegionManager regionManager)
        {
            _regionManager = regionManager;
            ProfilCommand = new DelegateCommand(Profil);
            DisconnectCommand = new DelegateCommand(Disconnect);
            PlayCommand = new DelegateCommand(Play);
            DeckCommand = new DelegateCommand(Deck);
            RankingCommand = new DelegateCommand(Ranking);
        }
        private void Navigate(Uri uri)
        {
            _regionManager.RequestNavigate(Views.MainWindow.MainRegionName, uri);
        }
        private void Profil()
        {
            Navigate(new Uri(Views.Profil.ViewName, UriKind.Relative));
        }
        private void Disconnect()
        {
            Settings.SetUser(null);
            Navigate(new Uri(Views.Login.ViewName, UriKind.Relative));
        }
        private void Play()
        {
            Navigate(new Uri(Views.DeckSelection.ViewName, UriKind.Relative));
        }
        private void Deck()
        {
            Navigate(new Uri(Views.DeckList.ViewName, UriKind.Relative));
        }
        private void Ranking()
        {
            Navigate(new Uri(Views.Ranking.ViewName, UriKind.Relative));
        }
    }
}
