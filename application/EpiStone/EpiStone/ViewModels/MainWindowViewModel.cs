﻿using EpiStone.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Windows.Media;

namespace EpiStone.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        #region Commands

        #endregion

        #region Private Members

        private string _title = "EpiStone";

        #endregion

        #region Public Properties
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }
        public double WindowMinimumWidth { get; set; } = 400;
        public double WindowMinimumHeight { get; set; } = 400;


        #endregion

        public MainWindowViewModel(IRegionViewRegistry regionManager)
        {
            regionManager.RegisterViewWithRegion(Views.MainWindow.MainRegionName, typeof(Login));
            //regionManager.RegisterViewWithRegion(Views.MainWindow.MainRegionName, typeof(Game));
        }
    }
}
