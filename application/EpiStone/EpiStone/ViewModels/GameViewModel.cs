﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using EpiStone.BusinessManagement.Interface;
using System.Windows;
using System.Windows.Media.Imaging;

namespace EpiStone.ViewModels
{
    public class GameViewModel : BindableBase, INavigationAware
    {
        private static readonly SolidColorBrush NoManaColor = new SolidColorBrush(Colors.Gray);
        private static readonly SolidColorBrush HasManaColor = new SolidColorBrush(Colors.DarkCyan);

        private static readonly SolidColorBrush WaitingPlayer = new SolidColorBrush(Colors.Transparent);
        private static readonly SolidColorBrush PlayingPlayer = new SolidColorBrush(Colors.Green);

        private IRegionManager _region;
        private IGame _game;
        private IEffect _effect;
        private List<DBO.Effect> _effectList;
        private List<int> _handsIds;
        private List<int> _enemyMonstersIds;
        private List<int> _myMonstersIds;
        private bool _enemyHasProvoc;

        #region properties
        private DBO.Board _board;
        public DBO.Board Board
        {
            get { return _board; }
            set { SetProperty(ref _board, value); }
        }

        private ObservableCollection<int> _enemyCards;
        public ObservableCollection<int> EnemyCards
        {
            get { return _enemyCards; }
            set { SetProperty(ref _enemyCards, value); }
        }

        private ObservableCollection<DBO.Card> _myCards;
        public ObservableCollection<DBO.Card> MyCards
        {
            get { return _myCards; }
            set { SetProperty(ref _myCards, value); }
        }

        private ObservableCollection<DBO.Card> _enemyMonsters;
        public ObservableCollection<DBO.Card> EnemyMonsters
        {
            get { return _enemyMonsters; }
            set { SetProperty(ref _enemyMonsters, value); }
        }

        private ObservableCollection<DBO.Card> _myMonsters;
        public ObservableCollection<DBO.Card> MyMonsters
        {
            get { return _myMonsters; }
            set { SetProperty(ref _myMonsters, value); }
        }

        private ImageSource _enemyHeroes;
        public ImageSource EnemyHeroes
        {
            get { return _enemyHeroes; }
            set { SetProperty(ref _enemyHeroes, value); }
        }

        private ImageSource _myHeroes;
        public ImageSource MyHeroes
        {
            get { return _myHeroes; }
            set { SetProperty(ref _myHeroes, value); }
        }

        private ObservableCollection<SolidColorBrush> _enemyMana;
        public ObservableCollection<SolidColorBrush> EnemyMana
        {
            get { return _enemyMana; }
            set { SetProperty(ref _enemyMana, value); }
        }

        private ObservableCollection<SolidColorBrush> _myMana;
        public ObservableCollection<SolidColorBrush> MyMana
        {
            get { return _myMana; }
            set { SetProperty(ref _myMana, value); }
        }

        private Predicate<DBO.Card> _isCardActive;
        public Predicate<DBO.Card> IsCardActive
        {
            get { return _isCardActive; }
            set { SetProperty(ref _isCardActive, value); }
        }

        private SolidColorBrush _playerColor;
        public SolidColorBrush PlayerColor
        {
            get { return _playerColor; }
            set { SetProperty(ref _playerColor, value); }
        }
        private SolidColorBrush _enemyPlayerColor;
        public SolidColorBrush EnemyPlayerColor
        {
            get { return _enemyPlayerColor; }
            set { SetProperty(ref _enemyPlayerColor, value); }
        }

        private Predicate<DBO.MonsterBoardCard> _ProvocCheck;
        public Predicate<DBO.MonsterBoardCard> ProvocCheck
        {
            get { return _ProvocCheck; }
            set { SetProperty(ref _ProvocCheck, value); }
        }

        private string _resultText;
        public string ResultText
        {
            get { return _resultText; }
            set { SetProperty(ref _resultText, value); }
        }

        private Visibility _resultVisibility;
        public Visibility ResultVisibility
        {
            get { return _resultVisibility; }
            set { SetProperty(ref _resultVisibility, value); }
        }

        private string _resultScore;
        public string ResultScore
        {
            get { return _resultScore; }
            set { SetProperty(ref _resultScore, value); }
        }
        #endregion

        #region commands
        public DelegateCommand GiveUpCommand { get; set; }
        public DelegateCommand EndTurnCommand { get; set; }
        public DelegateCommand<Tuple<DBO.Card, DBO.Card>> CardDropCommand { get; set; }
        public DelegateCommand MenuCommand { get; set; }
        #endregion
        public GameViewModel(IRegionManager region, IGame game, IEffect effect)
        {
            ResultVisibility = Visibility.Collapsed;
            _region = region;
            _game = game;
            _effect = effect;
            _handsIds = new List<int>();
            _enemyMonstersIds = new List<int>();
            _myMonstersIds = new List<int>();
            EnemyCards = new ObservableCollection<int>();
            MyCards = new ObservableCollection<DBO.Card>();
            EnemyMonsters = new ObservableCollection<DBO.Card>();
            MyMonsters = new ObservableCollection<DBO.Card>();
            EnemyMana = new ObservableCollection<SolidColorBrush>();
            MyMana = new ObservableCollection<SolidColorBrush>();
            for (int i = 0; i < 10; i++)
            {
                EnemyMana.Add(NoManaColor);
                MyMana.Add(NoManaColor);
            }

            GiveUpCommand = new DelegateCommand(GiveUp);
            EndTurnCommand = new DelegateCommand(EndTurn);
            CardDropCommand = new DelegateCommand<Tuple<DBO.Card, DBO.Card>>(DropOnMonsterHandler);
            MenuCommand = new DelegateCommand(Menu);
            IsCardActive = new Predicate<DBO.Card>((card) =>
            {
                if (!Board.PlayerTurn)
                    return false;
                if (card is DBO.MonsterBoardCard)
                {
                    return ((DBO.MonsterBoardCard)card).CanAttack;
                }
                else
                {
                    if (card.Type == DBO.Card.TypeCard.Monstre && MyMonsters.Count == 5)
                        return false;
                    return card.Cost <= Board.Player.Cristals;
                }

            });
            ProvocCheck = new Predicate<DBO.MonsterBoardCard>((card) =>
            {
                return !_enemyHasProvoc || (card != null && card.Status == DBO.MonsterBoardCard.StatusCard.Provoc);
            });

            InitBoard();
        }

        private void InitBoard()
        {
            Board = new DBO.Board()
            {
                PlayerTurn = true,
                //Turns = 0,
                Enemy = new DBO.EnemyPlayer()
                {
                    Cristals = 0,
                    Life = 30,
                    FieldMonster = new List<DBO.MonsterBoardCard>(),
                    NbCardDeck = 30
                    //NbCardHand = 2
                    //PlayerId = board.Enemy.PlayerId
                },
                Player = new DBO.UserPlayer()
                {
                    Cristals = 0,
                    Life = 30,
                    FieldMonster = new List<DBO.MonsterBoardCard>(),
                    NbCardDeck = 30,
                    PlayerHand = new List<DBO.Card>()
                    //PlayerId = Settings.GetUser().Id
                }
            };
            PlayerColor = PlayingPlayer;
            EnemyPlayerColor = WaitingPlayer;
        }

        public async void GiveUp()
        {
            await Settings.GameHubConnection.InvokeAsync("GiveUp", Settings.GameId, Settings.GetUser().Id);
            await Settings.GameHubConnection.DisposeAsync();
        }

        public void EndTurn()
        {
            if (!Board.PlayerTurn)
                return;
            _game.EndTurn();
        }
        
        private void Menu()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                _region.RequestNavigate(Views.MainWindow.MainRegionName, Views.Menu.ViewName);
            });
        }

        #region actionHandler
        private void DropOnMonsterHandler(Tuple<DBO.Card, DBO.Card> cards)
        {
            DBO.Card source = cards.Item1;
            DBO.Card target = cards.Item2;
            if (source is DBO.MonsterBoardCard)
            {
                _game.Attack(source.SpecialId, target.SpecialId);
            }
            else if (source.Type == DBO.Card.TypeCard.Monstre)
            {
                _game.PlayMonster(source.Id, source.SpecialId, target.SpecialId);
            }
            else
            {
                _game.PlaySpell(source.SpecialId, target.SpecialId);
            }
        }

        public void DropOnBoardHandler(DBO.Card card)
        {
            if (card.Type == DBO.Card.TypeCard.Monstre)
            {
                _game.PlayMonster(card.Id, card.SpecialId);
            }
            else
            {
                _game.PlaySpell(card.SpecialId);
            }
        }

        public void DropOnHeroes(DBO.Card card)
        {
            _game.Attack(card.SpecialId, -1);
        }
        #endregion

        #region navigation
        public async void OnNavigatedTo(NavigationContext navigationContext)
        {
            Settings.GameHubConnection.On<string>("Update", (json) =>
            {
                DBO.Board newBoard = JsonConvert.DeserializeObject<DBO.Board>(json);
                if (CheckEndGame(newBoard))
                    return;
                UpdateBoard(newBoard);
            });

            Settings.GameHubConnection.On<string>("error", async (param) =>
            {
                await Settings.GameHubConnection.InvokeAsync("GetBoardState", Settings.GameId);
            });

            _effectList = await _effect.GetEffectsAsync();
            await Settings.GameHubConnection.InvokeAsync("GetBoardState", Settings.GameId);
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            ResultVisibility = Visibility.Collapsed;
        }
        #endregion

        #region gameUpdate
        private bool CheckEndGame(DBO.Board board)
        {
            if (board.Enemy == null)
            {
                ResultText = "VICTOIRE !";
                ResultVisibility = Visibility.Visible;
                Settings.GetUser().Score += 3;
                ResultScore = "New Score: " + Settings.GetUser().Score;
                return true;
            }
            else if (board.Player == null)
            {
                ResultText = "DEFAITE !";
                ResultVisibility = Visibility.Visible;
                Settings.GetUser().Score += 1;
                ResultScore = "New Score: " + Settings.GetUser().Score;
                return true;
            }
            return false;
        }
        private void UpdateBoard(DBO.Board newBoard)
        {
            if (EnemyHeroes == null) // first call
            {
                Uri uriImage = new Uri(Settings.CreateRessourcePathHero(newBoard.Enemy.HeroImage), UriKind.Relative);
                Application.Current.Dispatcher.Invoke(() => EnemyHeroes = new BitmapImage(uriImage));

                uriImage = new Uri(Settings.CreateRessourcePathHero(newBoard.Player.HeroImage), UriKind.Relative);
                Application.Current.Dispatcher.Invoke(() => MyHeroes = new BitmapImage(uriImage));
            }
            if (newBoard.PlayerTurn != Board.PlayerTurn)
            {
                if (newBoard.PlayerTurn)
                {
                    PlayerColor = PlayingPlayer;
                    EnemyPlayerColor = WaitingPlayer;
                }
                else
                {
                    PlayerColor = WaitingPlayer;
                    EnemyPlayerColor = PlayingPlayer;
                }
            }
            /* Enemy Hand */
            UpdateEnemyHand(newBoard.Enemy.NbCardHand);
            /* Player Hand */
            UpdateCardList(MyCards, Board.Player.PlayerHand, newBoard.Player.PlayerHand, _handsIds);
            /* Enemy Monster */
            UpdateCardList(EnemyMonsters, Board.Enemy.FieldMonster, newBoard.Enemy.FieldMonster, _enemyMonstersIds);
            _enemyHasProvoc = EnemyMonsters.Any((card) => ((DBO.MonsterBoardCard)card).Status == DBO.MonsterBoardCard.StatusCard.Provoc);
            /* Player monsters */
            UpdateCardList(MyMonsters, Board.Player.FieldMonster, newBoard.Player.FieldMonster, _myMonstersIds);
            /* My mana */
            UpdateMana(MyMana, newBoard.Player.Cristals);
            /* Enemy mana */
            UpdateMana(EnemyMana, newBoard.Enemy.Cristals);
            Board = newBoard;
        }

        private void UpdateEnemyHand(long nbCard)
        {
            if (nbCard == EnemyCards.Count)
                return;
            while (nbCard > EnemyCards.Count)
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    EnemyCards.Add(0);
                });
            while (nbCard < EnemyCards.Count)
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    EnemyCards.RemoveAt(0);
                });
        }

        private void UpdateCardList<T>(ObservableCollection<DBO.Card> cards,
            List<T> oldCards, List<T> newCards, List<int> indexList) where T : DBO.Card
        {
            List<DBO.Card> newCardList = new List<DBO.Card>();
            foreach (var item in newCards)
            {
                if (item != null)
                {
                    newCardList.Add(item);
                }
            }
            if (newCardList.Count == 0)
            {
                indexList.Clear();
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    cards.Clear();
                });
                return;
            }
            List<int> newCardIds = newCardList.ConvertAll((card) => card.SpecialId);
            List<DBO.Card> oldCardList = new List<DBO.Card>();
            foreach (var item in oldCards)
            {
                if (item != null)
                {
                    oldCardList.Add(item);
                }

            }
            foreach (var oldCard in oldCardList)
            {
                DestroyCard(cards, oldCard.SpecialId, indexList);
            }
            foreach (var newCard in newCardList)
            {
                if (!indexList.Contains(newCard.SpecialId))
                    AddCards(cards, newCard, indexList);
                else if (newCard is DBO.MonsterBoardCard)
                {
                    UpdateCard(cards, GetCardIndex(cards, newCard.SpecialId), (DBO.MonsterBoardCard)newCard);
                }
            }
        }

        private int GetCardIndex(ObservableCollection<DBO.Card> list, int specialID)
        {
            int index = 0;
            foreach (var card in list)
            {
                if (card.SpecialId == specialID)
                    return index;
                index++;
            }
            // must not happen
            return -1;
        }

        private void UpdateMana(ObservableCollection<SolidColorBrush> mana, int nbMana)
        {
            if (nbMana == 0 || mana[nbMana - 1] == HasManaColor)
                for (int i = nbMana; i < 10 && mana[i] == HasManaColor; i++)
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        mana[i] = NoManaColor;
                    });
            else
                for (int i = nbMana - 1; i >= 0 && mana[i] == NoManaColor; i--)
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        mana[i] = HasManaColor;
                    });
        }

        private void AddCards(ObservableCollection<DBO.Card> list, DBO.Card card, List<int> indexList)
        {
            if (card.EffectID != -1)
                card.Effect = _effectList.FirstOrDefault(effet => effet.Id == card.EffectID);
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                list.Add(card);
            });
            indexList.Add(card.SpecialId);
        }
        private void UpdateCard(ObservableCollection<DBO.Card> cardList, int index, DBO.MonsterBoardCard update)
        {
            DBO.MonsterBoardCard old = (DBO.MonsterBoardCard)cardList[index];
            Application.Current.Dispatcher.Invoke(() => cardList[index] = update);

        }
        private void DestroyCard(ObservableCollection<DBO.Card> list, int cardId, List<int> indexList)
        {
            foreach (var item in list)
            {
                if (item.SpecialId == cardId)
                {
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        list.Remove(item);
                    });
                    indexList.Remove(item.SpecialId);
                    break;
                }
            }
        }
        #endregion
    }
}
