﻿using Microsoft.AspNetCore.SignalR.Client;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EpiStone.ViewModels
{
    public class WaitingRoomViewModel : BindableBase
    {
        private IRegionManager _regionManager;

        public DelegateCommand DeckSelectionCommand { get; set; }

        public WaitingRoomViewModel(IRegionManager regionManager)
        {
            _regionManager = regionManager;

            DeckSelectionCommand = new DelegateCommand(DeckSelection);
        }

        private async void DeckSelection()
        {
            await Settings.GameHubConnection.InvokeAsync("Disconnect", Settings.GetUser().Id);
            await Settings.GameHubConnection.DisposeAsync();
            Navigate(new Uri(Views.DeckSelection.ViewName, UriKind.Relative));
        }
        private void Navigate(Uri uri)
        {
            _regionManager.RequestNavigate(Views.MainWindow.MainRegionName, uri);
        }
    }
}
