﻿using EpiStone.BusinessManagement.Interface;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace EpiStone.ViewModels
{
    public class DeckListViewModel : BindableBase, INavigationAware
    {
        private IRegionManager _regionManager;
        private IHeroes _heroes;
        private IDeck _deck;

        #region properties
        private ObservableCollection<Tuple<DBO.Deck, string>> _data;
        public ObservableCollection<Tuple<DBO.Deck, string>> Data
        {
            get { return _data; }
            set { SetProperty(ref _data, value); }
        }

        private Visibility _noDeckSelectedVisibility;
        public Visibility NoDeckSelectedVisibility
        {
            get { return _noDeckSelectedVisibility; }
            set { SetProperty(ref _noDeckSelectedVisibility, value); }
        }
        #endregion

        #region commands
        public DelegateCommand NewDeckCommand { get; set; }
        public DelegateCommand<int?> DeckCommand { get; set; }
        public DelegateCommand MenuCommand { get; set; }
        public DelegateCommand<int?> DeleteDeckCommand { get; set; }
        #endregion

        public DeckListViewModel(IRegionManager regionManager, IHeroes heroes, IDeck deck)
        {
            _regionManager = regionManager;
            _heroes = heroes;
            _deck = deck;

            Data = new ObservableCollection<Tuple<DBO.Deck, string>>();

            NewDeckCommand = new DelegateCommand(NewDeck);
            DeckCommand = new DelegateCommand<int?>(Deck);
            MenuCommand = new DelegateCommand(Menu);
            DeleteDeckCommand = new DelegateCommand<int?>(DeleteDeck);
            NoDeckSelectedVisibility = Visibility.Collapsed;
        }

        private async void GetData()
        {
            int t = Settings.GetUser().Id;
            List<DBO.Deck> decks = await _deck.GetUserDecksAsync(t);
            if (decks == null)
            {
                return;
            }
            else
            {
                ObservableCollection<Tuple<DBO.Deck, string>> data = new ObservableCollection<Tuple<DBO.Deck, string>>();
                foreach (DBO.Deck d in decks)
                {
                    DBO.Heroes hero = await _heroes.GetHeroesAsync(d.HeroId);
                    data.Add(new Tuple<DBO.Deck, string>(d, "/Ressources/Images/HeroPortrait/" + hero.Image));
                }
                Data = data;
            }
        }

        private async void DeleteDeck(int? index)
        {
            if (index != null && index >= 0)
            {
                NoDeckSelectedVisibility = Visibility.Collapsed;
                bool deleted = await _deck.DeleteDeckAsync(Data[index.GetValueOrDefault()].Item1.Id);
                if (deleted)
                {
                    Data.RemoveAt(index.GetValueOrDefault());
                }
            }
            else
            {
                NoDeckSelectedVisibility = Visibility.Visible;
            }
        }

        private void Menu()
        {
            Navigate(new Uri(Views.Menu.ViewName, UriKind.Relative));
        }

        private void Deck(int? deckId)
        {
            if (deckId != null && deckId >= 0)
            {
                NoDeckSelectedVisibility = Visibility.Collapsed;
                var parameters = new NavigationParameters
                {
                    { DeckViewModel.IdParameter, deckId }
                };
                Navigate(new Uri(Views.Deck.ViewName + parameters.ToString(), UriKind.Relative));
            }
            else
            {
                NoDeckSelectedVisibility = Visibility.Visible;
            }
        }

        private void NewDeck()
        {
            var parameters = new NavigationParameters
            {
                { DeckViewModel.IdParameter, -1 }
            };
            Navigate(new Uri(Views.Deck.ViewName + parameters.ToString(), UriKind.Relative));
        }

        private void Navigate(Uri uri)
        {
            _regionManager.RequestNavigate(Views.MainWindow.MainRegionName, uri);
        }

        public void OnNavigatedTo(NavigationContext context)
        {
            GetData();
        }
        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
        }
    }
}
