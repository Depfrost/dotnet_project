﻿using EpiStone.BusinessManagement.Interface;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace EpiStone.ViewModels
{
    public class ProfilViewModel : BindableBase, INavigationAware
    {
        private IRegionManager _regionManager;
        private IUser _user;

        #region properties
        private string _pseudo;
        public string Pseudo
        {
            get { return _pseudo; }
            set { SetProperty(ref _pseudo, value); }
        }
        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }
        private long _score;
        public long Score
        {
            get { return _score; }
            set { SetProperty(ref _score, value); }
        }
        private long _victories;
        public long Victories
        {
            get { return _victories; }
            set { SetProperty(ref _victories, value); }
        }
        private long _defeat;
        public long Defeat
        {
            get { return _defeat; }
            set { SetProperty(ref _defeat, value); }
        }
        private long _draw;
        public long Draw
        {
            get { return _draw; }
            set { SetProperty(ref _draw, value); }
        }
        private Visibility _editErrorVisibility;
        public Visibility EditErrorVisibility
        {
            get { return _editErrorVisibility; }
            set { SetProperty(ref _editErrorVisibility, value); }
        }
        #endregion

        #region commands
        public DelegateCommand RankingCommand { get; set; }
        public DelegateCommand MenuCommand { get; set; }
        public DelegateCommand EditCommand { get; set; }
        #endregion

        public ProfilViewModel(IRegionManager regionManager, IUser user)
        {
            _regionManager = regionManager;
            _user = user;

            RankingCommand = new DelegateCommand(Ranking);
            MenuCommand = new DelegateCommand(Menu);
            EditCommand = new DelegateCommand(Edit);
            EditErrorVisibility = Visibility.Collapsed;
        }

        private async void Edit()
        {
            DBO.User user = new DBO.User
            {
                Pseudo = Pseudo,
                Email = Email,
                Id = Settings.GetUser().Id,
                Password = Settings.GetUser().Password,
                Score = Settings.GetUser().Score,
                Victories = Settings.GetUser().Victories,
                Defeats = Settings.GetUser().Defeats,
                Draws = Settings.GetUser().Draws
            };
            
            bool success = await _user.UpdateAsync(user);
            if (success)
            {
                Settings.SetUser(user);
                EditErrorVisibility = Visibility.Collapsed;
            }
            else
            {
                Pseudo = Settings.GetUser().Pseudo;
                Email = Settings.GetUser().Email;
                EditErrorVisibility = Visibility.Visible;
            }
        }

        private void Fill_informations()
        {
            DBO.User user = Settings.GetUser();
            Pseudo = user.Pseudo;
            Email = user.Email;
            Score = user.Score;
            Victories = user.Victories;
            Defeat = user.Defeats;
            Draw = user.Draws;
        }

        private void Ranking()
        {
            Navigate(new Uri(Views.Ranking.ViewName, UriKind.Relative));
        }

        private void Menu()
        {
            Navigate(new Uri(Views.Menu.ViewName, UriKind.Relative));
        }

        private void Navigate(Uri uri)
        {
            _regionManager.RequestNavigate(Views.MainWindow.MainRegionName, uri);
        }

        public void OnNavigatedTo(NavigationContext context)
        {
            Fill_informations();
        }
        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            EditErrorVisibility = Visibility.Collapsed;
        }
    }
}
