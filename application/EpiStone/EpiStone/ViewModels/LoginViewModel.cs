﻿using EpiStone.BusinessManagement.Interface;
using Microsoft.AspNetCore.SignalR.Client;
using EpiStone.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using Xceed.Wpf.Toolkit;

namespace EpiStone.ViewModels
{
    public class LoginViewModel : BindableBase
    {
        private IRegionManager _regionManager;
        private IUser _user;

        #region properties
        private string _pseudo;
        public string Pseudo
        {
            get { return _pseudo; }
            set { SetProperty(ref _pseudo, value); }
        }
        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private Visibility _errorVisibility;
        public Visibility ErrorVisibility
        {
            get { return _errorVisibility; }
            set { SetProperty(ref _errorVisibility, value); }
        }
        #endregion

        #region commands
        public DelegateCommand ConnexionCommand { get; set; }
        public DelegateCommand InscriptionCommand { get; set; }
        public DelegateCommand QuitCommand { get; set; }
        #endregion

        public LoginViewModel(IRegionManager regionManager, IUser user)
        {
            _regionManager = regionManager;
            _user = user;
            ConnexionCommand = new DelegateCommand(ConnexionAsync);
            InscriptionCommand = new DelegateCommand(Inscription);
            QuitCommand = new DelegateCommand(Quit);
            ErrorVisibility = Visibility.Collapsed;
        }
        private void Navigate(Uri uri)
        {
            _regionManager.RequestNavigate(Views.MainWindow.MainRegionName, uri);
        }
        private async void ConnexionAsync()
        {
            DBO.User user = await _user.Connexion(Pseudo, Password);
            if (user == null)
            {
                ErrorVisibility = Visibility.Visible;
                return;
            }
            else
            {
                ErrorVisibility = Visibility.Collapsed;
                Settings.SetUser(user);
            }
            Navigate(new Uri(Views.Menu.ViewName, UriKind.Relative));
        }

        private void Inscription()
        {
            var uri = new Uri(Views.Inscription.ViewName, UriKind.Relative);
            Navigate(uri);
        }
        private void Quit()
        {
            System.Windows.Application.Current.Shutdown();
        }
    }
}
