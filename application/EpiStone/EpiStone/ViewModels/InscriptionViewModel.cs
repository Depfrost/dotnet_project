﻿using EpiStone.BusinessManagement.Interface;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace EpiStone.ViewModels
{
    public class InscriptionViewModel : BindableBase
    {
        private IRegionManager _regionManager;
        private IUser _user;

        #region properties
        private string _pseudo;
        public string Pseudo
        {
            get { return _pseudo; }
            set { SetProperty(ref _pseudo, value); }
        }
        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }
        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }
        private string _password2;
        public string Password2
        {
            get { return _password2; }
            set { SetProperty(ref _password2, value); }
        }

        private Visibility _passwordErrorVisibility;
        public Visibility PasswordErrorVisibility
        {
            get { return _passwordErrorVisibility; }
            set { SetProperty(ref _passwordErrorVisibility, value); }
        }

        private Visibility _pseudoErrorVisibility;
        public Visibility PseudoErrorVisibility
        {
            get { return _pseudoErrorVisibility; }
            set { SetProperty(ref _pseudoErrorVisibility, value); }
        }
        
        private Visibility _emailErrorVisibility;
        public Visibility EmailErrorVisibility
        {
            get { return _emailErrorVisibility; }
            set { SetProperty(ref _emailErrorVisibility, value); }
        }
        #endregion

        #region commands
        public DelegateCommand InscriptionCommand { get; set; }
        //public DelegateCommand<string> RetourCommand { get; set; }
        public DelegateCommand RetourCommand { get; set; }
        #endregion
        public InscriptionViewModel(IRegionManager regionManager, IUser user)
        {
            _regionManager = regionManager;
            _user = user;
            RetourCommand = new DelegateCommand(Retour);
            InscriptionCommand = new DelegateCommand(Inscription);
            PseudoErrorVisibility = Visibility.Collapsed;
            PasswordErrorVisibility = Visibility.Collapsed;
            EmailErrorVisibility = Visibility.Collapsed;
        }
        private void Navigate(Uri uri)
        {
            _regionManager.RequestNavigate(Views.MainWindow.MainRegionName, uri);
        }
        private async void Inscription()
        {
            if (Email == "")
            {
                EmailErrorVisibility = Visibility.Visible;
                return;
            }
            EmailErrorVisibility = Visibility.Collapsed;
            if (Password != Password2)
            {
                PasswordErrorVisibility = Visibility.Visible;
                return;
            }
            PseudoErrorVisibility = Visibility.Collapsed;
            DBO.User user = await _user.Inscription(Pseudo, Email, Password);
            if (user == null)
            {
                PseudoErrorVisibility = Visibility.Visible;
                return;
            }
            Settings.SetUser(user);
            PasswordErrorVisibility = Visibility.Collapsed;
            Navigate(new Uri(Views.Menu.ViewName, UriKind.Relative));
        }
        private void Retour()
        {
            Uri uri = new Uri(Views.Login.ViewName, UriKind.Relative);
            Navigate(uri);
        }
    }
}
