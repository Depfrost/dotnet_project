﻿using EpiStone.BusinessManagement.Interface;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace EpiStone.ViewModels
{
    public enum FilterType
    {
        Tous,
        Monstre,
        Sort
    }

    public class DeckViewModel : BindableBase, INavigationAware
    {
        public static string IdParameter = "IdDeck";

        private IRegionManager _regionManager;
        private IDeck _deck;
        private ICard _card;
        private IHeroes _heroes;
        private List<int> _cardsId;
        private bool _isNewDeck;
        private int _deckId;
        
        public class CardItem : BindableBase
        {
            private int _nb;
            public int Nb
            {
                get { return _nb; }
                set { SetProperty(ref _nb, value); }
            }
            private DBO.Card _card;
            public DBO.Card Card
            {
                get { return _card; }
                set { SetProperty(ref _card, value); }
            }
            private Visibility _isVisible;
            public Visibility IsVisible
            {
                get { return _isVisible; }
                set { SetProperty(ref _isVisible, value); }
            }

            public DelegateCommand AddingCommand { get; set; }
            public DelegateCommand RemovingCommand { get; set; }
        }

        #region properties

        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set { SetProperty(ref _searchText, value); }
        }

        private int _monsterNb;
        public int MonsterNb
        {
            get { return _monsterNb; }
            set
            {
                SetProperty(ref _monsterNb, value);
                CardsNb = _monsterNb + _spellNb;
            }
        }
        private int _spellNb;
        public int SpellNb
        {
            get { return _spellNb; }
            set
            {
                SetProperty(ref _spellNb, value);
                CardsNb = _monsterNb + _spellNb;
            }
        }
        private int _cardsNb = 0;
        public int CardsNb
        {
            get { return _cardsNb; }
            set { SetProperty(ref _cardsNb, value); }
        }
        private ObservableCollection<string> _cardsName;
        public ObservableCollection<string> CardsName
        {
            get { return _cardsName; }
            set { SetProperty(ref _cardsName, value); }
        }
        private string _selectedCard;
        public string SelectedCard
        {
            get { return _selectedCard; }
            set { SetProperty(ref _selectedCard, value); }
        }

        private ObservableCollection<CardItem> _cards;
        public ObservableCollection<CardItem> Cards
        {
            get { return _cards; }
            set { SetProperty(ref _cards, value); }
        }

        private FilterType _selectedFilter;
        public FilterType SelectedFilter
        {
            get { return _selectedFilter; }
            set { SetProperty(ref _selectedFilter, value); UpdateVisibleCards(SearchText); }
        }
        private Visibility _errorVisibility;
        public Visibility ErrorVisibility
        {
            get { return _errorVisibility; }
            set { SetProperty(ref _errorVisibility, value); }
        }

        private DBO.Heroes _selectedHeroes;
        public DBO.Heroes SelectedHeroes
        {
            get { return _selectedHeroes; }
            set { SetProperty(ref _selectedHeroes, value); }
        }

        private string _deckName;
        public string DeckName
        {
            get { return _deckName; }
            set { SetProperty(ref _deckName, value); }
        }

        private ObservableCollection<DBO.Heroes> _heroesList;
        public ObservableCollection<DBO.Heroes> HeroesList
        {
            get { return _heroesList; }
            set { SetProperty(ref _heroesList, value); }
        }
        #endregion

        #region commands
        public DelegateCommand ValidationCommand { get; set; }
        public DelegateCommand CancelationCommand { get; set; }
        public DelegateCommand ErrorCommand { get; set; }
        #endregion

        public DeckViewModel(IRegionManager regionManager, ICard card, IDeck deck, IHeroes heroes)
        {
            _regionManager = regionManager;
            _cardsId = new List<int>();
            _card = card;
            _deck = deck;
            _heroes = heroes;
            Cards = new ObservableCollection<CardItem>();
            CardsName = new ObservableCollection<string>();
            ValidationCommand = new DelegateCommand(ValidationAsync);
            CancelationCommand = new DelegateCommand(Cancelation);
            HeroesList = new ObservableCollection<DBO.Heroes>();
            SearchText = "";
            ErrorVisibility = Visibility.Collapsed;
            ErrorCommand = new DelegateCommand(() => ErrorVisibility = Visibility.Collapsed);
        }

        public async void OnNavigatedTo(NavigationContext navigationContext)
        {
            try
            {
                _deckId = Convert.ToInt32(navigationContext.Parameters[IdParameter]);
                _isNewDeck = _deckId == -1;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return;
            }
            var cardsTask = _card.GetCardsAsync();
            var heroesTask = _heroes.GetHeroesListAsync();
            foreach (var card in await cardsTask)
            {
                AddCard(card);
                CardsName.Add(card.Name);
            }
            foreach (var heroes in await heroesTask)
            {
                HeroesList.Add(heroes);
            }

            SelectedHeroes = HeroesList.First();
            DeckName = "NewDeck";
            SelectedFilter = FilterType.Tous;
            SearchText = "";
            // Load existing deck if we are editing a deck
            if (!_isNewDeck)
                LoadDeck(_deckId);
        }

        private void AddCard(DBO.Card card)
        {
            var tmp = new CardItem()
            {
                Card = card,
                Nb = 0,
                IsVisible = Visibility.Visible
            };
            tmp.AddingCommand = new DelegateCommand(() =>
            {
                if (tmp.Nb == 2 || CardsNb == 25)
                    return;
                tmp.Nb++;
                _cardsId.Add(tmp.Card.Id);
                if (card.Type == DBO.Card.TypeCard.Monstre)
                    MonsterNb++;
                else
                    SpellNb++;
            });
            tmp.RemovingCommand = new DelegateCommand(() =>
            {
                if (tmp.Nb == 0)
                    return;
                tmp.Nb--;
                _cardsId.Remove(tmp.Card.Id);
                if (card.Type == DBO.Card.TypeCard.Monstre)
                    MonsterNb--;
                else
                    SpellNb--;
            });
            Cards.Add(tmp);
        }

        public void UpdateVisibleCards(string text)
        {
            SearchText = text;
            foreach (CardItem item in Cards)
            {
                bool searchParam = SearchText == "" || item.Card.Name.ToLower().Contains(SearchText.ToLower());
                if ((SelectedFilter == FilterType.Tous && searchParam)
                    || (SelectedFilter == FilterType.Monstre && item.Card.Type == DBO.Card.TypeCard.Monstre && searchParam)
                    || (SelectedFilter == FilterType.Sort && item.Card.Type == DBO.Card.TypeCard.Sort && searchParam))
                    item.IsVisible = Visibility.Visible;
                else
                    item.IsVisible = Visibility.Collapsed;
            }
        }

        private async void LoadDeck(int id)
        {
            // Get the deck currently modified
            DBO.Deck existingDeck = await _deck.GetDeckAsync(id);
            if (existingDeck == null)
                return;

            // Set the deck name and hero
            DeckName = existingDeck.Name;
            SelectedHeroes = HeroesList.FirstOrDefault(hero => hero.Id == existingDeck.HeroId);

            // Load cards form existing deck to the view
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card1).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card2).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card3).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card4).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card5).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card6).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card7).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card8).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card9).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card10).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card11).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card12).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card13).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card14).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card15).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card16).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card17).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card18).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card19).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card20).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card21).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card22).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card23).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card24).AddingCommand.Execute();
            Cards.FirstOrDefault(card => card.Card.Id == existingDeck.Card25).AddingCommand.Execute();
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            // Clear previous data
            ErrorVisibility = Visibility.Collapsed;
            CardsNb = 0;
            MonsterNb = 0;
            SpellNb = 0;
            _cardsId.Clear();
            HeroesList.Clear();
            Cards.Clear();
            CardsName.Clear();
        }

        public async void ValidationAsync()
        {
            if (CardsNb != 25)
            {
                ErrorVisibility = Visibility.Visible;
                return;
            }
            DBO.Deck deck = CreateDeck();
            if (deck == null)
                return;
            if (_isNewDeck)
            {
                if (await _deck.CreateDeckAsync(deck) == null)
                    return;
            }
            else
            {
                deck.Id = _deckId;
                if (await _deck.UpdateDeckAsync(deck) == false)
                    return;
            }

            _regionManager.RequestNavigate(Views.MainWindow.MainRegionName, new Uri(Views.DeckList.ViewName, UriKind.Relative));
        }

        public void Cancelation()
        {
            _regionManager.RequestNavigate(Views.MainWindow.MainRegionName, new Uri(Views.DeckList.ViewName, UriKind.Relative));
        }

        private DBO.Deck CreateDeck()
        {
            return new DBO.Deck()
            {
                UserId = Settings.GetUser().Id,
                HeroId = SelectedHeroes.Id,
                Name = DeckName,
                Card1 = _cardsId[0],
                Card2 = _cardsId[1],
                Card3 = _cardsId[2],
                Card4 = _cardsId[3],
                Card5 = _cardsId[4],
                Card6 = _cardsId[5],
                Card7 = _cardsId[6],
                Card8 = _cardsId[7],
                Card9 = _cardsId[8],
                Card10 = _cardsId[9],
                Card11 = _cardsId[10],
                Card12 = _cardsId[11],
                Card13 = _cardsId[12],
                Card14 = _cardsId[13],
                Card15 = _cardsId[14],
                Card16 = _cardsId[15],
                Card17 = _cardsId[16],
                Card18 = _cardsId[17],
                Card19 = _cardsId[18],
                Card20 = _cardsId[19],
                Card21 = _cardsId[20],
                Card22 = _cardsId[21],
                Card23 = _cardsId[22],
                Card24 = _cardsId[23],
                Card25 = _cardsId[24]
            };
        }
    }
}
