﻿using EpiStone.BusinessManagement.Interface;
using Microsoft.AspNetCore.SignalR.Client;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace EpiStone.ViewModels
{
    public class DeckSelectionViewModel : BindableBase, INavigationAware
    {
        private IRegionManager _regionManager;
        private IDeck _deck;
        private IHeroes _heroes;

        #region properties
        private ObservableCollection<Tuple<DBO.Deck, string>> _data;
        public ObservableCollection<Tuple<DBO.Deck, string>> Data
        {
            get { return _data; }
            set { SetProperty(ref _data, value); }
        }
        #endregion

        #region commands
        public DelegateCommand MenuCommand { get; set; }
        public DelegateCommand<int?> SearchGameCommand { get; set; }
        #endregion

        public DeckSelectionViewModel(IRegionManager regionManager, IDeck deck, IHeroes heroes)
        {
            _regionManager = regionManager;
            _deck = deck;
            _heroes = heroes;

            MenuCommand = new DelegateCommand(Menu);
            SearchGameCommand = new DelegateCommand<int?>(SearchGame);
        }

        private async void SearchGame(int? deckId)
        {
            if (deckId != null)
            {
                Navigate(new Uri(Views.WaitingRoom.ViewName, UriKind.Relative));

                Settings.GameHubConnection = new HubConnectionBuilder().WithUrl("http://localhost:59165/game/").Build();
                Settings.GameHubConnection.On<string, long, bool>("find_ennemy", (gameId, enemyId, isPlayer1) =>
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        Settings.IsPlayer1 = isPlayer1;
                        Settings.GameId = gameId;

                        _regionManager.RequestNavigate(Views.MainWindow.MainRegionName, new Uri(Views.Game.ViewName, UriKind.Relative));
                    });
                });

                await Settings.GameHubConnection.StartAsync();

                //launch mathmaking
                await Settings.GameHubConnection.InvokeAsync("FindGame", Settings.GetUser().Id, deckId);
            }
        }

        private async void GetData()
        {
            int t = Settings.GetUser().Id;
            List<DBO.Deck> decks = await _deck.GetUserDecksAsync(t);
            if (decks == null)
            {
                return;
            }
            else
            {
                ObservableCollection<Tuple<DBO.Deck, string>> data = new ObservableCollection<Tuple<DBO.Deck, string>>();
                foreach (DBO.Deck d in decks)
                {
                    DBO.Heroes hero = await _heroes.GetHeroesAsync(d.HeroId);
                    data.Add(new Tuple<DBO.Deck, string>(d, "/Ressources/Images/HeroPortrait/" + hero.Image));
                }
                Data = data;
            }
        }

        private void Menu()
        {
            Navigate(new Uri(Views.Menu.ViewName, UriKind.Relative));
        }

        private void Navigate(Uri uri)
        {
            _regionManager.RequestNavigate(Views.MainWindow.MainRegionName, uri);
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            GetData();
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
        }
    }
}
