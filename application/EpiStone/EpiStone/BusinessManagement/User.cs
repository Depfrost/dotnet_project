﻿using EpiStone.BusinessManagement.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement
{
    public class User : IUser
    {
        private DataAccess.Interface.IUser _da;
        public User(DataAccess.Interface.IUser da)
        {
            _da = da;
        }
        public async Task<DBO.User> Connexion(string pseudo, string password)
        {
            return await _da.GetUserAsync(pseudo, password);
        }
        public async Task<DBO.User> Inscription(string pseudo, string email, string password)
        {
            return await _da.CreateUserAsync(pseudo, email, password);
        }      
        public async Task<bool> UpdateAsync(DBO.User user)
        {
            return await _da.UpdateUserAsync(user);
        }
        public async Task<bool> Suppression(DBO.User user)
        {
            return await _da.DeleteUserAsync(user.Id);
        }
        public async Task<List<DBO.User>> GetScoresAsync()
        {
            return await _da.GetScoresAsync();
        }
    }
}
