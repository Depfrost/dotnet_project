﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement.Interface
{
    public interface IDeck
    {
        /// <summary>
        /// Get a deck.
        /// </summary>
        /// <param name="id">The id of the deck.</param>
        /// <returns>The deck of the user if found, null otherwise.</returns>
        Task<DBO.Deck> GetDeckAsync(int id);
        /// <summary>
        /// Get the decks of a user.
        /// </summary>
        /// <param name="idUser">The id of the user.</param>
        /// <returns>The list of the deck, or null if something went wrong.</returns>
        Task<List<DBO.Deck>> GetUserDecksAsync(int idUser);
        /// <summary>
        /// Delete a deck.
        /// </summary>
        /// <param name="id">The id of the deck.</param>
        /// <returns>True if the delete succeed, false otherwise.</returns>
        Task<bool> DeleteDeckAsync(int id);
        /// <summary>
        /// Create a deck.
        /// </summary>
        /// <param name="deck">The content of the deck.</param>
        /// <returns>The list of the deck of the user, or null if something went wrong.</returns>
        Task<List<DBO.Deck>> CreateDeckAsync(DBO.Deck deck);
        /// <summary>
        /// Update a deck.
        /// </summary>
        /// <param name="deck">The deck which needs to be update with the new values.</param>
        /// <returns>True if the update succeed, false otherwise.</returns>
        Task<bool> UpdateDeckAsync(DBO.Deck deck);
    }
}
