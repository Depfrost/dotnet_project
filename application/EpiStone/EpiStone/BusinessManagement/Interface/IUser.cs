﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement.Interface
{
    public interface IUser
    {
        /// <summary>
        /// Register a user.
        /// </summary>
        /// <param name="pseudo">The pseudo of the user.</param>
        /// <param name="email">The email of the user.</param>
        /// <param name="password">The password of the user.</param>
        /// <returns>Returns the user if the register succeeds, null otherwise.</returns>
        Task<DBO.User> Inscription(string pseudo, string email, string password);
        /// <summary>
        /// Connect a user.
        /// </summary>
        /// <param name="pseudo">The pseudo of the user.</param>
        /// <param name="password">The password of the user.</param>
        /// <returns>Returns the user if the connection succeeds, null otherwise.</returns>
        Task<DBO.User> Connexion(string pseudo, string password);
        /// <summary>
        /// Update a user.
        /// </summary>
        /// <param name="user">The user to update with the new values.</param>
        /// <returns>True if the update succeeds, false otherwise.</returns>
        Task<bool> UpdateAsync(DBO.User user);
        /// <summary>
        /// Get the score of a user.
        /// </summary>
        /// <returns>Returns the users with their scores, null if something went wrong.</returns>
        Task<List<DBO.User>> GetScoresAsync();
    }
}
