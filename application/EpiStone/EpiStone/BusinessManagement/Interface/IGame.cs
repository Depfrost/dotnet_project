﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement.Interface
{
    public interface IGame
    {
        /// <summary>
        /// Send the signal for end of turn.
        /// </summary>
        void EndTurn();
        /// <summary>
        /// Send a signal to play a monster.
        /// </summary>
        /// <param name="cardId">The id of the card.</param>
        /// <param name="specialCardId">The special id of the card for the game.</param>
        /// <param name="targetId">The target of the effect or -1 if the card doesnt't have effect or target is not needed.</param>
        void PlayMonster(long cardId, long specialCardId, long targetId = -1);
        /// <summary>
        /// Send a signal of an attack of monster.
        /// </summary>
        /// <param name="sourceId">The special id of the attacker.</param>
        /// <param name="targetId">The special id of the target.</param>
        void Attack(long sourceId, long targetId);
        /// <summary>
        /// Send a signal to play a spell.
        /// </summary>
        /// <param name="cardId">The id of the card which has the effect.</param>
        /// <param name="targetId">The special id of the target or -1 if the effect does not need a target.</param>
        void PlaySpell(long cardId, long targetId = -1);
        /// <summary>
        /// Send the signal for give up.
        /// </summary>
        void GiveUp();

    }
}
