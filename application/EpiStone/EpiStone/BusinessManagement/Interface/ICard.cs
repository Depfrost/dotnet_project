﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement.Interface
{
    public interface ICard
    {
        /// <summary>
        /// Get the list of all cards.
        /// </summary>
        /// <returns>The list of the cards, or null if something went wrong.</returns>
        Task<List<DBO.Card>> GetCardsAsync();
    }
}
