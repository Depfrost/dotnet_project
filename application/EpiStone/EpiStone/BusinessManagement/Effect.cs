﻿using EpiStone.BusinessManagement.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement
{
    public class Effect : IEffect
    {
        private DataAccess.Interface.IEffect _da;
        public Effect(DataAccess.Interface.IEffect effect)
        {
            _da = effect;
        }
        public async Task<DBO.Effect> GetEffectAsync(int id)
        {
            return await _da.GetEffectAsync(id);
        }
        public async Task<List<DBO.Effect>> GetEffectsAsync()
        {
            return await _da.GetEffectsAsync();
        }
    }
}
