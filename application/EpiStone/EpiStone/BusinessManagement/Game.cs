﻿using EpiStone.BusinessManagement.Interface;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement
{
    public class Game : IGame
    {
        public async void EndTurn()
        {
            await Settings.GameHubConnection.InvokeAsync("EndTurn", Settings.GameId, Settings.GetUser().Id);
        }

        public async void PlayMonster(long cardId, long specialCardId, long targetId = -1)
        {
            await Settings.GameHubConnection.InvokeAsync("PlayMonster", Settings.GameId, Settings.GetUser().Id, cardId, specialCardId, targetId);
        }

        public async void Attack(long sourceId, long targetId)
        {
            await Settings.GameHubConnection.InvokeAsync("Attack", Settings.GameId, Settings.GetUser().Id, sourceId, targetId);
        }

        public async void PlaySpell(long cardId, long targetId = -1)
        {
            await Settings.GameHubConnection.InvokeAsync("PlaySpell", Settings.GameId, Settings.GetUser().Id, cardId, targetId);
        }

        public async void GiveUp()
        {
            await Settings.GameHubConnection.InvokeAsync("GiveUp", Settings.GameId, Settings.GetUser().Id);
            await Settings.GameHubConnection.DisposeAsync();
        }
    }
}
