﻿using EpiStone.BusinessManagement.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement
{
    public class Heroes : IHeroes
    {
        private DataAccess.Interface.IHeroes _da;
        public Heroes(DataAccess.Interface.IHeroes da)
        {
            _da = da;
        }
        public async Task<List<DBO.Heroes>> GetHeroesListAsync()
        {
            return await _da.GetHeroesListAsync();
        }
        public async Task<DBO.Heroes> GetHeroesAsync(int id)
        {
            return await _da.GetHeroesAsync(id);
        }
    }
}
