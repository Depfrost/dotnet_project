﻿using EpiStone.BusinessManagement.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement
{
    public class Deck : IDeck
    {
        private DataAccess.Interface.IDeck _da;
        public Deck(DataAccess.Interface.IDeck da)
        {
            _da = da;
        }
        public async Task<DBO.Deck> GetDeckAsync(int id)
        {
            return await _da.GetDeckAsync(id);
        }
        public async Task<List<DBO.Deck>> CreateDeckAsync(DBO.Deck deck)
        {
            return await _da.CreateDeckAsync(Settings.GetUser().Id, deck);
        }
        public async Task<bool> UpdateDeckAsync(DBO.Deck deck)
        {
            return await _da.UpdateDeckAsync(deck);
        }
        public async Task<bool> DeleteDeckAsync(int id)
        {
            return await _da.DeleteDeckAsync(id);
        }
        public async Task<List<DBO.Deck>> GetUserDecksAsync(int idUser)
        {
            return await _da.GetUserDecksAsync(idUser);
        }
    }
}
