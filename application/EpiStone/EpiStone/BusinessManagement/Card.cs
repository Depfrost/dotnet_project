﻿using EpiStone.BusinessManagement.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.BusinessManagement
{
    public class Card : ICard
    {
        private DataAccess.Interface.ICard _da;
        private DataAccess.Interface.IEffect _effect;
        public Card(DataAccess.Interface.ICard da, DataAccess.Interface.IEffect effect)
        {
            _da = da;
            _effect = effect;
        }
        public async Task<List<DBO.Card>> GetCardsAsync()
        {
            List<DBO.Card> cards = await _da.GetCardsAsync();
            List<DBO.Effect> effects = await _effect.GetEffectsAsync();
            foreach (DBO.Card card in cards)
            {
                card.Effect = effects.FirstOrDefault(effect => effect.Id == card.EffectID);
            }
            return cards;
        }

        public async Task<DBO.Card> GetCardAsync(int id)
        {
            DBO.Card card = await _da.GetCardAsync(id);
            card.Effect = await _effect.GetEffectAsync(card.EffectID);
            return card;
        }
    }
}
