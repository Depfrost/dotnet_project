﻿using EpiStone.ViewModels;
using System.Windows.Controls;
using Xceed.Wpf.Toolkit;

namespace EpiStone.Views
{
    /// <summary>
    /// Interaction logic for Login2
    /// </summary>
    public partial class Login : UserControl
    {
        public static readonly string ViewName = "Login";
        public Login()
        {
            InitializeComponent();
        }

        private void WatermarkPasswordBox_PasswordChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            if (this.DataContext != null)
            { ((LoginViewModel)this.DataContext).Password = ((WatermarkPasswordBox)sender).Password; }
        }

        private void WatermarkTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
