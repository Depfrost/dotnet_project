﻿using System.Windows.Controls;

namespace EpiStone.Views
{
    /// <summary>
    /// Interaction logic for DeckManagement
    /// </summary>
    public partial class DeckList : UserControl
    {
        public static readonly string ViewName = "DeckList";
        public DeckList()
        {
            InitializeComponent();
        }
    }
}
