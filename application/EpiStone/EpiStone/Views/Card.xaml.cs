﻿using Prism.Commands;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace EpiStone.Views
{
    /// <summary>
    /// Interaction logic for Card
    /// </summary>
    public partial class Card : UserControl
    {
        public static readonly string ViewName = "Card";
        public static readonly DependencyProperty CardItemProperty = DependencyProperty.Register(
            ViewModels.CardViewModel.CardParameter, typeof(DBO.Card), typeof(Card), new PropertyMetadata(null, ValueChanged));
        private static void ValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Card tmp = (Card)d;
            DBO.Card card = tmp.CardItem;
            ((ViewModels.CardViewModel)((Card)d).DataContext).Card = card;
            ((ViewModels.CardViewModel)((Card)d).DataContext).RefreshCard(tmp.IsActive != null && card != null && tmp.IsActive(card));
        }
        public DBO.Card CardItem
        {
            get { return (DBO.Card)GetValue(CardItemProperty); }
            set { SetValue(CardItemProperty, value); }
        }

        public static readonly DependencyProperty DropCommandProperty = DependencyProperty.Register(
            "DropCommand", typeof(DelegateCommand<Tuple<DBO.Card, DBO.Card>>), typeof(Card));
        public DelegateCommand<Tuple<DBO.Card, DBO.Card>> DropCommand
        {
            get { return (DelegateCommand<Tuple<DBO.Card, DBO.Card>>)GetValue(DropCommandProperty); }
            set { SetValue(DropCommandProperty, value); }
        }

        public static readonly DependencyProperty IsActiveProperty = DependencyProperty.Register(
            "IsActive", typeof(Predicate<DBO.Card>), typeof(Card));
        public Predicate<DBO.Card> IsActive
        {
            get { return (Predicate<DBO.Card>)GetValue(IsActiveProperty); }
            set { SetValue(DropCommandProperty, value); }
        }

        public static readonly DependencyProperty ProvocCheckProperty = DependencyProperty.Register(
            "ProvocCheck", typeof(Predicate<DBO.MonsterBoardCard>), typeof(Card));
        public Predicate<DBO.MonsterBoardCard> ProvocCheck
        {
            get { return (Predicate<DBO.MonsterBoardCard>)GetValue(ProvocCheckProperty); }
            set { SetValue(ProvocCheckProperty, value); }
        }

        public Card()
        {
            InitializeComponent();
        }
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (e.LeftButton == MouseButtonState.Pressed && IsActive != null && IsActive(CardItem))
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                    new ParameterizedThreadStart(DoDrag), e);
        }

        private void DoDrag(object param)
        {
            MouseEventArgs e = param as MouseEventArgs;
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DataObject data = new DataObject();
                if (CardItem is DBO.MonsterBoardCard)
                {
                    data.SetData((DBO.MonsterBoardCard)CardItem);
                }
                else
                {
                    data.SetData(CardItem);
                    if (CardItem.Effect != null)
                        data.SetData(CardItem.Effect);
                }
                try
                {
                    DragDrop.DoDragDrop(this, data, DragDropEffects.Copy);
                }
                catch (InvalidOperationException ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
        }

        protected override void OnDrop(DragEventArgs e)
        {
            base.OnDrop(e);
            if (e.Data.GetDataPresent(typeof(DBO.MonsterBoardCard)))
            {
                DBO.Card card = (DBO.Card)e.Data.GetData(typeof(DBO.MonsterBoardCard));
                DropCommand.Execute(new Tuple<DBO.Card, DBO.Card>(card, CardItem));
            }
            else if (e.Data.GetDataPresent(typeof(DBO.Card)))
            {
                DBO.Card card = (DBO.Card)e.Data.GetData(typeof(DBO.Card));
                DropCommand.Execute(new Tuple<DBO.Card, DBO.Card>(card, CardItem));
            }
            e.Handled = true;
        }
        protected override void OnDragOver(DragEventArgs e)
        {
            base.OnDragOver(e);
            CardDragOver(e.Source, e);
        }

        private void CardDragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            if (e.Data.GetDataPresent(typeof(DBO.MonsterBoardCard)) && CardItem is DBO.MonsterBoardCard)
            {
                DBO.MonsterBoardCard card = (DBO.MonsterBoardCard)e.Data.GetData(typeof(DBO.MonsterBoardCard));
                if (card.SpecialId != CardItem.SpecialId && ProvocCheck != null && ProvocCheck((DBO.MonsterBoardCard)CardItem))
                    e.Effects = DragDropEffects.Copy;
            }
            else if (e.Data.GetDataPresent(typeof(DBO.Card)) &&
                ((DBO.Card)e.Data.GetData(typeof(DBO.Card))).SpecialId != CardItem.SpecialId)
            {
                if (e.Data.GetDataPresent(typeof(DBO.Effect)) &&
                    ((DBO.Effect)e.Data.GetData(typeof(DBO.Effect))).TargetSingleMonster)
                {
                    e.Effects = DragDropEffects.Copy;
                }
            }
            e.Handled = true;
        }
    }
}
