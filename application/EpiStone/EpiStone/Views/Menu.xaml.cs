﻿using System.Windows.Controls;

namespace EpiStone.Views
{
    /// <summary>
    /// Interaction logic for Menu
    /// </summary>
    public partial class Menu : UserControl
    {
        public static readonly string ViewName = "Menu";
        public Menu()
        {
            InitializeComponent();
        }
    }
}
