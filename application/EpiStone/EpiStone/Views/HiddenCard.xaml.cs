﻿using System.Windows.Controls;

namespace EpiStone.Views
{
    /// <summary>
    /// Interaction logic for HiddenCard
    /// </summary>
    public partial class HiddenCard : UserControl
    {
        public static readonly string ViewName = "HiddenCard";
        public HiddenCard()
        {
            InitializeComponent();
        }
    }
}
