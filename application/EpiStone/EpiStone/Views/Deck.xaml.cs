﻿using System.Windows.Controls;

namespace EpiStone.Views
{
    /// <summary>
    /// Interaction logic for Deck
    /// </summary>
    public partial class Deck : UserControl
    {
        public static readonly string ViewName = "Deck";
        public Deck()
        {
            InitializeComponent();
        }

        private void WatermarkTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ((ViewModels.DeckViewModel)(DataContext)).UpdateVisibleCards(searchBox.Text);
        }
    }
}
