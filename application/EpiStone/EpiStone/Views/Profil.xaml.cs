﻿using System.Windows.Controls;

namespace EpiStone.Views
{
    /// <summary>
    /// Interaction logic for Profil
    /// </summary>
    public partial class Profil : UserControl
    {
        public static readonly string ViewName = "Profil";
        public Profil()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            PseudoTextBox.IsReadOnly = false;
            EmailTextBox.IsReadOnly = false;
            EditButton.Visibility = System.Windows.Visibility.Hidden;
            ValidationButton.Visibility = System.Windows.Visibility.Visible;
        }

        private void Button_Click_1(object sender, System.Windows.RoutedEventArgs e)
        {
            PseudoTextBox.IsReadOnly = true;
            EmailTextBox.IsReadOnly = true;
            EditButton.Visibility = System.Windows.Visibility.Visible;
            ValidationButton.Visibility = System.Windows.Visibility.Hidden;
        }
    }
}
