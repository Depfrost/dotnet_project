﻿using Prism.Commands;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace EpiStone.Views
{
    /// <summary>
    /// Interaction logic for MonsterBoard
    /// </summary>
    public partial class MonsterBoard : UserControl
    {
        public static readonly DependencyProperty DropCommandProperty = DependencyProperty.Register("DropCommand", typeof(DelegateCommand<DBO.Card>), typeof(MonsterBoard));

        public DelegateCommand<DBO.Card> DropCommand
        {
            get { return (DelegateCommand<DBO.Card>)GetValue(DropCommandProperty); }
            set { SetValue(DropCommandProperty, value); }
        }

        public MonsterBoard()
        {
            InitializeComponent();
        }
        
    }
}
