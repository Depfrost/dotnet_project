﻿using System;
using System.Windows;
using System.Windows.Media;

namespace EpiStone.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static readonly string ViewName = "MainWindow";
        public static readonly string MainRegionName = "Screen";

        public MediaPlayer backgroundMusicPlayer = new MediaPlayer();
        public MainWindow()
        {
            InitializeComponent();

            backgroundMusicPlayer.Open(new Uri("../Ressources/Music/mainMusic.mp3", UriKind.RelativeOrAbsolute));
            backgroundMusicPlayer.MediaEnded += new EventHandler(Media_Ended);
            backgroundMusicPlayer.Play();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
        private void Media_Ended(object sender, EventArgs e)
        {
            backgroundMusicPlayer.Position = TimeSpan.Zero;
            backgroundMusicPlayer.Play();
        }
    }
}
