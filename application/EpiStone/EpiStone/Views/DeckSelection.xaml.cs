﻿using System.Windows.Controls;

namespace EpiStone.Views
{
    /// <summary>
    /// Interaction logic for DeckSelection
    /// </summary>
    public partial class DeckSelection : UserControl
    {
        public static readonly string ViewName = "DeckSelection";
        public DeckSelection()
        {
            InitializeComponent();
        }
    }
}
