﻿using System.Windows.Controls;

namespace EpiStone.Views
{
    /// <summary>
    /// Interaction logic for WaitingRoom
    /// </summary>
    public partial class WaitingRoom : UserControl
    {
        public static readonly string ViewName = "WaitingRoom";
        public WaitingRoom()
        {
            InitializeComponent();
        }
    }
}
