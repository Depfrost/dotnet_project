﻿using System.Windows.Controls;

namespace EpiStone.Views
{
    /// <summary>
    /// Interaction logic for Ranking
    /// </summary>
    public partial class Ranking : UserControl
    {
        public static readonly string ViewName = "Ranking";
        public Ranking()
        {
            InitializeComponent();
        }
    }
}
