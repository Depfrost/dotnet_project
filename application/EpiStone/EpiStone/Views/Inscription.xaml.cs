﻿using System.Windows.Controls;
using Xceed.Wpf.Toolkit;

namespace EpiStone.Views
{
    /// <summary>
    /// Interaction logic for Inscription
    /// </summary>
    public partial class Inscription : UserControl
    {
        public static readonly string ViewName = "Inscription";
        public Inscription()
        {
            InitializeComponent();
        }

        private void WatermarkPasswordBox_PasswordChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            if (this.DataContext != null)
            { ((dynamic)this.DataContext).Password = ((WatermarkPasswordBox)sender).Password; }
        }

        private void WatermarkPasswordBox_PasswordChanged_1(object sender, System.Windows.RoutedEventArgs e)
        {
            if (this.DataContext != null)
            { ((dynamic)this.DataContext).Password2 = ((WatermarkPasswordBox)sender).Password; }
        }
    }
}
