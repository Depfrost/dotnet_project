﻿using System.Windows;
using System.Windows.Controls;

namespace EpiStone.Views
{
    /// <summary>
    /// Interaction logic for Deck
    /// </summary>
    public partial class Game : UserControl
    {
        public static readonly string ViewName = "Game";
        public Game()
        {
            InitializeComponent();
        }

        private void MyBoard_Drop(object sender, DragEventArgs e)
        {
            //base.OnDrop(e);
            if (e.Handled)
                return;
            if (e.Data.GetDataPresent(typeof(DBO.Card)))
            {
                if (e.Data.GetDataPresent(typeof(DBO.Effect)) &&
                    ((DBO.Effect)e.Data.GetData(typeof(DBO.Effect))).TargetSingleMonster)
                    return;
                e.Handled = true;
                DBO.Card card = (DBO.Card)e.Data.GetData(typeof(DBO.Card));
                ((ViewModels.GameViewModel)(DataContext)).DropOnBoardHandler(card);
            }
        }

        private void MyBoard_DragOver(object sender, DragEventArgs e)
        {
            //base.OnDragOver(e);
            e.Effects = DragDropEffects.None;
            if (e.Data.GetDataPresent(typeof(DBO.Card)))
            {
                if (!e.Data.GetDataPresent(typeof(DBO.Effect)) ||
                    !((DBO.Effect)e.Data.GetData(typeof(DBO.Effect))).TargetSingleMonster)
                {
                    //e.Handled = true;
                    e.Effects = DragDropEffects.Copy;
                }
            }
        }

        private void Heroes_Drop(object sender, DragEventArgs e)
        {
            if (e.Handled)
                return;
            if (e.Data.GetDataPresent(typeof(DBO.MonsterBoardCard)))
            {
                e.Handled = true;
                DBO.MonsterBoardCard card = (DBO.MonsterBoardCard)e.Data.GetData(typeof(DBO.MonsterBoardCard));
                ((ViewModels.GameViewModel)(DataContext)).DropOnHeroes(card);
            }
        }

        private void Heroes_DragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            if (e.Data.GetDataPresent(typeof(DBO.MonsterBoardCard)))
            {
                if (((ViewModels.GameViewModel)(DataContext)).ProvocCheck(null))
                    e.Effects = DragDropEffects.Copy;
            }
        }
    }
}
