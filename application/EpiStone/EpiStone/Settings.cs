﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone
{
    public static class Settings
    {
        public static readonly string ServerUrl = "http://localhost:59165/";

        public static HubConnection GameHubConnection;
        public static string GameId { get; set; }
        public static bool IsPlayer1 { get; set; }

        private static DBO.User _user;
        public static DBO.User GetUser()
        {
            return _user;
        }
        public static void SetUser(DBO.User user)
        {
            _user = user;
        }
        public static string CreateRessourcePath(string type, string elt)
        {
            return "/Ressources/Images/" + type + "/" + elt;
        }
        public static string CreateRessourcePathCard(string elt)
        {
            return CreateRessourcePath("Cards", elt);
        }
        public static string CreateRessourcePathHero(string elt)
        {
            return CreateRessourcePath("HeroPortrait", elt);
        }
    }
}
