﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.DBO
{
    public class Player
    {
        public long PlayerId { get; set; }
        public int Cristals { get; set; }
        public long Life { get; set; }
        public long NbCardDeck { get; set; }
        public List<MonsterBoardCard> FieldMonster { get; set; }
        public string HeroImage { get; set; }
    }
}
