﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.DBO
{
    public class Board
    {
        public bool PlayerTurn { get; set; }
        public long Turns { get; set; }
        public UserPlayer Player { get; set; }
        public EnemyPlayer Enemy { get; set; }
    }
}
