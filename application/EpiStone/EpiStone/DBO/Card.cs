﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.DBO
{
    public class Card : Base
    {
        public enum TypeCard
        {
            Monstre,
            Sort
        }
        public int EffectID { get; set; }
        public Effect Effect { get; set; }
        public TypeCard Type { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public int Life { get; set; }
        public int Attack { get; set; }
        public int Cost { get; set; }
        public int SpecialId { get; set; }
    }
}
