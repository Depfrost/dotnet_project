﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.DBO
{
    public class MonsterBoardCard : Card
    {
        public enum StatusCard
        {
            Aucun,
            Gele,
            Provoc,
            Bouclier,
            Charge
        }
        public StatusCard Status { get; set; }
        public bool CanAttack { get; set; }
    }
}
