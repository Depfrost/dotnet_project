﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.DBO
{
    public class Effect : Base
    {
        public enum TypeEffect
        {
            Boost,
            Bouclier,
            Charge,
            Degats,
            Gele,
            Mort,
            Pioche,
            Provoc
            
        }
        public string Name { get; set; }
        public TypeEffect Type { get; set; }
        public string Description { get; set; }
        public bool TargetHero { get; set; }
        public bool TargetSingleMonster { get; set; }
    }
}
