﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.DBO
{
    public class UserPlayer : Player
    {
        public List<Card> PlayerHand { get; set; }
    }
}
