﻿using EpiStone.DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.DataAccess
{
    public class Effect : IEffect
    {
        public async Task<DBO.Effect> GetEffectAsync(int id)
        {
            string uri = Settings.ServerUrl + "api/Effects/" + id;
            return await HTTPWrapper<DBO.Effect>.GetAsync(uri);
        }
        public async Task<List<DBO.Effect>> GetEffectsAsync()
        {
            string uri = Settings.ServerUrl + "api/Effects";
            return await HTTPWrapper<List<DBO.Effect>>.GetAsync(uri);
        }
    }
}
