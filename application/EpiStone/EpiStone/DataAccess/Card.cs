﻿using EpiStone.DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.DataAccess
{
    public class Card : ICard
    {
        public async Task<List<DBO.Card>> GetCardsAsync()
        {
            string url = Settings.ServerUrl + "api/Cards";
            return await HTTPWrapper<List<DBO.Card>>.GetAsync(url);
        }

        public async Task<DBO.Card> GetCardAsync(int id)
        {
            string url = Settings.ServerUrl + "api/Cards/" + id;
            DBO.Card card = await HTTPWrapper<DBO.Card>.GetAsync(url);
            return card;
        }
    }
}
