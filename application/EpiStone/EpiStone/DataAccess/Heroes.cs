﻿using EpiStone.DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.DataAccess
{
    public class Heroes : IHeroes
    {
        public async Task<List<DBO.Heroes>> GetHeroesListAsync()
        {
            string uri = Settings.ServerUrl + "api/Heroes";
            return await HTTPWrapper<List<DBO.Heroes>>.GetAsync(uri);
        }
        public async Task<DBO.Heroes> GetHeroesAsync(int id)
        {
            string uri = Settings.ServerUrl + "api/Heroes/" + id;
            return await HTTPWrapper<DBO.Heroes>.GetAsync(uri);
        }
    }
}
