﻿using EpiStone.DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.DataAccess
{
    public class Deck : IDeck
    {
        public async Task<DBO.Deck> GetDeckAsync(int id)
        {
            string uri = Settings.ServerUrl + "api/Decks/" + id;
            return await HTTPWrapper<DBO.Deck>.GetAsync(uri);
        }
        public async Task<List<DBO.Deck>> CreateDeckAsync(int id, DBO.Deck deck)
        {
            string uri = Settings.ServerUrl + "api/Decks";
            return await HTTPWrapper<List<DBO.Deck>>.PostAsync(uri, deck);
        }
        public async Task<bool> UpdateDeckAsync(DBO.Deck deck)
        {
            string uri = Settings.ServerUrl + "api/Decks/" + deck.Id;
            return await HTTPWrapper<bool>.PutAsync(uri, deck);
        }
        public async Task<bool> DeleteDeckAsync(int id)
        {
            string uri = Settings.ServerUrl + "api/Decks/" + id;
            return await HTTPWrapper<bool>.DeleteAsync(uri);
        }
        public async Task<List<DBO.Deck>> GetUserDecksAsync(int idUser)
        {
            string uri = Settings.ServerUrl + "api/Decks/user/" + idUser;
            return await HTTPWrapper<List<DBO.Deck>>.GetAsync(uri);
        }
    }
}
