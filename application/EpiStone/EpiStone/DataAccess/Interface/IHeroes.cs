﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.DataAccess.Interface
{
    public interface IHeroes
    {
        /// <summary>
        /// Get the list of heroes.
        /// </summary>
        /// <returns>The list of heroes, or null if something went wrong.</returns>
        Task<List<DBO.Heroes>> GetHeroesListAsync();
        /// <summary>
        /// Get a heroes.
        /// </summary>
        /// <param name="id">The id of the heroes.</param>
        /// <returns>The heroes found or null if something went wrong.</returns>
        Task<DBO.Heroes> GetHeroesAsync(int id);
    }
}
