﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.DataAccess.Interface
{
    public interface IUser
    {
        /// <summary>
        /// Get a user.
        /// </summary>
        /// <param name="pseudo">The pseudo of the user.</param>
        /// <param name="password">The password of the user.</param>
        /// <returns>Returns the user if it is found, null otherwise.</returns>
        Task<DBO.User> GetUserAsync(string pseudo, string password);
        /// <summary>
        /// Create a user
        /// </summary>
        /// <param name="pseudo">The pseudo of the user.</param>
        /// <param name="email">The email of the user.</param>
        /// <param name="password">The password of the user.</param>
        /// <returns>Returns the user if the creation succeeds, null otherwise.</returns>
        Task<DBO.User> CreateUserAsync(string pseudo, string email, string password);
        /// <summary>
        /// Update a user.
        /// </summary>
        /// <param name="user">The user to update with the new values.</param>
        /// <returns>True if the update succeeds, false otherwise.</returns>
        Task<bool> UpdateUserAsync(DBO.User user);
        /// <summary>
        /// Delete a user.
        /// </summary>
        /// <param name="id">The id of the user.</param>
        /// <returns>True if the delete succeeds, false otherwise.</returns>
        Task<bool> DeleteUserAsync(int id);
        /// <summary>
        /// Get the score of a user.
        /// </summary>
        /// <returns>Returns the users with their scores, null if something went wrong.</returns>
        Task<List<DBO.User>> GetScoresAsync();
    }
}
