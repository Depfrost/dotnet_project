﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.DataAccess.Interface
{
    public interface IEffect
    {
        /// <summary>
        /// Get a specific effect.
        /// </summary>
        /// <param name="id">The id of the effect.</param>
        /// <returns>The effect found or null if something went wrong.</returns>
        Task<DBO.Effect> GetEffectAsync(int id);
        /// <summary>
        /// Get the list of effects.
        /// </summary>
        /// <returns>The list of effect, or null if something went wrong.</returns>
        Task<List<DBO.Effect>> GetEffectsAsync();
    }
}
