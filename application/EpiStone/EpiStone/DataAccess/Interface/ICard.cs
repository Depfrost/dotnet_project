﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.DataAccess.Interface
{
    public interface ICard
    {
        /// <summary>
        /// Get the list of all cards.
        /// </summary>
        /// <returns>The list of the cards, or null if something went wrong.</returns>
        Task<List<DBO.Card>> GetCardsAsync();
        /// <summary>
        /// Get a card.
        /// </summary>
        /// <param name="id">The id of the card.</param>
        /// <returns>The card, or null if something went wrong.</returns>
        Task<DBO.Card> GetCardAsync(int id);
    }
}
