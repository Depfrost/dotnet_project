﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.DataAccess
{
    /// <summary>
    /// Wrapper to make http requests.
    /// </summary>
    /// <typeparam name="T">The type of the return value.</typeparam>
    public static class HTTPWrapper<T>
    {
        /// <summary>
        /// Make a GET request.
        /// </summary>
        /// <param name="url">The url for the http request</param>
        /// <returns>The value returns convert into a T object. If an error occurs, return default(T).</returns>
        public static async Task<T> GetAsync(string url)
        {
            HttpClient client = new HttpClient();
            try
            {
                var uri = new Uri(url);
                client.Timeout = TimeSpan.FromSeconds(10);
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<T>(content);
                    //Debug.WriteLine(result);
                    return result;
                }
                return default(T);

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return default(T);
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Make a POST request.
        /// </summary>
        /// <param name="url">The url for the http request.</param>
        /// <param name="data">The data to send.</param>
        /// <returns>The value returns convert into a T object. If an error occurs, return default(T).</returns>
        public static async Task<T> PostAsync(string url, object data)
        {
            HttpClient client = new HttpClient();
            try
            {
                var uri = new Uri(url);
                client.Timeout = TimeSpan.FromSeconds(10);
                var buffer = System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(data));
                var postData = new ByteArrayContent(buffer);
                postData.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = await client.PostAsync(uri, postData);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<T>(content);
                }
                return default(T);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return default(T);
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Make a PUT request.
        /// </summary>
        /// <param name="url">The url for the http request.</param>
        /// <param name="data">The data to send.</param>
        /// <returns>True if the request succeeds, false otherwise.</returns>
        public static async Task<bool> PutAsync(string url, object data)
        {
            HttpClient client = new HttpClient();
            try
            {
                var uri = new Uri(url);
                client.Timeout = TimeSpan.FromSeconds(10);
                var buffer = System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(data));
                var postData = new ByteArrayContent(buffer);
                postData.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = await client.PutAsync(uri, postData);
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
            finally
            {
                client.Dispose();
            }
        }
        /// <summary>
        /// Make a DELETE request.
        /// </summary>
        /// <param name="url">The url for the http request.</param>
        /// <returns>True if the request succeeds, false otherwise.</returns>
        public static async Task<bool> DeleteAsync(string url)
        {
            HttpClient client = new HttpClient();
            try
            {
                var uri = new Uri(url);
                client.Timeout = TimeSpan.FromSeconds(10);
                var response = await client.DeleteAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
            finally
            {
                client.Dispose();
            }
        }
    }
}
