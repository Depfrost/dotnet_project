﻿using EpiStone.DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpiStone.DataAccess
{
    public class User : IUser
    {
        public async Task<DBO.User> GetUserAsync(string pseudo, string password)
        {
            string url = Settings.ServerUrl + "api/Users/" + pseudo + "/" + password;
            return await HTTPWrapper<DBO.User>.GetAsync(url);
        }
        public async Task<DBO.User> CreateUserAsync(string pseudo, string email, string password)
        {
            string url = Settings.ServerUrl + "api/Users";
            DBO.User newUser = new DBO.User()
            {
                Pseudo = pseudo,
                Email = email,
                Password = password
            };
            return await HTTPWrapper<DBO.User>.PostAsync(url, newUser);
        }
        public async Task<bool> UpdateUserAsync(DBO.User user)
        {
            string url = Settings.ServerUrl + "api/Users/" + user.Id;
            return await HTTPWrapper<bool>.PutAsync(url, user);
        }
        public async Task<bool> DeleteUserAsync(int id)
        {
            string url = Settings.ServerUrl + "api/Users/" + id;
            return await HTTPWrapper<bool>.DeleteAsync(url);
        }
        public async Task<List<DBO.User>> GetScoresAsync()
        {
            string url = Settings.ServerUrl + "api/Scores";
            return await HTTPWrapper<List<DBO.User>>.GetAsync(url);
        }
    }
}
