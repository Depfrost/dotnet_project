﻿using EpiStone.Views;
using System.Windows;
using Prism.Modularity;
using Microsoft.Practices.Unity;
using Prism.Unity;

namespace EpiStone
{
    class Bootstrapper : UnityBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void InitializeShell()
        {
            Application.Current.MainWindow.Show();
        }

        protected override void ConfigureModuleCatalog()
        {
            var moduleCatalog = (ModuleCatalog)ModuleCatalog;
            //moduleCatalog.AddModule(typeof(YOUR_MODULE));
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();

            Container.RegisterTypeForNavigation<Inscription>(Inscription.ViewName);
            Container.RegisterTypeForNavigation<Login>(Login.ViewName);
            Container.RegisterTypeForNavigation<Menu>(Menu.ViewName);
            Container.RegisterTypeForNavigation<Profil>(Profil.ViewName);
            Container.RegisterTypeForNavigation<Ranking>(Ranking.ViewName);
            Container.RegisterTypeForNavigation<DeckList>(DeckList.ViewName);
            Container.RegisterTypeForNavigation<Card>(Card.ViewName);
            Container.RegisterTypeForNavigation<HiddenCard>(HiddenCard.ViewName);
            Container.RegisterTypeForNavigation<Deck>(Deck.ViewName);
            Container.RegisterTypeForNavigation<DeckSelection>(DeckSelection.ViewName);
            Container.RegisterTypeForNavigation<WaitingRoom>(WaitingRoom.ViewName);
            Container.RegisterTypeForNavigation<Game>(Game.ViewName);

            Container.RegisterType<BusinessManagement.Interface.ICard, BusinessManagement.Card>();
            Container.RegisterType<BusinessManagement.Interface.IDeck, BusinessManagement.Deck>();
            Container.RegisterType<BusinessManagement.Interface.IEffect, BusinessManagement.Effect>();
            Container.RegisterType<BusinessManagement.Interface.IGame, BusinessManagement.Game>();
            Container.RegisterType<BusinessManagement.Interface.IHeroes, BusinessManagement.Heroes>();
            Container.RegisterType<BusinessManagement.Interface.IUser, BusinessManagement.User>();

            Container.RegisterType<DataAccess.Interface.ICard, DataAccess.Card>();
            Container.RegisterType<DataAccess.Interface.IDeck, DataAccess.Deck>();
            Container.RegisterType<DataAccess.Interface.IEffect, DataAccess.Effect>();
            Container.RegisterType<DataAccess.Interface.IHeroes, DataAccess.Heroes>();
            Container.RegisterType<DataAccess.Interface.IUser, DataAccess.User>();
        }
    }
}
